<?php
// Text
$_['text_information']  = 'Information';
$_['text_service']      = 'Customer Service';
$_['text_extra']        = 'Extras';
$_['text_contact']      = 'Contact Us';
$_['text_return']       = 'Returns';
$_['text_sitemap']      = 'Site Map';
$_['text_manufacturer'] = 'Brands';
$_['text_voucher']      = 'Gift Certificates';
$_['text_affiliate']    = 'Affiliates';
$_['text_special']      = 'Specials';
$_['text_account']      = 'My Account';
$_['text_order']        = 'Order History';
$_['text_wishlist']     = 'Wish List';
$_['text_newsletter']   = 'Newsletter';
$_['text_companyname']   = 'JESCO HOA BINH ENGINEERING JSC';
$_['text_address']   = '9th Floor, South Building - 60 Truong Son Street, Ward 2, Tan Binh Dist, HCMC';
$_['text_powered']      = 'Powered By <a href="http://www.phudieudatset.com">Phù điêu đất sét</a><br /> %s &copy; %s';

$_['text_our_company']   = 'Our Company';
$_['text_aboutus']   = 'About us';
$_['text_our_services']   = 'Our Services';
$_['text_scanner']   = 'Barcode Scanner';
$_['text_printer']   = 'Barcode Printer';
$_['text_accessories']   = 'Barcode Accessories';
$_['text_rfidsolution']   = 'RFID Solution';
$_['text_follow']   = 'Follow Us';