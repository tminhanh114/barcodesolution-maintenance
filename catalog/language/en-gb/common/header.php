<?php
// Text
$_['text_home']          = 'Home';
$_['text_wishlist']      = 'Wish List (%s)';
$_['text_shopping_cart'] = 'Shopping Cart';
$_['text_category']      = 'Categories';
$_['text_account']       = 'My Account';
$_['text_register']      = 'Register';
$_['text_login']         = 'Login';
$_['text_order']         = 'Order History';
$_['text_transaction']   = 'Transactions';
$_['text_download']      = 'Downloads';
$_['text_logout']        = 'Logout';
$_['text_checkout']      = 'Checkout';
$_['text_search']        = 'Search';
$_['text_all']           = 'Show All';

$_['text_aboutus']           = 'About us';
$_['text_overview']           = 'Overview';
$_['text_vision']           	= 'Vision/mission';
$_['text_leadership']           = 'Leadership ';
$_['text_organization']         = 'Organization Chart';
$_['text_engagement']           = 'Engagement';
$_['text_establish']           = 'Establish';
$_['text_news']           		= 'News';
$_['text_tech_news']           	= 'Tech News';
$_['text_jescoasianews']        = 'Jesco Asia News';
$_['text_jescohoabinhnews']     = 'Jesco Hoa Binh News';
$_['text_careers']           	= 'Careers';
$_['text_buss']           		= 'Bussiness';
$_['text_electric']           	= 'Electrical system';
$_['text_acmv']           		= 'ACMV system';
$_['text_water']           		= 'Water supply and waste water treatment system';
$_['text_fire']           		= 'Fire fighting system';
$_['text_mne']           		= 'M&E consultant';
$_['text_projects']           	= 'Projects';

$_['text_scanner']           	= 'Scanner';
$_['text_printer']           	= 'Printer';
$_['text_accessories']           	= 'Accessories';
$_['text_promotion']           	= 'Promotion';
$_['text_rfid']           	= 'RFID';
$_['text_solution']           	= 'Solution';
$_['text_rent']           	= 'Rental';

$_['text_ongoing']           	= 'Ongoing Projects';
$_['text_completed']           	= 'Completed Projects';
$_['text_activity']           	= 'Company Activity';
$_['text_activity_company']     = 'Company Activity';
$_['text_activity_social']      = 'Social Activity';
$_['text_activity_gallery']      = 'Gallery';
$_['text_investor_relation']    = 'Investor Relation';
$_['text_corporate_governance'] = 'CORPORATE GOVERNANCE';
$_['text_announcements']       	= 'Information disclosure'; 
$_['text_gms']       			= 'GMS'; 
$_['text_investor_activity']    = 'Strategical partners';
$_['text_contact']           = 'Contact';
