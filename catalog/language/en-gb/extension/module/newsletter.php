<?php
// Heading
$_['heading_title']     			  = 'Newsletter';
// Sent
$_['error_newsletter_sent']           = 'Newsletter sent successfully!';

// Error
$_['error_news_email_duplicate']      = 'Your email is already in our database!';
$_['error_news_email_invalid']        = 'E-Mail Address does not appear to be valid!';
$_['error_news_email_required']       = 'E-mail is required!';
$_['error_newsletter_fail']           = 'Failed to send newsletter!';

// Text
$_['text_when']     				  = 'When chose us';
$_['text_when_comment']     		= 'Tư vấn các vấn đề liên quan đến dự án, bảo trì, bảo dưỡng, vận hành tòa nhà, ...';

$_['text_email']     				  = 'Email:';
$_['text_subscribe_title']     			= 'Subscribe Email';
$_['text_subscribe_btn']     		  = 'Subscribe';
$_['text_subscribe_placeholder']      = 'Subscribe our newsletter';
$_['text_subscribe_comment']      = 'Connect with us to keep track of the latest information';

$_['text_brochure_title']     				  = 'Brochure Jesco Hoa Binh';
$_['text_brochure_comment']     		  = 'Download the company’s latest brochure for better knowledge about us';


$_['text_news']           		= 'News';
$_['text_hbcnews']           	= 'HBC News';
$_['text_jescoasianews']        = 'Jesco Asia News';
$_['text_jescohoabinhnews']     = 'Jesco Hoa Binh News';
$_['text_careers']           	= 'Careers';
$_['text_activity']           	= 'Company Activity';
$_['text_activity_company']     = 'Company Activity';
$_['text_activity_social']      = 'Social Activity';
$_['text_investor_relation']    = 'Investor Relation';
$_['text_corporate_governance'] = 'Corporate Governance';
$_['text_announcements']       	= 'Information disclosure'; 
$_['text_gms']       			= 'GMS'; 
$_['text_investor_activity']    = 'Investor activities';
$_['text_sustainability']    = 'Sustainability';
