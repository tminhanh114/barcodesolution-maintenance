<?php
// Text
$_['text_search']       = 'Tìm kiếm';
$_['text_brand']        = 'Thương hiệu';
$_['text_manufacturer'] = 'Nhà sản xuất:';
$_['text_model']        = 'Mã sản phẩm:';
$_['text_bookname']     = 'Sách:';
$_['text_author']       = 'Tác giả:';
$_['text_translator']   = 'Dịch giả:';
$_['text_isbn13']       = 'ISBN-13:';
$_['text_binding']      = 'Trình bày bìa sách:';
$_['text_publishingdate'] = 'Năm phát hành:';
$_['text_publisher']    = 'Nhà xuất bản:';
$_['text_numberofpages'] = 'Số trang:';
$_['text_booklanguage']     = 'Ngôn ngữ sách:';
$_['text_madein']     = 'Xuất xứ:';
$_['text_material']     = 'Chất liệu:';
$_['text_reward']       = 'Điểm thưởng:'; 
$_['text_points']       = 'Giá điểm thưởng:';
$_['text_stock']        = 'Tình trạng:';
$_['text_instock']      = 'Còn hàng';
$_['text_price']        = 'Giá:'; 
$_['text_tax']          = 'Giá chưa có VAT:'; 
$_['text_discount']     = '<b>Giá đặc biệt khi mua với số lượng từ %s: %s</b>';
$_['text_option']       = 'Chọn kích thước';
$_['text_qty']          = 'Số lượng:';
$_['text_minimum']      = 'Sản phẩm này có số lượng đặt tối thiểu %s';
$_['text_or']           = '- Hoặc -';
$_['text_login']                              = 'Bạn hãy <a href="%s">đăng nhập</a> hoặc <a href="%s">đăng ký</a> để đăng/viết ý kiến';
$_['text_reviews']      = '%s đánh giá'; 
$_['text_write']        = 'Write a review';
$_['text_no_reviews']   = 'There are no reviews for this product';
$_['text_note']         = '<span style="color: #FF0000;">Note:</span> does not support HTML!';
$_['text_related']                            = 'Sản phẩn liên quan';
$_['text_share']        = 'Chia sẻ';
$_['text_success']      = 'Cám ơn bạn đã đánh giá. Đánh giá đã được gửi đến quản trị trước khi được chấp nhận.';
$_['text_upload']       = 'Tập tin của bạn đã tải lên thành công!';
$_['text_wait']         = 'Vui lòng đợi!';
$_['text_payment_recurring']                    = 'Phương thức trả định kỳ';
$_['text_trial_description']   = '%s cho mỗi lần thanh toán, %d lần mỗi %s , thanh toán %d lần, sau đó';
$_['text_payment_description'] = '%s cho mỗi lần thanh toán, %d lần mỗi %s , thanh toán %d lần';
$_['text_payment_cancel']      = '%s cho mỗi lần thanh toán, %d lần mỗi %s cho đến khi thanh toán hết';
$_['text_day']                                = 'ngày';
$_['text_week']                               = 'tuần';
$_['text_semi_month']                         = 'nửa tháng';
$_['text_month']                              = 'tháng';
$_['text_year']                               = 'năm';
$_['text_tags']         = 'Tag:';
$_['text_error']        = 'Không có sản phẩm!';
$_['text_contact']  = 'Contact'; 

// Entry
$_['entry_qty']                               = 'Số lượng';
$_['entry_name']        = 'Your name:';
$_['entry_review']      = 'Write a review:';
$_['entry_rating']      = 'Your rating:';
$_['entry_good']        = 'Good';
$_['entry_bad']         = 'Bad';
$_['entry_captcha']     = 'Nhập mã kiểm tra vào ô bên dưới:';

// Tabs
$_['tab_description']   = 'Description';
$_['tab_meaning']   = 'Tiện ích khu vực';
$_['tab_attribute']     = 'Đặc tính';
$_['tab_review']        = 'Rating (%s)';
$_['text_related']             = 'Related Products';

// Error
// Error
$_['error_name']               = 'Warning: Review Name must be between 3 and 25 characters!';
$_['error_text']               = 'Warning: Review Text must be between 25 and 1000 characters!';
$_['error_rating']             = 'Warning: Please select a review rating!';
$_['error_captcha']     = 'Lỗi: Mã kiểm tra không chính xác!';
$_['error_upload']      = 'Cần tải lên tập tin!';
$_['error_filename']    = 'Tên tập tin từ 3 đến 128 kí tự!';
$_['error_filetype']    = 'Kiểu file không hợp lệ!';
?>