<?php
// Heading
$_['heading_title']  = 'Contact Us';

// Text
$_['text_location']  = 'Our Location';
$_['text_store']     = 'Our Stores';
$_['text_contact']   = 'Contact Form';

$_['text_fax']       = 'Fax';
$_['text_open']      = 'Opening Times';
$_['text_comment']   = 'Comments';
$_['text_success']   = '<p>Your enquiry has been successfully sent to the store owner!</p>';
$_['text_message']   = 'Thanks, Your enquiry has been successfully sent to our department!';

// Entry
$_['entry_name']     = 'Your Name';
$_['entry_department']         = 'Select Department';
$_['entry_department_1']         = 'Sale';
$_['entry_department_2']         = 'Technical';
$_['entry_email']    = 'E-Mail Address';
$_['entry_phone']    = 'Phone Number';
$_['entry_enquiry']  = 'Enquiry';
$_['entry_submit']	 = 'Send';

// Email
$_['email_subject']  = 'Enquiry %s';

// Errors
$_['error_name']     = 'Name must be between 3 and 32 characters!';
$_['error_email']    = 'E-Mail Address does not appear to be valid!';
$_['error_enquiry']  = 'Enquiry must be between 10 and 3000 characters!';

$_['text_companyname']   = 'BEETECH COMPANY LIMITED';
$_['text_address']   = 'Address: No 30 đường 18, KP 5, Hiep Binh Chanh Ward, Thu Duc City, HCMC, Vietnam';
$_['text_telephone'] = 'Hotline: 093.808.9669';
