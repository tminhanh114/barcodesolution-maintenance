<?php
// Heading
$_['heading_title']      = 'Liên hệ với chúng tôi';

// Text 
$_['text_location']      = 'Thông tin của chúng tôi';
$_['text_store']     = 'Gian hàng của chúng tôi';
$_['text_contact']       = 'Thông tin liên hệ';
$_['text_address']       = 'Địa chỉ:';
$_['text_email']         = 'E-Mail:';
$_['text_telephone']     = 'Điện thoại:';
$_['text_fax']           = 'Fax:';
$_['text_message']       = 'Cảm ơn bạn. Thư liên hệ của bạn đã được gửi đến chúng tôi!';
$_['text_open']      = 'Giờ mở cửa';
$_['text_comment']   = 'Ý kiến đóng góp';
$_['text_success']   = '<p>Your enquiry has been successfully sent to the store owner!</p>';

// Entry Fields
$_['entry_name']         = 'Họ và tên';
$_['entry_department']         = 'Chọn phòng ban';
$_['entry_department_1']         = 'Phòng kinh doanh';
$_['entry_department_2']         = 'Phòng kỹ thuật';
$_['entry_email']        = 'Địa chỉ EMail';
$_['entry_phone']    = 'Số điện thoại';
$_['entry_enquiry']      = 'Nội dung';
$_['entry_captcha']	 = 'Nhập các ký tự bên dưới:';
$_['entry_submit']	 = 'Gửi';

// Email
$_['email_subject']      = 'Tiêu đề %s';

// Errors
$_['error_name']         = 'Tên của bạn phải nhiều hơn 3 và nhỏ hơn 32 ký tự!';
$_['error_email']        = 'Bạn phải điền chính xác Email!';
$_['error_enquiry']      = 'Nội dung liên hệ phải nhiều hơn 10 và nhỏ hơn 3000 ký tự!';
$_['error_captcha']	 = 'Bạn đã nhập mã xác nhận không chính xác!';

$_['text_companyname']   = 'CÔNG TY TNHH BEETECH';
$_['text_address']   = 'Địa chỉ: 30 đường 18, KP 5, P. Hiệp Bình Chánh, Tp. Thủ Đức, TP.Hồ Chí Minh';
$_['text_telephone'] = 'Hotline: 093.808.9669'
?>
