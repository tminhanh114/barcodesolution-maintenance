<?php
// Text
$_['text_information']  = 'Thông tin';
$_['text_service']      = 'Chăm sóc khách hàng';
$_['text_extra']        = 'Chức năng khác';
$_['text_contact']      = 'Liên hệ';
$_['text_return']       = 'Trả hàng';
$_['text_sitemap']      = 'Sơ đồ trang';
$_['text_manufacturer'] = 'Thương hiệu';
$_['text_voucher']      = 'Phiếu quà tặng';
$_['text_affiliate']    = 'Đại lý';
$_['text_special']      = 'Khuyến mãi';
$_['text_account']      = 'Tài khoản của tôi';
$_['text_order']        = 'Lịch sử đơn hàng';
$_['text_wishlist']     = 'Danh sách yêu thích';
$_['text_newsletter']   = 'Thư thông báo';
$_['text_powered']      = 'Bản quyền Phù điêu đất sét &copy; 2017';
$_['text_companyname']   = 'CÔNG TY CỔ PHẦN KỸ THUẬT JESCO HÒA BÌNH';
$_['text_address']   = 'Lầu 9, Tòa nhà South Building - 60 Trường Sơn, P.2, Q.Tân Bình, TP.HCM';

$_['text_our_company']   = 'Về chúng tôi';
$_['text_aboutus']   = 'Về chúng tôi';
$_['text_our_services']   = 'Các dịch vụ của Barcodesolution';
$_['text_scanner']   = 'Máy quét mã vạch';
$_['text_printer']   = 'Máy in mã vạch';
$_['text_accessories']   = 'Phụ kiện mã vạch';
$_['text_rfidsolution']   = 'Thiết bị và giải pháp RFID';
$_['text_follow']   = 'Kết nối với chúng tôi';