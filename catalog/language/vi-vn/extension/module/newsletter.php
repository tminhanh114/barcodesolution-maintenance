<?php
// Heading
$_['heading_title']     			  = 'Newsletter';



// Sent
$_['error_newsletter_sent']           = 'Chúng tôi đã nhận được email của Bạn!';

// Error
$_['error_news_email_duplicate']      = 'Chúng tôi đã nhận được email của Bạn!';
$_['error_news_email_invalid']        = 'Địa chỉ email lỗi!';
$_['error_news_email_required']       = 'Hãy thêm địa chỉ email!';
$_['error_newsletter_fail']           = 'Có lỗi xảy ra khi đăng kí email!';

// Text

$_['text_when']     				  = 'Chúng tôi sẵn sàng';
$_['text_when_comment']     				  = 'Tư vấn các vấn đề liên quan đến dự án triển khai hệ thống barcode, RFID ...';
$_['text_email']     				  = 'Email:';
$_['text_subscribe_title']     			= 'Đăng ký nhận bản tin';
$_['text_subscribe_btn']     		  = 'Đăng ký';
$_['text_subscribe_placeholder']      = 'Để lại địa chỉ email';
$_['text_subscribe_comment']      = 'Gửi đến bạn những thông tin cập nhật nhất';

$_['text_brochure_title']     				  = 'Brochure Công ty Beetech';
$_['text_brochure_comment']     		  = 'Thông tin mới nhất của Beetech, giúp các bạn hiểu chúng tôi hơn';


$_['text_news']           		= 'Tin tức';
$_['text_hbcnews']           	= 'Tin thị trường';
$_['text_jescoasianews']        = 'Tin công nghệ';
$_['text_jescohoabinhnews']     = 'Tin khuyến mãi';
$_['text_careers']           	= 'Cơ hội nghề nghiệp';
$_['text_activity']           	= 'Hoạt động công ty';
$_['text_activity_company']     = 'Hoạt động công ty';
$_['text_activity_social']      = 'Hoạt động cộng đồng';
$_['text_investor_relation']    = 'Quan hệ đầu tư';
$_['text_corporate_governance'] = 'Công tác xã hội';
$_['text_announcements']       	= 'Công bố thông tin'; 
$_['text_gms']       			= 'Đại hội đồng cổ động'; 
$_['text_investor_activity']    = 'Hoạt động đầu tư';
$_['text_sustainability']    = 'Phát triển bền vững';
