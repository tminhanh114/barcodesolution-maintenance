<footer>
<div class="container">
<div id="rfid-footer" style="width: 100%;position: relative;float: left;padding: 20px 10px;">
    <div class="footer-content" style="position: relative;margin: 0 auto;max-width: 1200px;">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
            <ul class="column" style="padding: 3px">
                <li class="footer-title"><?php echo $footer_sitemap['our_company']['text']; ?></li>
                <li><a href="<?php echo $footer_sitemap['aboutus']['link']; ?>"><?php echo $footer_sitemap['aboutus']['text']; ?></a></li>
                <li><a href="<?php echo $footer_sitemap['contact']['link']; ?>"><?php echo $footer_sitemap['contact']['text']; ?></a></li>
            </ul>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
            <ul class="column" style="padding: 3px">
                <li class="footer-title"><?php echo $footer_sitemap['our_services']['text']; ?></li>
                <li><a href="<?php echo $footer_sitemap['our_services_scanner']['link']; ?>"><?php echo $footer_sitemap['our_services_scanner']['text']; ?></a></li>
                <li><a href="<?php echo $footer_sitemap['our_services_printer']['link']; ?>"><?php echo $footer_sitemap['our_services_printer']['text']; ?></a></li>
                <li><a href="<?php echo $footer_sitemap['our_services_accessories']['link']; ?>"><?php echo $footer_sitemap['our_services_accessories']['text']; ?></a></li>
                <li><a href="<?php echo $footer_sitemap['our_services_rfidsolution']['link']; ?>"><?php echo $footer_sitemap['our_services_rfidsolution']['text']; ?></a></li>
            </ul>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
            <ul class="column" style="padding: 3px">
                <li class="footer-title"><?php echo $footer_sitemap['follow']['text']; ?></li>
                <li style="list-style:none;text-align:left;"><a href="https://www.facebook.com/RfidBarcodeSolutionVietNam" title="Join our Facebook community for special promotions and contests" rel="nofollow" target="_blank"><img src="http://www.ptsmobile.com/images/facebook-icon.png" alt="Participate in our special Facebook promotions" width="30" height="30"></a>&nbsp;&nbsp;
                    <a href="https://www.facebook.com/RfidBarcodeSolutionVietNam" rel="nofollow" title="Connect with us on Twitter" target="_blank"><img src="http://www.ptsmobile.com/images/twitter-icon.png" alt="Share stories with us on Twitter" width="30" height="30"></a>&nbsp;&nbsp;
                    <a href="https://www.facebook.com/RfidBarcodeSolutionVietNam" rel="nofollow" title="Visit our YouTube Channel" target="_blank"><img src="http://www.ptsmobile.com/images/youtube-icon.png" alt="Visit YouTube and leave comments on our videos" width="30" height="29"></a>
                </li>
            </ul>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
            <ul class="column" style="padding: 3px">
                <li class="footer-title">WEBSITE THÀNH VIÊN</li>
                <li style="list-style:none;text-align:left;">
                  <a class="hidden" href="http://barcodesolution.com.vn/"><img style="height: 36px;" src="http://rfidsolution.com.vn/image/data/8_system/barcode.png" /></a>
                  <a href="http://beetech.com.vn/"><img style="height: 36px;" src="http://rfidsolution.com.vn/image/data/8_system/beetech-cn.png" /></a>&nbsp; 
                  <a class="hidden" href="http://splendidtechnology.com.vn/"><img style="height: 28px;" src="http://beetech.com.vn/image/data/8_system/splendidtechnology.png" /></a>
                  <a href="http://rfidsolution.com.vn/"><img style="height: 36px;" src="http://rfidsolution.com.vn/catalog/view/theme/default/image/rfid/logo_h70.png" /></a>
                </li>
            </ul>
        </div>
    </div>
</div>
</div>
<div class="aboutus-hub-new">
  <a href="gioi-thieu-barcode-solution"><img src="image/catalog/system/info.png" /><span style="font: 300 12px/12px 'open sans',verdana;color: black;clear: both;width: 100%;">Giới thiệu</span></a>
  <a href="lien-he"><img src="image/catalog/system/mail.png"  /><span style="font: 300 12px/12px 'open sans',verdana;color: black;">Liên hệ</span></a>
  <a target="_blank" href="https://www.facebook.com/RfidBarcodeSolutionVietNam"><img src="image/catalog/system/facebook.png" /><span style="font: 300 12px/12px 'open sans',verdana;color: black;">Facebook</span></a>
</div>
<div class="aboutus-hub hidden">
  <?php $icon_arr = array(0 => "group", 1 => "star", 2 => "question_answer"); ?>
  <ul>
      <li><a href="gioi-thieu-barcode-solution"><i class="material-icons"><?php echo $icon_arr[0]; ?></i><span><?php echo 'Giới thiệu'; ?></a></span></li>
      <li><a href="lien-he"><i class="material-icons"><?php echo $icon_arr[2]; ?></i><span><?php echo 'Liên hệ'; ?></span></a></li>
      <li><a target="_blank" href="https://www.facebook.com/RfidBarcodeSolutionVietNam"><i class="material-icons" style="background: #3b579d;border-color: #3b579d !important;"><img style="width: 32px;padding: 3px;position: relative;float: left;" src="image/catalog/system/facebook-white-64.png"></i><span><?php echo 'facebook'; ?></span></a></li>
  </ul>
</div>
<!-- Start of beetechsupport Zendesk Widget script -->
<script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=188d1591-a940-46ac-a30e-107f4729b0e1"> </script>
<!-- End of beetechsupport Zendesk Widget script -->

<!-- BEGIN JIVOSITE CODE {literal} -->
<!--script type='text/javascript'>
(function(){ var widget_id = 'TBVyaNoAL9';var d=document;var w=window;function l(){
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script-->
<!-- {/literal} END JIVOSITE CODE -->

</footer>
<!-- ajax search -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
$(document).ready( function() {



});  
</script>
<!-- ajax search ::end-->
<!--a href="tel:"><i class="Phone is-animating"></i></a-->
<a href="tel:(84)938089669" style="display: block;position: fixed;left: 20px;bottom: 40px;z-index: 101;"><i class="material-icons phone-ringing" style="font-size: 36px;color: #1ea1db;">local_phone</i></a>
<a href="#" class ="scrollToTop" style="z-index: 101;" ><i class="material-icons" style="font-size: 32px;color: #1ea1db;font-weight: bolder;">arrow_upward</i></a>
<script type="text/javascript">
$(document).ready(function(){
  $('#menu-aboutus li').click(function(){
    $('#aboutus-hub-button').click();
  });

  $('.toggle-btn').click(function(){
    $('#overlayForMobileMenu').toggleClass('displayOverlay');
  });
  $('#overlayForMobileMenu').click(function(){
    //$('#overlayForMobileMenu').toggleClass('displayOverlay');
    $('.toggle-btn').click();
  });  

  //Check to see if the window is top if not then display button
  $(window).scroll(function(){

  });
  
  //Click event to scroll to top
  $('.scrollToTop').click(function(){
    $('html, body').animate({scrollTop : 0},800);
    return false;
  });


  
});  
</script>
<!-- About us -> Leader Ship -->
<script type="text/javascript">
  function openSearchOverlay(){
      $('#searchOverlay').css('display','block');
      $(".gsc-search-box input[type='text']").focus();
  };
  function hideSearchOverlay(){
      $('#searchOverlay').css('display','none');
  };

  $(document).ready( function() {
    $('#leadCarousel').carousel({
      interval:  false
  });
  
  var leadclickEvent = false;
  $('#leadCarousel').on('click', '.submenu a', function() {
      leadclickEvent = true;
      $('.submenu li').removeClass('active');
      $(this).parent().addClass('active');

  }).on('slid.bs.carousel', function(e) {
    if(!leadclickEvent) {
      var count = $('.submenu').children().length -1;
      var current = $('.submenu li.active');
      current.removeClass('active').next().addClass('active');
      var id = parseInt(current.data('slide-to'));
      if(count == id) {
        $('.submenu li').first().addClass('active');  
      }
    }
    leadclickEvent = false;
  });

var is_expired = 0;
$(function (){
var timer = setInterval(hidead, 100);
if (is_expired > 0) {
 clearInterval(timer);
}
}); 

function hidead() {
  $(".gsc-adBlock").hide();
};

});

</script>
<!-- About us -> Leader Ship :: End -->
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />


</body></html>
