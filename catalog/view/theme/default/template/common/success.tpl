<?php echo $header; ?>
<div class="container">
    <div id="home-text" class="breadcrumb">
      <h2><?php echo $heading_title; ?></h2>
      <ul>
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1 class="hidden"><?php echo $heading_title; ?></h1>
      <?php if(isset($info_last_order)){ ?>
      <p>
          <span>Mã đơn hàng: </span><span style="color: red;"><?php echo $info_last_order['order_id']; ?></span><br />
          <span>Qúy khách vui lòng chuyển khoản với nội dung:</span><span style="color: red;"> Dat coc/thanh toan don hang <b>#<?php echo $info_last_order['order_id'];  ?></b></span>
      </p>
      <hr />
      <?php } ?>
      <?php echo $text_message; ?>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>