<?php if (count($languages) > 1) { ?>
<div class="head-language">
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-language" style="width: 100%;position: relative;float: left;height: 56px;">
      <ul style="width: 100%;position: relative;float: left;">
        <?php foreach ($languages as $language) { ?> 
         <li style="background-color: <?php echo ($language['code'] == $code) ? '#c3c3c3;display: none;' : 'display: block;' ?>;">
         	<button class="btn btn-link btn-block language-select" type="button" name="<?php echo $language['code']; ?>">
         		<?php /* echo ($language['code'] == 'vi-vn') ? 'VI' : 'EN';*/ ?>
         		<img style="position: relative; display: inline-block;width: 30px;" src="catalog/language/flag/<?php echo $language['code']; ?>.png" alt="<?php echo $language['name']; ?>" title="<?php echo $language['name']; ?>" /><br />
         		<h2 class="hidden-xs hidden-sm"><?php echo ($language['code'] == 'vi-vn') ? 'Language' : 'Ngôn ngữ'; ?></h2>
         	</button>
         </li>
        <?php } ?>
      </ul>
    <input type="hidden" name="code" value="" />
    <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
  </form>
</div>
<?php } ?>
