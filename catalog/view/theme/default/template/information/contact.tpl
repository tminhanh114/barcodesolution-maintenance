<?php echo $header; ?>
<div class="row cat-image-title" >
    <img src="image/catalog/7_banner/contact-banner.jpg" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>"  />  
    <div class="contact-title">
        <h2 class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="display: inline-block;"><?php echo $heading_title; ?></h2>
    </div>
</div>
<div class="container" id="content" style="position: relative;float: left;width: 100%;background: url(catalog/view/theme/default/image/jhe/pattern2.jpg) 0 0 repeat;padding-top: 70px;">
  <div class="row" style="width: 100%;max-width: 1200px;position: relative;margin: 0 auto;">
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: center;padding-right: 10px;">
        <a class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" rel="nofollow" target="_blank" href="#"><img style="width: 100%;max-width: 200px;" src="http://beetech.com.vn/image/data/8_system/beetech-h80.png" alt="" /></a>
        <span style="font: 400 18px/36px 'open sans',arial;text-transform: uppercase;color: #0e75d3;"><?php echo $text_companyname; ?></span><br />
        <span style="font: 300 15px/20px arial;"><?php echo $text_address; ?></span><br/>
        <span style="font: 300 15px/20px arial;"><?php echo $text_telephone; ?></span><br/>
        <span style="font: 300 15px/20px arial;"> Email: info@beetech.com.vn</span><br/>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="require-col">
            <div class="input-text col-sm-12" style="margin-bottom: 32px">
                <input name="input-name" placeholder="<?php echo  $entry_name;  ?>" id="input-name" type="text" class="form-control">
            </div>

            <div class="col-sm-6 input-text" style="margin-bottom: 32px; padding-left: 0px;">
                <input name="input-email" placeholder="<?php echo  $entry_email;  ?>" id="input-email" type="text"  class="form-control">
            </div>
            <div class="col-sm-6 input-text" style="margin-bottom: 32px; padding-left: 0px;">
                <input name="input-email" placeholder="<?php echo  $entry_phone;  ?>" id="input-phone" type="text"  class="form-control">
            </div>
            <div class="col-sm-6 hidden" style="margin-bottom: 32px">
              <select class="form-control" name="input-department" id="input-department">
                <option value="0" selected="selected"><?php echo  $entry_department;  ?></option>
                <option value="1"><?php echo  $entry_department_1;  ?></option>
                <option value="2"><?php echo  $entry_department_2;  ?></option>
              </select>
            </div>
            <div class="input-area col-sm-12" style="margin-bottom: 32px">
                <textarea id="input-enquiry" placeholder="<?php echo  $entry_enquiry;  ?>" name="enquiry"  class="form-control"></textarea>
            </div>

            <div class="input-but pull-right">
                <input style="padding: 7px 20px;text-transform: uppercase;margin-bottom: 15px;" onclick="" id="ss-submit-in" type="button" value="<?php echo $entry_submit; ?>" class="btn btn-primary">
            </div>
        </div>

        <iframe name="hidden_iframe" id="hidden_iframe" style="display:none;" onload="if(submitted){};" ></iframe>
        <form style="display:none;" action="" method="POST" id="ss-form" target="hidden_iframe" onsubmit="">
            <input type="text" id="form-name" name=""  value="" ><br />
            <input type="text" id="form-email" name=""  value="" ><br />
            <input type="text" id="form-enquiry" name=""  value="" ><br />
            <input type="text" id="text_message" name="text_message"  value="<?php echo  $text_message;  ?>" ><br />
            <input type="submit" name="submit" value="Gửi đi" id="ss-submit" class="jfk-button jfk-button-action">
        </form>

    </div>
  </div>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3918.7969417561844!2d106.71676791440937!3d10.826845992287327!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x317528815d401329%3A0xc96f79e11a244920!2zMzAgxJDGsOG7nW5nIFPhu5EgMTgsIEhp4buHcCBCw6xuaCBDaMOhbmgsIFRo4bunIMSQ4bupYywgSOG7kyBDaMOtIE1pbmgsIFZpZXRuYW0!5e0!3m2!1sen!2s!4v1569334962841!5m2!1sen!2s" width="100%" height="350" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
</div>

<!-- script for form  -->
<script type="text/javascript">
$(document).ready( function() {
    function trim(str) {
        return str.replace(/^\s+|\s+$/g,"");
    }
    function reg_valid(){
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/; 

        if ($('#input-name').val() == ""){
            $('#input-name').css('border-left','1px solid red');
            return false;    
        } 
        if ($('#input-email').val() == ""){
            $('#input-email').css('border-left','1px solid red');
            return false;    
        }
        if (!filter.test($('#input-email').val())){
            $('#input-email').css('border-left','1px solid red');
            return false;    
        } 
        if ($('#input-enquiry').val() ==""){
            $('#input-enquiry').css('border-left','1px solid red');
            return false;    
        }

        if ($('#input-department').val() == 0){
            $('#input-department').css('border-left','1px solid red');
            return false;    
        }

        $('.form-control').css('border-left','1px solid #ccc');

        return true;
    }
    $('#ss-submit-in').click(function(e){
       if(reg_valid()){
            if($('#input-department').val() == 1){
                $('#ss-form').attr('action','https://docs.google.com/forms/d/e/1FAIpQLSfAdDb3e0CQa3pHeufYQ6G77qW1IxqI1gei7VLraAI2Bck9wg/formResponse');
                $('#form-name').attr('name','entry.879566548');   
                $('#form-email').attr('name','entry.929323704');   
                $('#form-enquiry').attr('name','entry.2142049280'); 

                $('#form-name').val($('#input-name').val()); $('#input-name').val(''); 
                $('#form-email').val($('#input-email').val()); $('#input-email').val('');
                $('#form-enquiry').val($('#input-enquiry').val()); $('#input-enquiry').val($('#text_message').val());

                $('#ss-submit').click();$('#form-enquiry').val(''); $('#form-email').val('');  $('#form-name').val(''); 
            }else{
                $('#ss-form').attr('action','https://docs.google.com/forms/d/e/1FAIpQLSdZsyaV8DixJjD4SPVesArUKgB1kBG45Y8yM8j-TSzQ3xic6w/formResponse');
                $('#form-name').attr('name','entry.879566548');   
                $('#form-email').attr('name','entry.929323704');   
                $('#form-enquiry').attr('name','entry.2142049280'); 

                $('#form-name').val($('#input-name').val()); $('#input-name').val(''); 
                $('#form-email').val($('#input-email').val()); $('#input-email').val('');
                $('#form-enquiry').val($('#input-enquiry').val()); $('#input-enquiry').val($('#text_message').val());

                $('#ss-submit').click();$('#form-enquiry').val(''); $('#form-email').val('');  $('#form-name').val(''); 
            }
       } 
    });


});
  

</script>
<!-- script for form :: end -->


<?php echo $footer; ?>
