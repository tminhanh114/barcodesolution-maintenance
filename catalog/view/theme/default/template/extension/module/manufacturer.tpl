<div class="list-group" style="width: 100%; max-width: 1200px;position: relative;margin: 0 auto;padding: 10px 0px;">
	<span class="list-group-item active hidden"><?php echo $manufacturer_heading; ?></span>
	<div class="owl-carousel owl-theme manufacturer-group"  id="manufacturer-group">
	  <?php foreach ($manufacturers as $manufacturer) {  ?>
	  	<div  class="item text-center" style="padding: 1px; width: 100%;">
		  <a href="<?php echo $manufacturer['href']; ?>" class="list-group-item">
		  <?php if($manufacturer_image_status == 1) { ?>
		  	<img src="<?php echo $manufacturer['image']; ?>" style="width: 100%;">
		  <?php } ?>
		  <span class="hidden"><?php echo $manufacturer['name']; ?></span></a>
		</div>
	  <?php } ?>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){

    var owlmf = $("#manufacturer-group");
    owlmf.owlCarousel({
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem: true,
        pagination: false,
        rewindSpeed: 500,
        dots: false,
        autoplay:true,
        autoplayTimeout:2000,
        autoplayHoverPause:true,
        loop: true,
        responsive:{
            0:{
              items:3,
              nav:true,
              slideBy: 1
          },
          600:{
              items:4,
              nav:false,
              slideBy: 3
          },
          1000:{
              items:7,
              nav:true,
              loop:false,
              slideBy:5
          }
        },
        navText : ['<i class="material-icons">arrow_back_ios</i>','<i class="material-icons">arrow_forward_ios</i>']   
    });
});
</script>
