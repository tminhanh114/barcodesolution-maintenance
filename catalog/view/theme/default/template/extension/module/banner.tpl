<div class="row">
    <?php if ($banner_show_title) { ?>
    <h5 class="right-title"><?php echo $heading_title; ?></h5>
    <?php } ?>
    <div class="owl-carousel owl-theme"  id="banner<?php echo $module; ?>" style="width: 100%;">
      <?php for ($key = 0; $key < count($newbanners);$key++) { ?>
      <div class="item text-center" style="padding: 1px;width: 100%;">
        <?php if ($newbanners[$key]['link']) { ?>
        <a href="<?php echo $newbanners[$key]['link']; ?>" style="margin-bottom: 2px;position: relative;float: left;width: 100%;">
            <img style="width: 100%;" src="<?php echo $newbanners[$key]['image']; ?>" alt="<?php echo $newbanners[$key]['title']; ?>" class="img-responsive" />
            <h2 style="font-size: 18px;line-height: 1.3em;"><?php echo $newbanners[$key]['title']; ?></h2>
        </a>
        <?php } else { ?>
            <img  style="margin-bottom: 2px;width: 100%;" src="<?php echo $newbanners[$key]['image']; ?>" alt="<?php echo $newbanners[$key]['title']; ?>" class="img-responsive" />
            <h2 style="font-size: 18px;line-height: 1.3em;"><?php echo $newbanners[$key]['title']; ?></h2>
        <?php } ?>
      </div>
      <?php } ?>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){

    var bn<?php echo $module; ?> = $("#banner<?php echo $module; ?>");
    bn<?php echo $module; ?>.owlCarousel({
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem: true,
        pagination: false,
        rewindSpeed: 500,
        items : 1,
        dots: false,
        autoplay:true,
        autoplayTimeout:3000,
        autoplayHoverPause:true,
        loop: true
    }); 
});
</script>