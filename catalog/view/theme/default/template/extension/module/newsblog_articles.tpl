<div class="news-list-main-home" style="">
<?php if ($heading_title) { ?>
<H5 style="font: 300 30px/30px 'open sans',verdana;padding: 35px 0px 15px 0px;text-transform: uppercase;"><?php echo $heading_title; ?></H5>
<?php } ?>
<?php if ($html) { ?>
<?php echo $html; ?>
<?php } ?>
<div class="row" style="width: 100%;max-width: 1200px;margin: 0 auto;">
  <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 main-box">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 main-news">
      <?php if ($articles[0]['thumb']) { ?>
        <div class="image transition" ><a class="news-list-main-image-a" href="<?php echo $articles[0]['href']; ?>"><img class="news-list-main-image-img kenburns-top" src="<?php echo $articles[0]['thumb']; ?>" alt="<?php echo $articles[0]['name']; ?>" title="<?php echo $articles[0]['name']; ?>" class="img-responsive" style="width: 100%;" /></a></div>
      <?php } ?>
      <div class="caption col-lg-12 news-list-main-cap" style="background: #ebebeb;padding: 15px;">
        <a href="<?php echo $articles[0]['href']; ?>"><h4><?php echo $articles[0]['name']; ?></h4></a>
        <p><?php echo $articles[0]['preview']; ?></p>
      </div>        
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 next-news" >
    <?php if ($articles[1]['thumb']) { ?>
      <a  href="<?php echo $articles[1]['href']; ?>"><img src="<?php echo $articles[1]['thumb']; ?>" alt="<?php echo $articles[1]['name']; ?>" title="<?php echo $articles[1]['name']; ?>" class="img-responsive" /></a>
      <a style="padding: 15px 10px; background: #ebebeb;position: relative;float: left;width: 100%;display: block;text-align: center;" href="<?php echo $articles[1]['href']; ?>"><h4 ><?php echo $articles[1]['name']; ?></h4></a>
    <?php } ?>        
    </div>    
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 next-news" >
    <?php if ($articles[2]['thumb']) { ?>
      <a  href="<?php echo $articles[2]['href']; ?>"><img src="<?php echo $articles[2]['thumb']; ?>" alt="<?php echo $articles[2]['name']; ?>" title="<?php echo $articles[2]['name']; ?>" class="img-responsive" /></a>
      <a style="padding: 15px 10px; background: #ebebeb;position: relative;float: left;width: 100%;display: block;text-align: center;" href="<?php echo $articles[2]['href']; ?>"><h4 > <?php echo $articles[2]['name']; ?></h4></a>
    <?php } ?>        
    </div>
      
  </div>
  <div class="col-lg-4 col-md-4 hidden-sm hidden-xs" style="padding: 10px 0px 10px 5px;background: white;">
      <ul class="nav nav-tabs">
        <?php foreach($categories as $key => $category){ ?>
            <li class="<?php echo ($key == 0) ? 'active' : ''; ?>"><a href="#<?php echo $category['category_id']; ?>" data-toggle="tab"><?php echo $category['category_name']; ?></a></li>    
        <?php } ?>
      </ul>
      <div class="tab-content clearfix">
        <?php foreach($categories as $key => $category){ ?>
        <div class="tab-pane <?php echo ($key == 0) ? 'active' : '';  ?>" id="<?php echo $category['category_id']; ?>">
          <?php foreach ($articles as $article) {
            if(($article['article_id'] == $articles[0]['article_id']) || ($article['article_id'] == $articles[1]['article_id']) || ($article['article_id'] == $articles[0]['article_id'])) continue;
            if($article['mainCategoryId'] == $category['category_id']){
          ?>
            <div class="product-layout col-xs-12" style="border-bottom: 1px solid #e1e1e1;">
              <div class="transition">
                <div class="caption">
                  <h4 style="text-align: left;font: 600 15px/20px 'open sans',verdana;padding-bottom: 5px;"><a href="<?php echo $article['href']; ?>"><?php echo $article['name']; ?></a></h4>
                  <p style="font: 300 15px/20px 'open sans',verdana"><?php echo $article['preview']; ?></p>
                </div>
              </div>
            </div>
          <?php }
          } ?>  
        </div>        
        <?php } ?>
      </div>
  </div>
</div>
<?php if ($link_to_category) { ?>
<a class="hidden"> href="<?php echo $link_to_category; ?>"><i class="fas fa-arrow-right" style="color: red;"></i></a>
<?php } ?>
</div>