<div class="row" style="margin-bottom: 10px;">
    <?php if ($banner_show_title) { ?>
    <h5 class="right-title"><?php echo $heading_title; ?></h5>
    <?php } ?>
    <div class="banner-grid-theme"  id="banner<?php echo $module; ?>" style="width: 100%;">
      <?php for ($key = 0; $key < count($newbanners);$key++) { ?>
      <div class="row text-center col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding: 5px;">
        <?php if ($newbanners[$key]['link']) { ?>
        <a href="<?php echo $newbanners[$key]['link']; ?>" style="margin-bottom: 2px;position: relative;float: left;width: 100%;">
            <img style="width: 100%;" src="<?php echo $newbanners[$key]['image']; ?>" alt="<?php echo $newbanners[$key]['title']; ?>" class="img-responsive" />
            <h2 style="font-size: 14px;line-height: 1.3em;"><?php echo $newbanners[$key]['title']; ?></h2>
        </a>
        <?php } else { ?>
            <img  style="margin-bottom: 2px;width: 100%;" src="<?php echo $newbanners[$key]['image']; ?>" alt="<?php echo $newbanners[$key]['title']; ?>" class="img-responsive" />
            <h2 style="font-size: 14px;line-height: 1.3em;"><?php echo $newbanners[$key]['title']; ?></h2>
        <?php } ?>
      </div>
      <?php } ?>
    </div>
</div>
