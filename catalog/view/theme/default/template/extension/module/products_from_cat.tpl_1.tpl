  
<div class="feature-div products-from-cat-div">
<h2><?php echo $heading_title; ?></h2>
  <!-- template 01-->
  <div class="owl-carousel owl-theme feature-products products-category-home"  id="products-from-cat-<?php echo $module; ?>">
    <?php foreach ($products as $key => $product) { ?>
      <div class="item text-center" style="padding: 1px;">
        <?php if($products['khuyenmai'] && ($key == 0){ ?>
          <!--khuyen mai bang san pham-->
          <a class="row" href="<?php echo '';// $product['href']; ?>" style="">
              <img src="<?php echo $product['thumb'][1]; ?>" alt="<?php echo $product['name'] . ' ' . $key; ?>" class="img-responsive" />
          </a>
          <div class="row info">
            <a href="<?php echo '';// $product['href']; ?>"><h3 style="text-align: left;" ><?php echo $product['name']; ?></h3></a>
            <div class="discount-text"><?php echo $product['meta_description']; ?></div>
          </div>  
          <!--khuyen mai bang san pham :: end-->
        <?php }esle{ ?>
          <a class="row" href="<?php echo '';// $product['href']; ?>" style="">
              <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" class="img-responsive" />
              <?php if ($product['special']) { ?>
                <label class="home-discount">Giảm <?php echo $product['downpricerate']; ?></label>  
              <?php } ?> 
          </a>       
          <div class="row info">
            <a href="<?php echo '';// $product['href']; ?>"><h3 style="text-align: left;" ><?php echo $product['name']; ?></h3></a>
            <?php if ($product['price']) { ?>
            <p class="price">
              <?php if (!$product['special']) { ?>
                <strong><?php echo $product['price']; ?></strong>
              <?php } else { ?>
                <strong><?php echo $product['special']; ?></strong><span class="price-old"><?php echo $product['price']; ?></span>
              <?php } ?>
            </p>
            <div class="discount-text"><?php echo $product['meta_description']; ?></div>
            <?php } ?>
          </div>        
        <?php } ?>
            

      </div>
    <?php } ?>
  </div>
  <!-- -->
</div>
<script type="text/javascript">
$(document).ready(function(){

    var fp = $("#products-from-cat-<?php echo $module; ?>");
    fp.owlCarousel({
        navigation : false, 
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem: true,
        pagination: false,
        rewindSpeed: 500,        
        dots: false,
        nav: false,
        responsive:{
          0:{
              items:1,
              nav:true,
              slideBy: 1
          },
          600:{
              items:3,
              nav:false,
              slideBy: 3
          },
          1000:{
              items:5,
              nav:true,
              loop:false,
              slideBy:5
          }
        } 
    }); 
});
</script>