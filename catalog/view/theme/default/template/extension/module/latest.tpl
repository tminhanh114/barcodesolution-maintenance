<div class="row latest-div" style="width: 100%">
  <h5><?php echo $heading_title; ?></h5>
  <?php foreach ($products as $product) { ?>
  <div class="product-item" style="width: 100%;">
    <div class="transition">
      <div class="featured-image col-lg-4 col-md-4 col-sm-4 col-xs-4">
        <a class="display-img" href="<?php echo $product['href']; ?>"><img width="100%" src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a>
        <a class="display-des hidden"  href="<?php echo $product['href']; ?>"><p class="home-product-description"><?php echo $product['description']; ?></p></a>
      </div>

      <div class="caption col-lg-8 col-md-8 col-sm-8 col-xs-8">
        <h4 style="font-size: 16px;"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
        <?php if ($product['price']) { ?>
        <p class="price">
          <?php if (!$product['special']) { ?>
            <strong><?php echo $product['price']; ?></strong>
          <?php } else { ?>
            <strong><?php echo $product['special']; ?></strong><span class="price-old hidden"><?php echo $product['price']; ?></span>
          <?php } ?>
        </p>
        <?php } ?>
      </div>
      <div class="button-group hidden">
        <button style="width: 100%;" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-shopping-cart"></i>&nbsp;&nbsp;&nbsp; <span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>
      </div>
    </div>
  </div>
  <?php } ?>
</div>