  
<div class="feature-div">
<h2><?php echo $heading_title; ?></h2>
  <!-- -->
  <div class="owl-carousel owl-theme feature-products"  id="feature-products">
    <?php foreach ($products as $key => $product) { ?>
      <div class="item text-center" style="padding: 1px;">
        <?php if ($product['href']) { ?>
        <a class="row" href="<?php echo '';// $product['href']; ?>" style="">
            <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" class="img-responsive" />
            <label class="home-discount">Giảm 1.000.000 đ</label>
        </a>
        <?php } ?>
            
        <div class="row info" style="" >
          <a href="<?php echo '';// $product['href']; ?>"><h3 style="text-align: left;" ><?php echo $product['name']; ?></h3></a>
          <?php if ($product['price']) { ?>
          <p class="price">
            <?php if (!$product['special']) { ?>
              <strong><?php echo $product['price']; ?></strong>
            <?php } else { ?>
              <strong><?php echo $product['special']; ?></strong><span class="price-old"><?php echo $product['price']; ?></span>
            <?php } ?>
          </p>
          <?php } ?>
        </div>
      </div>
    <?php } ?>
  </div>
  <!-- -->
</div>
<script type="text/javascript">
$(document).ready(function(){

    var fp = $("#feature-products");
    fp.owlCarousel({
        navigation : false,
        navigationText : false, 
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem: true,
        pagination: false,
        rewindSpeed: 500,        
        dots: false,
        responsive:{
          0:{
              items:2,
              nav:true,
              navigationText : false 
          },
          600:{
              items:3,
              nav:false,
              navigationText : false
          },
          1000:{
              items:5,
              nav:true,
              loop:false,
              navigationText : false
          }
        } 
    }); 
});
</script>