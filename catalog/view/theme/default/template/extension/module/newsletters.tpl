<script>
	function regNewsletter()
	{
		var emailpattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		var email = $('#txtemail').val();
		
		if(email != "")
		{
			if(!emailpattern.test(email))
			{
				$("#text-danger-newsletter").remove();
				$("#form-newsletter-error").removeClass("has-error");
				$("#newsletter-email").append('<div class="text-danger" id="text-danger-newsletter"><?php echo $error_news_email_invalid; ?></div>');
				$("#form-newsletter-error").addClass("has-error");

				return false;
			}
			else
			{
				$.ajax({
					url: 'index.php?route=extension/module/newsletters/add',
					type: 'post',
					data: 'email=' + $('#txtemail').val(),
					dataType: 'json',
					async:false,

					success: function(json) {

						if (json.message == true) {
							alert('<?php echo $error_newsletter_sent; ?>');
							document.getElementById("form-newsletter").reset();
							return true;						
						}
						else {
							$("#text-danger-newsletter").remove();
							$("#form-newsletter-error").removeClass("has-error");
							$("#newsletter-email").append(json.message);
							$("#form-newsletter-error").addClass("has-error");
						}
					}
				});
				return false;
			}
		}
		else
		{

			$("#text-danger-newsletter").remove();
			$("#form-newsletter-error").removeClass("has-error");
			$("#newsletter-email").append('<div class="text-danger" id="text-danger-newsletter"><?php echo $error_news_email_required; ?></div>');
			$("#form-newsletter-error").addClass("has-error");

			return false;
		}
	}

</script>

<div class="row newsletters" style="position: relative; float: left; width: 100%;background: white;padding-top: 20px;margin-top: 10px;">
	<div class="row tuvan-div" style="position: relative; margin: 0 auto;width: 100%;max-width: 900px;background: rgba(255, 255, 0, 0.3);box-shadow: 0 0 9px 3px rgba(0, 0, 0, 0.3);display: none;">
		<div class="col-md-8">
			<h6 class="text-left" style="color: black;"><span><?php echo $text_when; ?></span></h6>
			<p class="text-left no-margin" style="font-size: 16px;"><?php echo $text_when_comment; ?></p>
		</div>
		<div class="col-md-4">
			<div class="text-center">
				<a class="" href="tel:+84984689669" style="color: #fff; font-size: 30px; font-weight: 300;line-height: 90px;">
				<i class="material-icons" style="font-size: 40px;color: #ffdb38;">local_phone</i>0984689669</a>
			</div>
		</div>
	</div>

	<div class="row" style="width: 100%;max-width: 1200px;margin: 0 auto;width: 100%; position: relative; padding: 10px 5px 20px 5px;">
		<div class="row col-lg-3 col-md-3 col-sm-6 col-xs-12" style="padding: 5px; display: none;">
			<h6 style="color: #ffda00;text-align: left;font: 400 18px/22px 'open sans'; text-transform: uppercase;"><?php echo $text_subscribe_title; ?></h6>
			<p><?php echo $text_subscribe_comment; ?></p>
			<form action="" method="post" class="form-horizontal" id="form-newsletter">
				<div id="form-newsletter-error">
					<div class="col-xs-8 col-sm-8 col-lg-8 col-md-8" id="newsletter-email">
						<input type="email" name="txtemail" id="txtemail" value="" placeholder="<?php echo $text_subscribe_placeholder; ?>" class="form-control" style="width: 100%;"/>
						<span class="col-xs-2"></span>
					</div>				
					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"><a onclick="return regNewsletter();" style="font: 300 15px/32px 'open sans'; padding: 0 7px; display: inline-block; background-color: #34a3d6;color: white;text-transform: uppercase; border-radius: 5px;margin-left: 5px;cursor: pointer;"><?php echo $text_subscribe_btn; ?></a></div>
				</div>
			</form>
		</div>
		<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 footer-info" style="padding: 5px 0px;">
                <h3 class="footer-company-name">CÔNG TY TNHH BARCODE SOLUTION VIỆT NAM</h3>
                <p class="color-white hidden" style="font: 300 14px/18px 'open sans',arial;color: white;"></p>
                <div class="contact-phonefax"><i class="material-icons" style="font-size: 15px;">location_on</i>&nbsp;&nbsp; 362/56 Nguyễn Đình Chiểu, P. 4, Quận 3, HCMC</div>
                <div class="contact-phonefax"><i class="material-icons" style="font-size: 15px;">phone</i>&nbsp;&nbsp; <a href="tel:(84)0984689669" style="font: inherit !important; color: inherit;text-decoration: none;">(84)0984689669</a></div>             
                <div class="contact-phonefax"><i class="material-icons" style="font-size: 15px;">email</i>&nbsp;&nbsp; info@barcodesolution.com.vn </div>             
        </div>

		<div class="row col-lg-3 col-md-3 col-sm-6 col-xs-12" style="padding: 5px; display: none;">
	         <h6 style="color: #ffda00;text-align: left;font: 400 18px/22px 'open sans'; text-transform: uppercase;"><?php echo $text_brochure_title; ?></h6>
	         <a class="col-lg-12 col-md-12 col-sm-6 col-xs-6" data-toggle="modal" href="#normalModal" style="text-align: center;"><img src="catalog/view/theme/default/image/jhe/download-brochure-1.png" style="width: 80%;"></a>   
	         <p class="col-lg-12 col-md-12 col-sm-6 col-xs-6"><?php echo $text_brochure_comment; ?></p>

			<div id="normalModal" class="modal fade">
			  	<div class="modal-dialog">
				    <div class="modal-content col-sm-10 col-md-10" style="width: max-content;">
				      	<div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					        <h4  style="display: inline-block;line-height: 30px;" class="modal-title"><?php echo $text_brochure_title; ?></h4>
					        <a style="display: inline-block;line-height: 30px;" href="http://www.rfidsolution.com.vn"><img width="30px" src="image/catalog/system/download-2.png"></a>
			      		</div>
						<div class="modal-body">
							<iframe src="https://drive.google.com/file/d/1SnH0SHbU1PT0libXxigZmF9f7qY81ayM/preview" width="640" height="480"></iframe>
						</div>		      
			    	</div><!-- /.modal-content -->
		  		</div><!-- /.modal-dialog -->
			</div><!-- /.modal -->

    	</div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 footer_hub" style="padding: 0px; display: none;">
		 <div class="row col-lg-6 col-md-6 col-sm-6 col-xs-12 hub_left" style="box-sizing: border-box;">
		    <h6 style="color: #ffda00;text-align: left;font: 400 18px/22px 'open sans';"><?php echo $footer_hub['text_news']; ?></h6>
		    <ul class="sitemap-home">
		       <li><a class="transition" href="<?php echo $footer_hub['news_hbcnews']['link']; ?>" target="_self" style="width: 100%"><?php echo $footer_hub['news_hbcnews']['text']; ?></a></li>
		       <li><a class="transition" href="<?php echo $footer_hub['news_jescoasianews']['link']; ?>" target="_self" style="width: 100%"><?php echo $footer_hub['news_jescoasianews']['text']; ?></a></li>
		       <li><a class="transition" href="<?php echo $footer_hub['news_jescohoabinhnews']['link']; ?>" target="_self" style="width: 100%"><?php echo $footer_hub['news_jescohoabinhnews']['text']; ?></a></li>
		       <li><a class="transition" href="<?php echo $footer_hub['news_careers']['link']; ?>" target="_self" style="width: 100%"><?php echo $footer_hub['news_careers']['text']; ?></a></li>
		    </ul>
		 </div>
		 <div class="row col-lg-6 col-md-6 col-sm-6 col-xs-12 hub_right" style="box-sizing: border-box;">
		    <h6 style="color: #ffda00;text-align: left;font: 400 18px/22px 'open sans';"><?php echo $footer_hub['activity_sustainability']; ?></h6>
		    <ul class="sitemap-home">
		    	<li><a class="transition" href="<?php echo $footer_hub['corporate_governance']['link']; ?>" target="_self" style="width: 100%"><?php echo $footer_hub['corporate_governance']['text']; ?></a></li>
		    	<li><a class="transition" href="<?php echo $footer_hub['investor_relation']['link']; ?>" target="_self" style="width: 100%"><?php echo $footer_hub['investor_relation']['text']; ?></a></li>
		    	<li><a class="transition" href="<?php echo $footer_hub['announcements']['link']; ?>" target="_self" style="width: 100%"><?php echo $footer_hub['announcements']['text']; ?></a></li>
		    	<li><a class="transition" href="<?php echo $footer_hub['investor_activity']['link']; ?>" target="_self" style="width: 100%"><?php echo $footer_hub['investor_activity']['text']; ?></a></li>
		    </ul>
		 </div>
		</div>    
	</div>
</div>