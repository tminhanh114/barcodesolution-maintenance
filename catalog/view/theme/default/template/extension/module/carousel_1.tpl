<!-- Slide box, hot news tgdd -->
<div class="row first-layer" style="width: 100%;max-width: 1200px;position: relative;margin: 0 auto;background: white;">
    <div class="carousel-first-div col-lg-8 col-md-12 col-sm-12 col-xs-12" style="padding: 0 5px 0px 0px;">
        <div class="owl-carousel owl-theme feature-project"  id="carousel-first-<?php echo $module; ?>" style="width: 100%;">
          <?php for ($key = 0; $key < (count($banners) - 3);$key++) { ?>
          <div class="item text-center" style="padding: 1px;width: 100%;">
            <?php if ($banners[$key]['link']) { ?>
            <a href="<?php echo $banners[$key]['link']; ?>" style="margin-bottom: 2px;position: relative;float: left;width: 100%;">
                <img style="width: 100%;" src="<?php echo $banners[$key]['image']; ?>" alt="<?php echo $banners[$key]['title']; ?>" class="img-responsive" />
            </a>
            <?php } else { ?>
                <img  style="margin-bottom: 2px;width: 100%;" src="<?php echo $banners[$key]['image']; ?>" alt="<?php echo $banners[$key]['title']; ?>" class="img-responsive" />
            <?php } ?>
          </div>
          <?php } ?>
        </div>
        <div class="custom-control hidden-md hidden-sm hidden-xs" style="position: relative;float: left;width: 100%;">
            <div class="owl-carousel owl-theme"  id="first-control-<?php echo $module; ?>">
                <?php for ($key = 0; $key < (count($banners)-3);$key++) { ?>
                    <div class="item text-center" data-slide="<?php echo $key; ?>" class="<?php echo ($key == 0) ? 'active' : ''; ?>"><a style=""><?php echo $banners[$key]['title']; ?></a></div>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="col-lg-4 hidden-md hidden-sm hidden-xs carousel-first-adv">
        <h2 style="margin-bottom: 5px;">Hot News</h2>
        <a id="hot-news" title="" href="#" class="liveevent card" target="_blank">
          <div class="ring-container"><div class="ringring"></div><div class="circle"></div></div><span class="text"><?php echo $latest_article['name']; ?></span>
        </a>
        <?php for ($key = count($banners)-3; $key < count($banners);$key++) { ?>
            <img style="width: 100%;margin-bottom: <?php echo ($key == count($banners) -1) ? '0' : '0'; ?>px;" src="<?php echo $banners[$key]['image']; ?>" alt="<?php echo $banners[$key]['title']; ?>" />
        <?php } ?>
    </div>
    <div class="row hidden-lg col-md-12 col-sm-12 col-xs-12 " >
        <div class="owl-carousel owl-theme menu-carousel-home"  id="menu-carousel-home">
          <div class="item text-center" ><a href="<?php echo $main_menu['scanner']['link']; ?>" ><img style="width: 100%;" src="image/catalog/system/barcode_scanner.png"  /></a><h2>Scanner</h2></div>
          <div class="item text-center" ><a href="<?php echo $main_menu['printer']['link']; ?>" ><img src="image/catalog/system/barcode_printer.png"  /></a><h2>Printer</h2></div>            
          <div class="item text-center" ><a href="<?php echo $main_menu['accessories']['link']; ?>" ><img src="image/catalog/system/accessories.png"  /></a><h2>Accessories</h2></div>            
          <div class="item text-center" ><a target="_blank" href="http://rfidsolution.com.vn/"><img src="image/catalog/system/rfid.png" /></a><h2>Rfid</h2></div>            
          <div class="item text-center" ><a href="<?php echo $main_menu['promotion']['link']; ?>" ><img src="image/catalog/system/gif.png" /></a><h2>Promotion</h2></div>            
          <div class="item text-center" ><a href="<?php echo $main_menu['solution']['link']; ?>" ><img style="width: 100%;" src="image/catalog/system/solution.png" /></a><h2>Solution</h2></div>            
          <div class="item text-center" ><a href="<?php echo $main_menu['rent']['link']; ?>" ><img style="width: 100%;" src="image/catalog/system/rent-6.png" /></a><h2>Rent</h2></div>            
          <div class="item text-center" ><a href="<?php echo $main_menu['repair']['link']; ?>" ><img style="width: 100%;" src="image/catalog/system/repair.png" /></a><h2>Repair</h2></div>            
          <div class="item text-center" ><a href="news/news-01" ><img style="width: 100%;" src="image/catalog/system/news.png" /></a><h2>News</h2></div>            
          <div class="item text-center" ><a href="<?php echo $main_menu['contact']['link']; ?>" ><img style="width: 100%;" src="image/catalog/system/contact.png" /></a><h2>Contact</h2></div>
        </div>
    </div>
</div>
<div class="promotebanner hidden-xs hidden-sm" style="margin: 0 auto;    position: relative; width: 100%;max-width: 1200px; margin-top: 10px;">
    <a aria-label="slide" href="#" style="display: block;"><img src="image/catalog/7_banner/banner-3.jpg" alt="quang cao san pham" style="width:100%;"></a>
</div>

<script type="text/javascript">
$(document).ready(function(){

    var owlfd = $("#carousel-first-<?php echo $module; ?>");
    owlfd.owlCarousel({
        navigation : false, 
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem: true,
        pagination: false,
        rewindSpeed: 500,
        items : 1,
        dots: false,
        autoplay:true,
        autoplayTimeout:2000,
        autoplayHoverPause:true,
        loop: true 
    });

    $('.custom-control').height($('.first-layer').height() - $('.feature-project').height());
    $('.custom-control .item').height($('.first-layer').height() - $('.feature-project').height());

    var owlctr = $("#first-control-<?php echo $module; ?>");
    owlctr.owlCarousel({
        navigation : false, 
        navigationText : false,
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem: true,
        pagination: false,
        rewindSpeed: 500,
        dots: false,
        responsive:{
            0:{
              items:2,
              nav:true,
              slideBy: 1
          },
          600:{
              items:3,
              nav:false,
              slideBy: 3
          },
          1000:{
              items:5,
              nav:true,
              loop:false,
              slideBy:5
          }
        }  
    });
        
    $('.custom-control').on('click', 'div', function() {
        var $this = $(this);
        var slideNum = $(this).data('slide');        
        owlfd.trigger('to.owl.carousel',[slideNum,300]);
    });

    var owlmnuhome = $("#menu-carousel-home");
    owlmnuhome.owlCarousel({
        navigation : false, 
        navigationText : false,
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem: true,
        pagination: false,
        rewindSpeed: 500,
        dots: false,
        responsive:{
            0:{
              items:5,
              nav:true,
              slideBy: 1
          },
          700:{
              items:7,
              nav:false,
              slideBy: 3
          },
          1000:{
              items:8,
              nav:true,
              loop:false,
              slideBy:5
          }
        }  
    });    


});
</script>