<!--style type="text/css">
@media (max-width: 768px) {
  .min-height-<?php /* echo $module; ?>{
    height: <?php echo $min[0]; ?>px;
  }
  .products-from-cat-div .owl-carousel{
    border: 1px solid #eee;
  }
  #products-from-cat-<?php echo $module; ?>{
    width: 130%;
  }
}
@media (min-width: 769px) and (max-width: 991px) {
  .min-height-<?php echo $module; ?>{
    min-height: <?php echo $min[1]; ?>px;
  }
  #products-from-cat-<?php echo $module; ?>{
    width: 130%;
  }  
}
@media (min-width: 992px){
  .min-height-<?php echo $module; ?>{
    min-height: <?php echo $min[2];*/ ?>px;
  }
}

</style-->
<style type="text/css">
@media (max-width: 768px) {
  #products-from-cat-<?php echo $module; ?>{
    width: 130%;
  }
}
@media (min-width: 769px) and (max-width: 991px) {
  #products-from-cat-<?php echo $module; ?>{
    width: 130%;
  }  
}  
</style>
<!-- Phu kien | single display and carousel  -->
<div class="row feature-div products-from-cat-div">
  <a href="<?php echo $link; ?>" class="row" style="text-align: left;"><h2 style="text-align: left;"><?php echo $heading_title; ?></h2></a>
  <!-- khuyen mai trong thang-->
  <div class="owl-carousel owl-theme feature-products products-category-home"  id="products-from-cat-<?php echo $module; ?>">
    <?php foreach ($products as $key => $product) { if($product['khuyenmai']){continue;} ?>
      <div class="item text-center min-height-<?php echo $module; ?>">
            <a class="row" href="<?php echo $product['href']; ?>">
                <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" class="img-responsive" />
                <?php if ($product['special']) { ?>
                  <label class="home-discount hidden">Giảm <?php echo $product['downpricerate']; ?></label>  
                  <label class="home-rate-discount-<?php echo ((int)substr($product['downrate'],0,-2) < 10) ? 'yellow' : 'red'; ?>">Giảm <?php echo $product['downrate']; ?></label>  
                <?php } ?> 
            </a>
                
            <div class="row info">
              <a href="<?php echo $product['href']; ?>"><h3 style="text-align: left;" ><?php echo $product['name']; ?></h3></a>
              <?php if ($product['price']) { ?>
              <p class="price">
                <?php if (!$product['special']) { ?>
                  <strong><?php echo $product['price']; ?></strong>
                <?php } else { ?>
                  <strong><?php echo $product['special']; ?></strong><span class="price-old hidden"><?php echo $product['price']; ?></span>
                <?php } ?>
              </p>
              <?php } ?>

              <?php if(strlen(trim(strip_tags($product['meta_description']))) > 0){  ?>
                <div class="discount-text"><?php echo ($key == 0) ? '' : $product['meta_description']; ?></div>
              <?php } ?>

            </div>
      </div> 
    <?php } ?>
  </div>
  <!-- -->
</div>
<script type="text/javascript">
$(document).ready(function(){

    var pk = $("#products-from-cat-<?php echo $module; ?>");
    pk.owlCarousel({
        navigation : false, 
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem: true,
        pagination: false,
        rewindSpeed: 500,        
        dots: false,
        nav: false,
        responsive:{
          0:{
              items:3,
              nav:true,
              slideBy: 2
          },
          600:{
              items:4,
              nav:false,
              slideBy: 3
          },
          1000:{
              items:5,
              nav:true,
              loop:false,
              slideBy:5
          }
        } 
    }); 
});
</script>