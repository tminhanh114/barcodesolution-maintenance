<div id="newsblogcategory<?php echo $module; ?>" class="col-lg-12 col-md-12 col-sm-12 hidden-xs" style="padding: 0px 10px 0px 0px;">
  <h2 style="font: normal 18px/40px 'Open sans',verdana !important;color:black;padding: 0 10px;border-bottom:1px solid #c95408;"><?php echo $heading_title; ?></h2>
  <div class="box-category-newsgroup" style="background: #4086c6;padding: 10px 0px;">
    <ul class="topnav">
      <?php foreach ($categories as $category) { ?>
      <li>
        <?php if ($category['category_id'] == $category_id) { ?>
        <a href="<?php echo $category['href']; ?>" class="active"><?php echo $category['name']; ?></a>
        <?php } else { ?>
        <a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
        <?php } ?>
        <?php if ($category['children']) { ?>
        <ul class="subnav">
          <?php foreach ($category['children'] as $child) { ?>
          <li>
            <?php if ($child['category_id'] == $child_id) { ?>
            <a href="<?php echo $child['href']; ?>" class="active"><?php echo $child['name']; ?></a>
            <?php } else { ?>
            <a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
            <?php } ?>
          </li>
          <?php } ?>
        </ul>
        <?php } ?>
      </li>
      <?php } ?>
    </ul>
  </div>
</div>
<script language="JavaScript">
$(document).ready(function() {

    $("ul.topnav li").each(function(index) {
            if($(this).find("a").attr('href') == "#"){
                $(this).click(function(){return false;});
            }
        
    });

       
    $("ul.topnav > li").click(function() {  
        if(!$(this).find("ul.subnav").is(':visible')){
            $(this).parent().find("ul.subnav").slideUp('fast');    
            $(this).find("ul.subnav").slideDown('slow').show(); 
            return false;;
        }
    });
});
</script>