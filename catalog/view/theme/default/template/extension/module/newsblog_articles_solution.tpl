<style type="text/css">
@media (max-width: 991px) {
  #solution-div{
    width: 130%;
  }  
}  
</style>
<div class="row feature-div products-from-cat-div">
  <div class="row" style="width: 100%;max-width: 1200px;margin: 0 auto;">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 main-box">
      <a href="<?php echo $link_to_category; ?>" class="row" style="text-align: left;"><h2 style="text-align: left;"><?php echo $heading_title; ?></h2></a>
      <div class="owl-carousel owl-theme solution-div"  id="solution-div">
        <?php foreach ($articles as $article) {  ?>
          <div  class="item text-center" style=" width: 100%;">
            <a href="<?php echo $article['href']; ?>"  style="display: block;padding: 7px;">
            <img src="<?php echo $article['thumb']; ?>" style="width: 100%;border-radius: 50%;">
            <span ><?php echo $article['name']; ?></span></a>
          </div>
        <?php } ?>
      </div>
    </div>
</div>
<?php if ($link_to_category) { ?>
<a class="hidden"> href="<?php echo $link_to_category; ?>"><i class="fas fa-arrow-right" style="color: red;"></i></a>
<?php } ?>
</div>
<script type="text/javascript">
$(document).ready(function(){

    var owlsl = $("#solution-div");
    owlsl.owlCarousel({
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem: true,
        pagination: false,
        rewindSpeed: 500,
        dots: false,
        responsive:{
            0:{
              items:3,
              nav:true,
              slideBy: 1
          },
          600:{
              items:3,
              nav:false,
              slideBy: 3
          },
          1000:{
              items:4,
              nav:true,
              loop:false,
              slideBy:4
          }
        },
        navText : ['<i class="material-icons">arrow_back_ios</i>','<i class="material-icons">arrow_forward_ios</i>']   
    });
});
</script>