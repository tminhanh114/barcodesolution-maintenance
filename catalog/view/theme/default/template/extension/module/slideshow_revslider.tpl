<div style="width: 100%; position: relative;float: left;">
  <div class="tp-banner-container"  style="z-index: 80;">
    <div class="tp-banner" >
      <ul>  <!-- SLIDE  -->
        <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
          <!-- MAIN IMAGE -->
          <img src="image/catalog/slide/jhe/slide5-1-1.jpg"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
          <!-- LAYERS -->

           <!-- LAYERS: slogan -->
          <div class="tp-caption customin customout"
            data-x="right"
            data-y="top"

            data-customin="x:0;y:200;z:100;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"

            data-start="200"
            data-speed="700"
            data-easing="Power0.easeIn"

            
            data-endspeed="500"
            data-captionhidden="on"
            
            ><div style="color: white;font-size: 18px;padding: 10px;font-weight: bold;font-family: sans-serif;">FOR SAFETY FOR SOCIETY</div>
          </div>


          <!-- LAYER NR. 3 -->
          <!--div class="tp-caption large_bold_grey skewfromrightshort customout" -->
          <div class="tp-caption large_bold_grey sft customout"
            data-x="80"
            data-y="96"
            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
            data-speed="500"
            data-start="800"
            data-easing="Back.easeOut"
            data-endspeed="500"
            data-endeasing="Power4.easeIn"
            data-captionhidden="on"
            style="z-index: 4"><span style="color: #1f9fd9;">JESCO HOABINH</span>
          </div>

          <!-- LAYER NR. 6 -->
          <div class="tp-caption large_bold_grey skewfromleftshort customout"
            data-x="175"
            data-y="152"
            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
            data-speed="300"
            data-start="1100"
            data-easing="Back.easeOut"
            data-endspeed="500"
            data-endeasing="Power4.easeIn"
            data-captionhidden="on"
            style="z-index: 7"><span style="color: #33316b;">ENGINEERING</span>
          </div>

          <!-- LAYER NR. 7 -->
          <div class="tp-caption small_thin_grey customin customout"
            data-x="80"
            data-y="240"
            data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
            data-speed="500"
            data-start="1300"
            data-easing="Power4.easeOut"
            data-endspeed="500"
            data-endeasing="Power4.easeIn"
            data-captionhidden="on"
            style="z-index: 8">With the growth and great development<br /> from 2008 to present,<BR /> Jesco Hoa Binh has become one of the best<br /> leading M&E contractors in Ho Chi Minh City</div>
          <div class="tp-caption small_thin_grey customin customout"
            data-x="80"
            data-y="380"
            data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
            data-speed="500"
            data-start="1600"
            data-easing="Power4.easeOut"
            data-endspeed="500"
            data-endeasing="Power4.easeIn"
            data-captionhidden="on"
            style="z-index: 8"><a href="http://www.google.com.vn" style="padding: 15px 45px;border: 2px solid #00bcd4;">PRODUCTS</a>
          </div>
        </li>        

        <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
          <!-- MAIN IMAGE -->
          <img src="http://thegem2.codexthemes.netdna-cdn.com/thegem/wp-content/uploads/2016/03/slide4-1.jpg"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
          <!-- LAYERS -->

          <!-- LAYER NR. 3 -->
          <div class="tp-caption large_bold_grey skewfromrightshort customout"
            data-x="580"
            data-y="96"
            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
            data-speed="500"
            data-start="800"
            data-easing="Back.easeOut"
            data-endspeed="500"
            data-endeasing="Power4.easeIn"
            data-captionhidden="on"
            style="z-index: 4"><span style="color: #1f9fd9;">JESCO HOABINH</span>
          </div>

          <!-- LAYER NR. 6 -->
          <div class="tp-caption large_bold_grey skewfromleftshort customout"
            data-x="675"
            data-y="152"
            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
            data-speed="300"
            data-start="1100"
            data-easing="Back.easeOut"
            data-endspeed="500"
            data-endeasing="Power4.easeIn"
            data-captionhidden="on"
            style="z-index: 7"><span style="color: #33316b;">ENGINEERING</span>
          </div>

          <!-- LAYER NR. 7 -->
          <div class="tp-caption small_thin_grey customin customout"
            data-x="580"
            data-y="240"
            data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
            data-speed="500"
            data-start="1300"
            data-easing="Power4.easeOut"
            data-endspeed="500"
            data-endeasing="Power4.easeIn"
            data-captionhidden="on"
            style="z-index: 8">With the growth and great development<br /> from 2008 to present,<BR /> Jesco Hoa Binh has become one of the best<br /> leading M&E contractors in Ho Chi Minh City
          </div>
          <div class="tp-caption small_thin_grey customin customout"
            data-x="580"
            data-y="380"
            data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
            data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
            data-speed="500"
            data-start="1600"
            data-easing="Power4.easeOut"
            data-endspeed="500"
            data-endeasing="Power4.easeIn"
            data-captionhidden="on"
            style="z-index: 8"><a href="http://www.google.com.vn" style="padding: 15px 45px;border: 2px solid #00bcd4;">PRODUCTS</a>
          </div>
        </li>

     </ul>
      <div class="tp-bannertimer"></div>
    </div>
  </div>

  <!-- THE SCRIPT INITIALISATION -->
  <!-- LOOK THE DOCUMENTATION FOR MORE INFORMATIONS -->
  <script type="text/javascript">

    var revapi;

    jQuery(document).ready(function() {

         revapi = jQuery('.tp-banner').revolution(
        {
          delay:4000,
          startwidth:1260,
          startheight:570,
          hideThumbs:10

        });

    }); //ready

  </script>

  <!-- END REVOLUTION SLIDER -->
</div>