<div class="row hidden-lg hidden-md" style="width: 100%;margin: 0 auto;" id="technology-div">
    <a href="<?php echo $link_to_category; ?>" class="row"><h2><?php echo $heading_title; ?></h2></a>
    <ul>
      <?php foreach ($articles as $key => $article) { if($key >= 3) continue;  ?>
        <li>
          <a href="<?php echo $article['href']; ?>">
            <img class="row" src="<?php echo $article['thumb']; ?>" style="width: 40%;padding-right: 5px">
            <h2 style="width: 60%;padding-left: 5px;" ><?php echo $article['name']; ?></h2>
          </a>
        </li>
      <?php } ?>
    </ul>
    <?php if ($link_to_category) { ?>
      <a class="link-button" href="<?php echo $link_to_category; ?>">Xem tin khác</a>
    <?php } ?>
</div>

<script type="text/javascript">

</script>