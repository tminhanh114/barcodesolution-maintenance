<div class="row solution-right-div" style="width: 100%;">
  <a href="<?php echo $link_to_category; ?>" class="row" style="text-align: left;"><h5><?php echo $heading_title; ?></h5></a>
  <ul>
  <?php foreach ($articles as $article) {  ?>
    <li>
      <a href="<?php echo $article['href']; ?>">
      <span ><?php echo $article['name']; ?></span></a>
    </li>
  <?php } ?>
  </ul>
  <?php if ($link_to_category) { ?>
    <a class="hidden"> href="<?php echo $link_to_category; ?>"><i class="fas fa-arrow-right" style="color: red;"></i></a>
  <?php } ?>
</div>

