<style type="text/css">

@media (max-width: 768px) {
  #products-from-cat-<?php echo $module; ?>{
    width: 130%;
  }
}
@media (min-width: 769px) and (max-width: 991px) {
  #products-from-cat-<?php echo $module; ?>{
    width: 130%;
  }  
}

</style>
<div class="row feature-div products-from-cat-div">
  <a href="<?php echo $link; ?>" class="row" style="text-align: left;"><h2 style="text-align: left;"><?php echo $heading_title; ?></h2></a>
  <!-- -->
  <div class="row col-lg-5 col-md-5 col-sm-8 col-xs-12 first-product-box">
    <?php if($products[1]['khuyenmai']){ ?>
        <!-- Co khuyen mai cho san pham dau tien -->
        <div class="row" style="width: 60%;position: relative;float: left;">  
          <a class="row" href="<?php echo $products[0]['href']; ?>">
              <img src="<?php echo $products[0]['thumb']; ?>" alt="<?php echo $products[0]['name']; ?>" class="img-responsive" />
              <?php if ($products[0]['special']) { ?>
                <label class="home-discount">Giảm <?php echo $products[0]['downpricerate']; ?></label>  
              <?php } ?> 
          </a>
              
          <div class="row info">
            <a href="<?php echo $products[0]['href']; ?>"><h3 style="text-align: left;" ><?php echo $products[0]['name']; ?></h3></a>
            <?php if ($products[0]['price']) { ?>
            <p class="price">
              <?php if (!$products[0]['special']) { ?>
                <strong><?php echo $products[0]['price']; ?></strong>
              <?php } else { ?>
                <strong><?php echo $products[0]['special']; ?></strong><span class="price-old hidden"><?php echo $products[0]['price']; ?></span>
              <?php } ?>
            </p>
            <?php } ?>
          </div>
        </div>
        <div class="row" style="width: 40%;position: relative;float: left;">
            <?php $num_div = (count($products[1]['thumb']) == 3) ? 4 : 6; ?>
            <span style="font-size: 14px;text-align: left;line-height: 2em;">Khuyến mãi:</span>
            <a href="<?php echo $products[0]['href']; ?>">
              <div class="row khuyenmai-image" style="width: 100%">
              <?php foreach($products[1]['thumb'] as $khuyenmai_image){ ?>
                <img class="row col-lg-<?php echo $num_div; ?> col-md-<?php echo $num_div; ?> col-sm-<?php echo $num_div; ?> col-xs-<?php echo $num_div; ?>"  src="<?php echo $khuyenmai_image['image']; ?>" alt="<?php echo $khuyenmai_image['name']; ?>" />
              <?php } ?>
              </div>
            </a>
            <div class="discount-text"><?php echo $products[0]['meta_description']; ?></div>
          </div>        

        <!-- Co khuyen mai cho san pham dau tien :: end-->
    <?php }else{ ?>
    <!-- Khong khuyen mai cho san pham dau tien -->
        <div class="row" style="width: 100%;position: relative;float: left;">  
          <a class="row" href="<?php echo $products[0]['href']; ?>" style="display: block;width: 100%">
              <img src="<?php echo $products[0]['thumb']; ?>" alt="<?php echo $products[0]['name']; ?>" class="img-responsive" style="display: block;width: 100%" />
              <?php if ($products[0]['special']) { ?>
                <label class="home-discount">Giảm <?php echo $products[0]['downpricerate']; ?></label>  
              <?php } ?> 
          </a>
              
          <div class="row info">
            <a href="<?php echo $product['href']; ?>"><h3 style="text-align: left;" ><?php echo $products[0]['name']; ?></h3></a>
            <?php if ($products[0]['price']) { ?>
            <p class="price">
              <?php if (!$products[0]['special']) { ?>
                <strong><?php echo $products[0]['price']; ?></strong>
              <?php } else { ?>
                <strong><?php echo $products[0]['special']; ?></strong><span class="price-old hidden"><?php echo $products[0]['price']; ?></span>
              <?php } ?>
            </p>
            <div class="discount-text"><?php echo $products[0]['meta_description']; ?></div>
            <?php } ?>
          </div>
        </div> 
    <!-- Khong khuyen mai cho san pham dau tien :: end-->
    <?php } ?> 
  </div>
  <div class="row col-lg-7 col-md-7 col-sm-4 col-xs-12 carousel-box" style="padding: 0px;">
    <div class="owl-carousel owl-theme feature-products products-category-home"  id="products-from-cat-<?php echo $module; ?>">
      <?php foreach ($products as $key => $product) { if($key < 2){ continue; } ?>
        <div class="item text-center min-height-<?php echo $module; ?>" style="padding: 1px;">
          <?php if($product['khuyenmai']){ ?> 
            <?php $num_div = (count($product['thumb']) == 3) ? 4 : 6; ?>
            <h3 style="font-size: 14px;text-align: left;">KHUYẾN MÃI</h3>
            <a href="<?php echo $product['href']; ?>">
              <div class="row khuyenmai-image" style="width: 100%">
              <?php foreach($product['thumb'] as $khuyenmai_image){ ?>
                <img class="row col-lg-<?php echo $num_div; ?> col-md-<?php echo $num_div; ?> col-sm-<?php echo $num_div; ?> col-xs-<?php echo $num_div; ?>"  src="<?php echo $khuyenmai_image['image']; ?>" alt="<?php echo $khuyenmai_image['name']; ?>" />
              <?php } ?>
              </div>
            </a>
            <div class="discount-text"><?php echo $product['meta_description']; ?></div>
          <?php }else{ ?>
            <a class="row" href="<?php echo $product['href']; ?>">
                <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" class="img-responsive" />
                <?php if ($product['special']) { ?>
                  <label class="home-discount hidden">Giảm <?php echo $product['downpricerate']; ?></label>  
                  <label class="home-rate-discount-<?php echo ((int)substr($product['downrate'],0,-2) < 10) ? 'yellow' : 'red'; ?>">Giảm <?php echo $product['downrate']; ?></label>  
                <?php } ?> 
            </a>
                
            <div class="row info">
              <a href="<?php echo $product['href']; ?>"><h3 style="text-align: left;" ><?php echo $product['name']; ?></h3></a>
              <?php if ($product['price']) { ?>
              <p class="price">
                <?php if (!$product['special']) { ?>
                  <strong><?php echo $product['price']; ?></strong>
                <?php } else { ?>
                  <strong><?php echo $product['special']; ?></strong><span class="price-old hidden"><?php echo $product['price']; ?></span>
                <?php } ?>
              </p>
              <?php } ?>

              <?php if(strlen(trim(strip_tags($product['meta_description']))) > 0){  ?>
                <div class="discount-text hello"><?php echo ($key == 0) ? '' : $product['meta_description']; ?></div>
              <?php } ?>

            </div>
                 
          <?php } ?>
        </div> 
      <?php } ?>
    </div>    
  </div>
  <!-- -->
</div>
<script type="text/javascript">
$(document).ready(function(){

    var fp<?php echo $module ?> = $("#products-from-cat-<?php echo $module; ?>");
    fp<?php echo $module ?>.owlCarousel({
        navigation : false, 
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem: true,
        pagination: false,
        rewindSpeed: 500,        
        dots: false,
        nav: false,
        responsive:{
          0:{
              items:3,
              nav:true,
              slideBy: 2
          },
          600:{
              items:2,
              nav:false,
              slideBy: 1
          },
          1000:{
              items:3,
              nav:true,
              loop:false,
              slideBy:3
          }
        } 
    }); 
});
</script>