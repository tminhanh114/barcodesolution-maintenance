<div class="test" style="width: 100%; position: relative;float: left;">
    <div class="row" style="padding: 20px 0px;"><H5 style="font: 300 30px/40px 'open sans',verdana;"><?php echo $heading_title; ?></H5></div>
    <div class="owl-carousel owl-theme feature-project"  id="carousel<?php echo $module; ?>">
      <?php for ($key = 0; $key < count($banners);$key++) { ?>
      <div class="item text-center" style="padding: 1px;">
        <?php if ($banners[$key]['link']) { ?>
        <a href="<?php echo $banners[$key]['link']; ?>" style="margin-bottom: 2px;position: relative;float: left;width: 100%;">
            <img style="width: 100%;" src="<?php echo $banners[$key]['image']; ?>" alt="<?php echo $banners[$key]['title']; ?>" class="img-responsive" />
            <span style="font-weight: 300;color: white;background: rgba(44, 77, 125, 0.8);padding: 7px 15px;position: absolute;right: 0px;bottom: 0px;"><?php echo $banners[$key]['title']; ?></span>
        </a>
        <?php } else { ?>
            <img  style="margin-bottom: 2px;width: 100%;" src="<?php echo $banners[$key]['image']; ?>" alt="<?php echo $banners[$key]['title']; ?>" class="img-responsive" />
            <span style="font-weight: 300;color: white;background: rgba(44, 77, 125, 0.8);padding: 7px 15px;position: absolute;right: 0px;bottom: 0px;"><?php echo $banners[$key]['title']; ?></span>
        <?php } ?>
      </div>
      <?php } ?>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  $('#carousel<?php echo $module; ?>').owlCarousel({
      loop: true,
      margin: 10,
      responsiveClass: true,
      responsive:{
              0:{
                  items:2
              },
              767:{
                  items:4
              },
              991:{
                  items:4
              },
              1199:{
                  items:4 
              }
          },
      dots: false,
      nav: true,
      navText : ['<i class="fa fa-angle-left fa-2x" ></i>','<i class="fa fa-angle-right fa-2x" ></i>']     
  });
});
</script>