  
<div class="feature-div">
<h5 style="text-align: center;"><?php echo 'SẢN PHẨM MỚI NHẤT';//$heading_title; ?></h5>
  <?php foreach ($products as $product) { ?>
  <div class="product-layout col-lg-4 col-md-4 col-sm-6 col-xs-12">
    <div class="product-thumb transition">
      <div class="featured-image">
        <a class="display-img" href="<?php echo $product['href']; ?>"><img width="100%" src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a>
        <a class="display-des"  href="<?php echo $product['href']; ?>"><p class="home-product-description"><?php echo $product['description']; ?></p></a>
      </div>

      <div class="caption">
        <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
        <?php if ($product['price']) { ?>
        <p class="price">
          <?php if (!$product['special']) { ?>
          <?php echo $product['price']; ?>
          <?php } else { ?>
          <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
          <?php } ?>
          <?php if ($product['tax']) { ?>
          <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
          <?php } ?>
        </p>
        <?php } ?>
      </div>
      <div class="button-group hidden">
        <button style="width: 100%;" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-shopping-cart"></i>&nbsp;&nbsp;&nbsp; <span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>
      </div>
    </div>
  </div>
  <?php } ?>

  <!-- -->
  <div class="owl-carousel owl-theme feature-products"  id="feature-products-<?php echo $module; ?>">
    <?php foreach ($products as $key => $product) { ?>
    <div class="item text-center" style="padding: 1px;">
      <?php if ($product['link']) { ?>
      <a href="<?php echo $product['link']; ?>" style="margin-bottom: 2px;position: relative;float: left;width: 100%;">
          <img style="width: 100%;" src="<?php echo $product['image']; ?>" alt="<?php echo $product['title']; ?>" class="img-responsive" />
          <span style="font-weight: 300;color: white;background: rgba(44, 77, 125, 0.8);padding: 7px 15px;position: absolute;right: 0px;bottom: 0px;"><?php echo $product['title']; ?></span>
      </a>
      <?php } else { ?>
          <img  style="margin-bottom: 2px;width: 100%;" src="<?php echo $product['image']; ?>" alt="<?php echo $product['title']; ?>" class="img-responsive" />
          <span style="font-weight: 300;color: white;background: rgba(44, 77, 125, 0.8);padding: 7px 15px;position: absolute;right: 0px;bottom: 0px;"><?php echo $product['title']; ?></span>
      <?php } ?>
    </div>
    <?php } ?>
  </div>
  <!-- -->
</div>
<script type="text/javascript">
$(document).ready(function(){

    var feature_products = $("#feature-products-<?php echo $module; ?>");
    feature_products.owlCarousel({
        navigation : false, 
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem: true,
        pagination: false,
        rewindSpeed: 500,
        items : 1,
        dots: false 
    }); 
});
</script>