<!--//status = 7: group news and box of category (category has 3 news item) -->
<?php echo $header; ?>
<div class="row cat-image-title" >
    <?php if ($thumb) { ?>
      <img style="width: 100%;position: relative;float: left;"   src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>"  />  
    <?php } ?>
    <div class="newsblog-title">
        <img class="col-lg-4 col-md-4 hidden-sm hidden-xs" style="display: inline-block;max-height: 100px;max-width: 233px;" src="catalog/view/theme/default/image/jhe/title-bg-6.png">
        <h2 class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="display: inline-block;"><?php echo $heading_title; ?></h2>
    </div>
</div>

<div class="container">
  <div class="breadcrumb" style="background-color: #eaeaea !important;padding: 15px; border-left: 3px solid #09004b; border-radius: 0px; ">
    <ul class="hidden">
      <?php foreach ($breadcrumbs as $key => $breadcrumb) { ?>
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ul>
  </div>  
  <div class="row">
    <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6 col-md-6 col-lg-6 col-xs-12';$class_r = 'col-sm-3 col-lg-3 hidden-xs col-md-3'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9 col-lg-9 col-md-9 col-xs-12';$class_r = 'col-sm-3 col-lg-3 col-md-3 hidden-xs'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12 col-lg-12 col-md-12 col-xs-12'; ?>
    <?php } ?>
    <div id="content-7" class="<?php echo $class; ?>">
      <?php echo $content_top; ?>
      <!--child category -->
      <?php if ($categories) {  ?>
        <h3 class="hidden"><?php echo $text_refine; ?></h3>
        <?php if (count($categories) <= 5) { ?>
        <div class="row hidden">
          <div class="col-sm-3">
            <ul>
              <?php foreach ($categories as $category) {  ?>
              <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
              <?php } ?>
            </ul>
          </div>
        </div>
        <?php } else { ?>
        <div class="row">
          <?php foreach (array_chunk($categories, ceil(count($categories) / 4)) as $categories) { ?>
          <div class="col-sm-3">
            <ul>
              <?php foreach ($categories as $category) { ?>
              <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
              <?php } ?>
            </ul>
          </div>
          <?php } ?>
        </div>
        <?php } ?>
      <?php } ?>
      <!--child category :: end -->
      <?php if ($articles) { ?>
        <!---->
        <div class="row article-3" style="width: 100%;max-width: 1200px;margin: 0 auto;">
            <?php foreach ($articles as $article) { ?>
              <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 item">
              <?php if ($article['thumb']) { ?>
                <a class="img-link"  href="<?php echo $article['href']; ?>"><img style="width: 100%;" src="<?php echo $article['thumb']; ?>" alt="<?php echo $article['name']; ?>" title="<?php echo $article['name']; ?>" class="img-responsive" /></a>
                <a class="title-link" href="<?php echo $article['href']; ?>"><h4 style="font: 400 15px/20px 'open sans',verdana;color: #077cce;text-align: justify;"> <?php echo $article['name']; ?></h4></a>
                <?php if(trim($article['preview']) != ""){ ?><p><?php echo $article['preview']; ?></p><?php } ?>
              <?php } ?>        
              </div>    
            <?php } ?>

            <div class="row" style="width: 100%;" >
              <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
              <div class="col-sm-6 text-right hidden"><?php echo $results; ?></div>
            </div>

        </div>
        <!---->

      <?php }else{ ?>
        <p style="width: 100%; padding: 20px 0;"><img src="image/catalog/system/underconstruction.jpg" style="width: 100%" /></p>
      <?php } ?>

      <?php echo $content_bottom; ?>
    </div>
    <div class="<?php  echo (isset($class_r)) ? $class_r : 'hidden'; ?>" style="<?php  echo (isset($class_r)) ? 'padding-top: 60px;' : 'display: none;'; ?>">
      <!-- Sibling category with 03 news of -->

        <div class="box-category-newsgroup" style="background: #4086c6;padding: 0px;border-top: 2px solid #292663;">
          <ul class="topnav">
            <?php foreach ($siblingCategories as $category) { ?>
            <li>
              <a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
              <?php if ($category['articles']) { ?>
                <ul class="news-list" style="display: block;padding: 0px 5px;background: #e4e4e4;">
                  <?php foreach ($category['articles'] as $child) { ?>
                    <li style="color: #504d7f;"><a style="color: black;" href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                  <?php } ?>
                </ul>
              <?php } ?>
            </li>
            <?php } ?>
          </ul>
        </div>

      <!-- Sibling category with 03 news of :: end -->

    </div>
  </div>
</div>
<?php echo $footer; ?>