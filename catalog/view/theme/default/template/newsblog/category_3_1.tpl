<?php echo $header; ?>
<div class="row cat-image-title" >
    <?php if ($thumb) { ?>
      <img style=""   src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>"  />  
    <?php } ?>
    <div style="background: url(catalog/view/theme/default/image/jhe/title-bg-6.png) 0 0 no-repeat;">
        <h2><?php echo $heading_title; ?></h2>
    </div>
</div>

<div class="container">
  <div class="breadcrumb" style="background-color: #eaeaea !important;padding: 15px; border-left: 3px solid #09004b; border-radius: 0px; ">
    <ul class="hidden">
      <?php foreach ($breadcrumbs as $key => $breadcrumb) { ?>
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ul>
  </div>  
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>" style="padding-top: 30px;"><?php echo $content_top; ?>
      <?php if ($thumb || $description) { ?>

      <hr>
      <?php } ?>
      <?php if ($categories) { ?>
      <h3><?php echo $text_refine; ?></h3>
      <?php if (count($categories) <= 5) { ?>
      <div class="row">
        <div class="col-sm-3">
          <ul>
            <?php foreach ($categories as $category) { ?>
            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
            <?php } ?>
          </ul>
        </div>
      </div>
      <?php } else { ?>
      <div class="row">
        <?php foreach (array_chunk($categories, ceil(count($categories) / 4)) as $categories) { ?>
        <div class="col-sm-3">
          <ul>
            <?php foreach ($categories as $category) { ?>
            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
            <?php } ?>
          </ul>
        </div>
        <?php } ?>
      </div>
      <?php } ?>
      <?php } ?>
      <?php if ($articles) { ?>
        <!---->
        <div class="row" style="width: 100%;max-width: 1200px;margin: 0 auto;">
          <div class="col-lg-8">
            <?php foreach ($articles as $article) { ?>
              <div class="col-lg-4" style="padding: 10px 5px 10px 5px; ">
              <?php if ($article['thumb']) { ?>
                <a  href="<?php echo $article['href']; ?>"><img src="<?php echo $article['thumb']; ?>" alt="<?php echo $article['name']; ?>" title="<?php echo $article['name']; ?>" class="img-responsive" /></a>
                <a style="padding: 10px; background: #ebebeb;position: relative;float: left;width: 100%;display: block;text-align: center;" href="<?php echo $article['href']; ?>"><h4 style="font: 400 15px/20px 'open sans',verdana;"> <?php echo $article['name']; ?></h4></a>
              <?php } ?>        
              </div>    
            <?php } ?>

            <div class="row col-lg-12" >
              <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
              <div class="col-sm-6 text-right"><?php echo $results; ?></div>
            </div>
          </div>

        </div>
        <!---->

      <?php } ?>

      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>