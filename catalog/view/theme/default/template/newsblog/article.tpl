<?php echo $header; ?>
<div style="width: 100%;position: relative;float: left;">
  <div class="row cat-image-title hidden" >
      <?php if ($cat_thumb) { ?>
        <img style="width: 100%;position: relative;float: left;"  src="<?php echo $cat_thumb; ?>" alt="<?php echo $heading_title_group; ?>" title="<?php echo $heading_title_group; ?>"  />  
        <div class="hidden">
          <img class="col-lg-4 col-md-4 hidden-sm hidden-xs" style="display: inline-block;max-height: 100px;max-width: 233px;" src="catalog/view/theme/default/image/jhe/title-bg-6.png">
          <h2 class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="display: inline-block;"><?php echo $heading_title_group; ?></h2>
        </div>      
      <?php }else{ ?>
            <h2 class="grouptitle-no-groupimage"><?php echo $heading_title_group; ?></h2>
      <?php } ?>
  </div>
  
  <div class="article-content" style="width: 100%; max-width: 1200px; position: relative;margin: 0 auto;">
    <div  class="tabmenu-news">
      <ul>
        <li><a href="#" class="actmenu">Tin mới</a></li>
        <li><a href="#">Sản phẩm mới</a></li>
        <li><a href="#">Giải pháp</a></li>
        <li><a href="#">Thị trường</a></li>
      </ul>
    </div>

    <h2 class=" hidden" style="display: inline-block;width: 100%;margin-top: 10px;border-left: 5px solid  #a2acb1;padding-left: 7px;"><?php echo $heading_title_group; ?></h2>
    <div class="row"  style="width: 100%;max-width: 800px;margin: 0 auto;position: relative;"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
        <div class="col-sm-12 col-lg-12 col-md-12 col-xs-12 news-content" style="text-align: justify;">
            <h1 style="" ><?php echo $heading_title; ?></h1>
            <div class="fb-like" data-href="" data-layout="button" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
            <div class="row userdetail" style="height: 20px;width: 100%;">
              <a class="hidden" href="/tin-tuc/profile/2004791"><img width="20" height="20" data-src="https://graph.facebook.com/1320167908001228/picture" src="https://graph.facebook.com/1320167908001228/picture">Hữu Tình</a>                      
              <span  class="hidden">22 giờ trước</span>
              <span  class="hidden" onclick="ScrollTo('#comment')"><i class="iconnews-comcya"></i>13 bình luận</span>
            </div>
            <img class="hidden" src="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
            <?php echo $preview;?>
            <?php echo $description; ?>
        </div>

        <div class="col-sm-12 col-lg-12 col-md-12 col-xs-12 news-image hidden" style="text-align: justify;">
            <?php if ($thumb || $images) { ?>
            <ul class="related-images">
              <?php if ($thumb) { ?>
              <li class="first-image"><a href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>">
                <img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a>
              </li>
              <?php } ?>
              <?php if ($images) { ?>
              <?php foreach ($images as $image) { ?>
              <li class="more-image col-lg-4 col-md-4 col-sm-4 col-xs-4"><a href="<?php echo $image['original']; ?>" title="<?php echo $heading_title; ?>"> <img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></li>
              <?php } ?>
              <?php } ?>
            </ul>
            <?php } ?>
        </div>
        <div style="padding-top: 20px;padding-left: 10px;">

            <?php if ($attributes) { ?>
            <h5><?php echo $text_attributes;?></h5>
              <?php foreach ($attributes as $attribute_group) { ?>
                  <?php foreach ($attribute_group['attribute'] as $attribute_item) { ?>
                      <b><?php echo $attribute_item['name'];?>:</b> <?php echo $attribute_item['text'];?><br />
                  <?php } ?>
              <?php } ?>
            <?php } ?>

            <?php if ($articles) { ?>
              <h3 style="border-bottom: 1px solid #4086c6;line-height:  36px;"><?php echo $text_related; ?></h3>
              <div class="row">
                <div class="related col-xs-12 col-lg-12 col-md-12 col-sm-12" style="padding: 5px 0px;">
                <?php foreach ($articles as $article) { ?>
                
                  <div class="product-thumb col-lg-4 col-md-4 col-sm-6 col-xs-6" style="border: none;">
                    <div class="image">
                          <a href="<?php echo $article['href']; ?>">
                                <img src="<?php echo $article['thumb']; ?>" alt="<?php echo $article['name']; ?>" title="<?php echo $article['name']; ?>" class="img-responsive" />
                          </a>
                          <h4 style="display: block; overflow: hidden; line-height: 1.3em; font-size: 16px; color: #333; line-height: 22px; font-weight: 300;font-family: Arial,Helvetica,sans-serif; width: auto;margin: 5px auto;"><a href="<?php echo $article['href']; ?>"><?php echo $article['name']; ?></a></h4>
                    </div>
                    
                  
                </div>
                <?php } ?>
                </div>
              </div>
            <?php } ?>
        </div>



      <?php if ($products) { ?>
      <h3><?php echo $text_related_products; ?></h3>
      <div class="row">
        <?php $i = 0; ?>
        <?php foreach ($products as $product) { ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-lg-6 col-md-6 col-sm-12 col-xs-12'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-lg-4 col-md-4 col-sm-6 col-xs-12'; ?>
        <?php } else { ?>
        <?php $class = 'col-lg-3 col-md-3 col-sm-6 col-xs-12'; ?>
        <?php } ?>
        <div class="<?php echo $class; ?>">
          <div class="product-thumb transition">
            <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
            <div class="caption">
              <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
              <p><?php echo $product['description']; ?></p>
              <?php if ($product['rating']) { ?>
              <div class="rating">
                <?php for ($i = 1; $i <= 5; $i++) { ?>
                <?php if ($product['rating'] < $i) { ?>
                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                <?php } else { ?>
                <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                <?php } ?>
                <?php } ?>
              </div>
              <?php } ?>
              <?php if ($product['price']) { ?>
              <p class="price">
                <?php if (!$product['special']) { ?>
                <?php echo $product['price']; ?>
                <?php } else { ?>
                <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                <?php } ?>
                <?php if ($product['tax']) { ?>
                <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
                <?php } ?>
              </p>
              <?php } ?>
            </div>
            <div class="button-group">
              <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');"><span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span> <i class="fa fa-shopping-cart"></i></button>
              <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button>
              <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button>
            </div>
          </div>
        </div>
        <?php if (($column_left && $column_right) && ($i % 2 == 0)) { ?>
        <div class="clearfix visible-md visible-sm"></div>
        <?php } elseif (($column_left || $column_right) && ($i % 3 == 0)) { ?>
        <div class="clearfix visible-md"></div>
        <?php } elseif ($i % 4 == 0) { ?>
        <div class="clearfix visible-md"></div>
        <?php } ?>
        <?php $i++; ?>
        <?php } ?>
      </div>
      <?php } ?>

      <?php if ($tags) { ?>
      <p><?php echo $text_tags; ?>
        <?php for ($i = 0; $i < count($tags); $i++) { ?>
        <?php if ($i < (count($tags) - 1)) { ?>
        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
        <?php } else { ?>
        <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
        <?php } ?>
        <?php } ?>
      </p>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
  </div>
  
</div>


<script type="text/javascript"><!--
$(document).ready(function() {
	$('.related-images').magnificPopup({
		type:'image',
		delegate: 'a',
		gallery: {
			enabled:true
		}
	});

  $('.newsblog-article').css('background','white');
});
//--></script>
<?php echo $footer; ?>