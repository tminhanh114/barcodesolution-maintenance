<!-- category solution -->
<?php echo $header; ?>
<div class="row cat-image-title" >
    <?php if ($thumb) { ?>
      <img style="width: 100%;position: relative;float: left;"   src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>"  />  
    <?php } ?>
    <div class="newsblog-title" style="background: none;">
        <h2 class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="display: inline-block;color: #333;"><?php echo $heading_title; ?></h2>
    </div>
</div>


<div class="container" style="background: white;padding: 0px;">
    <div class="row"><?php echo $column_left; ?>
    
    <?php $class = 'col-sm-12'; ?>
    <div id="content" class="<?php echo $class; ?>" ><?php echo $content_top; ?>
      <?php if ($articles) { ?>
        <!---->
        <div class="row" style="width: 100%;max-width: 1200px;margin: 0 auto;overflow: hidden;">
          <?php foreach ($articles as $article) { ?>
            <div class="col-xl-3 col-lg-3 col-md-4 hidden-sm hidden-xs solution-item">
            <?php if ($article['thumb']) { ?>
                <a class="img-link"  href="<?php echo $article['href']; ?>"><img style="width: 100%;" src="<?php echo $article['thumb']; ?>" alt="<?php echo $article['name']; ?>" title="<?php echo $article['name']; ?>" class="img-responsive" /></a>
                <a class="title-link" href="<?php echo $article['href']; ?>"><h4 style="font: 400 15px/18px 'open sans',verdana;color: #077cce;text-align: center;padding: 10px 0px"> <?php echo $article['name']; ?></h4></a>
                <?php if(trim($article['preview']) != ""){ ?><p><?php echo $article['preview']; ?></p><?php } ?>
            <?php } ?>        
            </div>    
          <?php } ?>          
          <div class="owl-carousel owl-theme solution-div hidden-lg hidden-md"  id="solution-div" style="width: 140%;">
            <?php foreach ($articles as $article) { ?>
            <div class="item  solution-item">
            <?php if ($article['thumb']) { ?>
                <a class="img-link"  href="<?php echo $article['href']; ?>"><img style="width: 100%;" src="<?php echo $article['thumb']; ?>" alt="<?php echo $article['name']; ?>" title="<?php echo $article['name']; ?>" class="img-responsive" /></a>
                <a class="title-link" href="<?php echo $article['href']; ?>"><h4 style="font: 400 15px/18px 'open sans',verdana;color: #077cce;text-align: center;padding: 10px 0px"> <?php echo $article['name']; ?></h4></a>
                <?php if(trim($article['preview']) != ""){ ?><p class="hidden"><?php echo $article['preview']; ?></p><?php } ?>
            <?php } ?>        
            </div>    
            <?php } ?>  
          </div>
        </div>
        <!---->

      <?php }else{ ?>
        <p style="width: 100%; padding: 20px 0;"><img src="image/catalog/system/underconstruction.jpg" style="width: 100%" /></p>
      <?php } ?>

      <?php echo $content_bottom; ?>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
       var winWidth =  $(window).width();
       if(winWidth < 768 ){
          //console.log('Window Width: '+ winWidth + 'class used: col-xs');
          var owlsl = $("#solution-div");
            owlsl.owlCarousel({
                slideSpeed : 300,
                paginationSpeed : 400,
                singleItem: true,
                pagination: false,
                rewindSpeed: 500,
                dots: false,
                responsive:{
                  0:{
                      items:3,
                      nav:true,
                      slideBy: 1
                  },
                  760:{
                      items:3,
                      nav:false,
                      slideBy: 3
                  }
                },
                navText : ['<i class="material-icons">arrow_back_ios</i>','<i class="material-icons">arrow_forward_ios</i>']   
            });

       }

    
});
</script>
<?php echo $footer; ?>