<!-- category of newsgroup SOLUTION like TGDD status = 10-->
<?php echo $header; ?>
<div class="container"  style="background: white;">
  <div  class="tabmenu-news">
    <ul>
      <?php foreach($childCategories_new as $key => $childCategory){ ?>
        <li><a class="<?php echo ($category_id == (int)$childCategory['category_id']) ? 'actmenu' : ''; ?>" href="<?php echo $childCategory['href']; ?>"><?php echo $childCategory['categoryName']; ?></a></li>
      <?php } ?>
      <!-- <li id="cat27"><a  class="<?php echo ($category_id == '27') ? 'actmenu' : ''; ?>" href="/giai-phap/xay-dung">Xây dựng</a></li>
      <li id="cat28"><a  class="<?php echo ($category_id == '28') ? 'actmenu' : ''; ?>" href="/giai-phap/giao-duc">Giáo dục</a></li>
      <li id="cat29"><a  class="<?php echo ($category_id == '29') ? 'actmenu' : ''; ?>" href="/giai-phap/chinh-phu">Chính phủ</a></li>
      <li id="cat30"><a  class="<?php echo ($category_id == '30') ? 'actmenu' : ''; ?>" href="/giai-phap/san-xuat">Sản xuất</a></li>
      <li id="cat31"><a  class="<?php echo ($category_id == '31') ? 'actmenu' : ''; ?>" href="/giai-phap/cham-soc-suc-khoe">Chăm sóc sức khoẻ</a></li>
      <li id="cat32"><a  class="<?php echo ($category_id == '32') ? 'actmenu' : ''; ?>" href="/giai-phap/ban-le">Bán lẻ</a></li>
      <li id="cat33"><a  class="<?php echo ($category_id == '33') ? 'actmenu' : ''; ?>" href="/giai-phap/giao-thong-van-tai-hau-can">Giao thông Vận tải & Hậu cần</a></li>
      <li id="cat34"><a  class="<?php echo ($category_id == '34') ? 'actmenu' : ''; ?>" href="/giai-phap/tien-ich-khach-san-giai-tri">Tiện ích khách sạn - Giải trí</a></li>
      <li id="cat35"><a  class="<?php echo ($category_id == '35') ? 'actmenu' : ''; ?>" href="/giai-phap/cac-giai-phap-khac">Các giải pháp khác</a></li> -->
    </ul>
  </div>
  <div class="row"><?php echo $column_left; ?>
    
    <?php $class = 'col-sm-12'; ?>
    <div id="content" class="<?php echo $class; ?>" ><?php echo $content_top; ?>
      <?php if ($articles) { ?>
        <!---->
        <div class="row article-3" style="width: 100%;max-width: 1200px;margin: 0 auto;">
          <div class="col-lg-8 col-md-8 col-md-12 col-sm-12" style="padding: 0px 10px 0px 0px; ">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 big-first-news" style="padding: 0px 10px 0px;">
              <a href="<?php echo $articles[0]['href']; ?>" alt="<?php echo $articles[0]['name']; ?>">
                <img class="img-responsive" src="<?php echo $articles[0]['thumb']; ?>"  title="<?php echo $articles[0]['name']; ?>" />
                <h3 style=""><?php echo $articles[0]['name']; ?></h3>
                <p style="padding: 10px 0px;"><?php echo $articles[0]['preview']; ?></p>
              </a>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"  style="padding: 0px 0px 0px 3px;">
              <a class="second-news" href="<?php echo $articles[1]['href']; ?>" alt="<?php echo $articles[1]['name']; ?>">
                <img class="img-responsive col-lg-12 col-md-12 col-sm-4 col-xs-4" src="<?php echo $articles[1]['thumb']; ?>"  title="<?php echo $articles[1]['name']; ?>" />
                <h3><?php echo $articles[1]['name']; ?></h3>
                <?php if(trim($articles[1]['preview']) != ""){ ?><p class="hidden-lg hidden-md col-sm-8 hidden-xs"><?php echo $articles[1]['preview']; ?></p><?php } ?>
              </a>
              <a class="third-news" href="<?php echo $articles[2]['href']; ?>" alt="<?php echo $articles[2]['name']; ?>">
                <img class="img-responsive hidden-lg hidden-md col-sm-4 col-xs-4" src="<?php echo $articles[2]['thumb']; ?>"  title="<?php echo $articles[2]['name']; ?>" />
                <h3><?php echo $articles[2]['name']; ?></h3>
                <?php if(trim($articles[2]['preview']) != ""){ ?><p class="hidden-lg hidden-md col-sm-8 hidden-xs"><?php echo $articles[2]['preview']; ?></p><?php } ?>
              </a>
              <a class="third-news" href="<?php echo $articles[3]['href']; ?>" alt="<?php echo $articles[3]['name']; ?>">
                <img class="img-responsive hidden-lg hidden-md col-sm-4 col-xs-4" src="<?php echo $articles[3]['thumb']; ?>"  title="<?php echo $articles[3]['name']; ?>" />
                <h3 ><?php echo $articles[3]['name']; ?></h3>
                <?php if(trim($articles[3]['preview']) != ""){ ?><p class="hidden-lg hidden-md col-sm-8 hidden-xs"><?php echo $articles[3]['preview']; ?></p><?php } ?>
              </a>
              <a class="third-news" style="border: none;" href="<?php echo $articles[4]['href']; ?>" alt="<?php echo $articles[4]['name']; ?>">
                <img class="img-responsive hidden-lg hidden-md col-sm-4 col-xs-4" src="<?php echo $articles[4]['thumb']; ?>"  title="<?php echo $articles[4]['name']; ?>" />
                <h3><?php echo $articles[4]['name']; ?></h3>
                <?php if(trim($articles[4]['preview']) != ""){ ?><p class="hidden-lg hidden-md col-sm-8 hidden-xs"><?php echo $articles[4]['preview']; ?></p><?php } ?>
              </a>
            </div>
            <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <ul class="newslist" id="mainlist">
              <?php foreach ($articles as $key => $article) { if($key < 5) continue; ?>
                <li >
                  <a  href="<?php echo $article['href']; ?>">
                  <?php if ($article['thumb']) { ?>
                    <img  class="img-link col-lg-4 col-md-4 col-sm-4 col-xs-4 img-responsive" src="<?php echo $article['thumb']; ?>" alt="<?php echo $article['name']; ?>" title="<?php echo $article['name']; ?>" />
                    <h3 class="col-lg-8 col-md-8"> <?php echo $article['name']; ?></h3>
                    <?php if(trim($article['preview']) != ""){ ?><p class="col-lg-8 col-md-8 col-sm-8 hidden-xs"><?php echo $article['preview']; ?></p><?php } ?>
                  <?php } ?>
                  </a>        
                </li>    
              <?php } ?>
              </ul>
            </div>
            <div class="row" style="width: 100%;" >
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left"><?php echo $pagination; ?></div>
              <div class="col-sm-6 text-right hidden"><?php echo $results; ?></div>
            </div>
          </div>
          <div class="row col-lg-4 col-md-4 col-md-12 col-sm-12" style="padding: 0px 0px 0px 5px;"><?php echo $column_right; ?></div>


        </div>
        <!---->

      <?php }else{ ?>
        <p style="width: 100%; padding: 20px 0;"><img src="image/catalog/system/underconstruction.jpg" style="width: 100%" /></p>
      <?php } ?>

      <?php echo $content_bottom; ?></div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $('.newsblog-category').css('background','white');
  });
</script>
<?php echo $footer; ?>