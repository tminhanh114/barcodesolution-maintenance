<?php echo $header;  ?>
  <div class="row " id="carousel-div">
    <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="false">
      <div>
        <ul class="submenu" >
            <?php foreach ($articles as $key => $article) { ?>
              <li data-target="#myCarousel" data-slide-to="<?php echo $key; ?>" <?php echo ($key == 0) ? 'class="active"' : ''; ?> ><a href="#"><?php echo $article['name']; ?></a></li>
            <?php } ?>
            <?php foreach($childCategories as $keyC => $category ){ ?>
              <li data-target="#myCarousel" data-slide-to="<?php echo $keyC + count($articles); ?>" ><a href="#"><?php echo $category['categoryName']; ?></a></li>
            <?php } ?>
        </ul>
      </div>
 
      <!-- Wrapper for slides -->
      <div class="carousel-inner">
            <?php foreach ($articles as $key => $article) { ?>
              <div class="item <?php echo ($key == 0) ? 'active' : ''; ?>">
                  <h2 style="border-left: 5px solid #3589c4; font: 400 20px/40px 'open sans',verdana; color: #3589c4; text-transform: uppercase; padding-left: 10px; margin-bottom: 20px;"><?php echo $article['name']; ?></h2>
                  <?php echo $article['description']; ?>
              </div><!-- End Item -->
            <?php } ?>
            <?php foreach($childCategories as $keyC => $category ){ ?>
              <div class="item">
                  
                  <h2 style="border-left: 5px solid #3589c4; font: 400 20px/40px 'open sans',verdana; color: #3589c4; text-transform: uppercase; padding-left: 10px; margin-bottom: 20px;"><?php echo $category['categoryName']; ?></h2>
                  <div class="bussiness" style="padding: 10px 0px 10px 5px;width: 100%">
                      <div class="tab-content clearfix" style="width: 100%;">
                        <div class="panel-group" id="accordion">    
                        <?php foreach ($category['articles'] as $keyA => $value) { ?>
                            <div class="panel panel-default">
                              <div class="panel-heading" style="width: 100%;">
                                <h4 class="panel-title" style="width: 100%;position: relative;float: left;font: 400 14px/21px 'open sans',verdana;padding: 5px 0px;text-transform: uppercase;">                                  
                                  <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $keyA; ?>"  aria-expanded="true" aria-controls="collapse<?php echo $keyA; ?>">
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"><span style="color: #252355;padding: 0px 10px 0px 0px; font-weight: bold; margin-right: 20px;"><?php echo ($keyA + 1); ?></span></div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"><?php echo $value['name']; ?></div>
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">NS-HCM</div>
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">....</div>
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">TP. Hồ Chí Minh</div>
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">2018</div>
                                  </a>
                                </h4>
                              </div>
                              <div id="collapse<?php echo $keyA; ?>" class="panel-collapse collapse collapsing" >
                                <p  style="padding: 10px 20px;"><?php echo $value['description']; ?></p>
                              </div>
                            </div>
                          
                        <?php } ?>
                        </div>

                      </div>  
                  </div>
              </div>
            <?php } ?>
      </div>

      <?php echo $content_bottom; ?>
    </div>
</div>
<script type="text/javascript">
  $(document).ready( function() {
    $('#myCarousel').carousel({
      interval:  false
  });
  
  var clickEvent = false;
  $('#myCarousel').on('click', '.submenu a', function() {
      clickEvent = true;
      $('.submenu li').removeClass('active');
      $(this).parent().addClass('active');

  }).on('slid.bs.carousel', function(e) {
    if(!clickEvent) {
      var count = $('.submenu').children().length -1;
      var current = $('.submenu li.active');
      current.removeClass('active').next().addClass('active');
      var id = parseInt(current.data('slide-to'));
      if(count == id) {
        $('.submenu li').first().addClass('active');  
      }
    }
    clickEvent = false;
  });
});
</script>
<?php echo $footer; ?>