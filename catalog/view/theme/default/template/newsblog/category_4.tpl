<?php echo $header;  ?>
<!-- //status = 4: group news with slider and some category of news -->

<style>
.timeline {
     list-style: none;
     padding: 20px 0 20px;
     position: relative;
}
 .timeline:before {
     top: 0;
     bottom: 0;
     position: absolute;
     content: " ";
     width: 3px;
     background-color: #eeeeee;
     left: 50%;
     margin-left: -1.5px;
}
 .timeline > li {
     margin-bottom: 20px;
     position: relative;
}
 .timeline > li:before, .timeline > li:after {
     content: " ";
     display: table;
}
 .timeline > li:after {
     clear: both;
}
 .timeline > li:before, .timeline > li:after {
     content: " ";
     display: table;
}
 .timeline > li:after {
     clear: both;
}
 .timeline > li > .timeline-panel {
     width: 46%;
     float: left;
     border: 1px solid #d4d4d4;
     border-radius: 2px;
     padding: 20px;
     position: relative;
     -webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
     box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
}
 .timeline > li > .timeline-panel:before {
     position: absolute;
     top: 26px;
     right: -15px;
     display: inline-block;
     border-top: 15px solid transparent;
     border-left: 15px solid #ccc;
     border-right: 0 solid #ccc;
     border-bottom: 15px solid transparent;
     content: " ";
}
 .timeline > li > .timeline-panel:after {
     position: absolute;
     top: 27px;
     right: -14px;
     display: inline-block;
     border-top: 14px solid transparent;
     border-left: 14px solid #fff;
     border-right: 0 solid #fff;
     border-bottom: 14px solid transparent;
     content: " ";
}
 .timeline > li > .timeline-badge {
     color: #fff;
     width: 50px;
     height: 50px;
     line-height: 50px;
     font-size: 1.4em;
     text-align: center;
     position: absolute;
     top: 16px;
     left: 50%;
     margin-left: -25px;
     background-color: #999999;
     z-index: 100;
     border-top-right-radius: 50%;
     border-top-left-radius: 50%;
     border-bottom-right-radius: 50%;
     border-bottom-left-radius: 50%;
}
 .timeline > li.timeline-inverted > .timeline-panel {
     float: right;
}
 .timeline > li.timeline-inverted > .timeline-panel:before {
     border-left-width: 0;
     border-right-width: 15px;
     left: -15px;
     right: auto;
}
 .timeline > li.timeline-inverted > .timeline-panel:after {
     border-left-width: 0;
     border-right-width: 14px;
     left: -14px;
     right: auto;
}
 .timeline-badge.primary {
     background-color: #2e6da4 !important;
}
 .timeline-badge.success {
     background-color: #3f903f !important;
}
 .timeline-badge.warning {
     background-color: #f0ad4e !important;
}
 .timeline-badge.danger {
     background-color: #d9534f !important;
}
 .timeline-badge.info {
     background-color: #5bc0de !important;
}
 .timeline-title {
     margin-top: 0;
     color: inherit;
     font: 400 18px/28px 'open sans',verdana;
}
 .timeline-body > p, .timeline-body > ul {
     margin-bottom: 0;
}
 .timeline-body > p + p {
     margin-top: 5px;
}
 @media (max-width: 767px) {
     ul.timeline:before {
         left: 40px;
    }
     ul.timeline > li > .timeline-panel {
         width: calc(100% - 90px);
         width: -moz-calc(100% - 90px);
         width: -webkit-calc(100% - 90px);
    }
     ul.timeline > li > .timeline-badge {
         left: 15px;
         margin-left: 0;
         top: 16px;
    }
     ul.timeline > li > .timeline-panel {
         float: right;
    }
     ul.timeline > li > .timeline-panel:before {
         border-left-width: 0;
         border-right-width: 15px;
         left: -15px;
         right: auto;
    }
     ul.timeline > li > .timeline-panel:after {
         border-left-width: 0;
         border-right-width: 14px;
         left: -14px;
         right: auto;
    }
}

</style>
  <div class="row " id="carousel-div">
    <div id="myCarousel" class="carousel slide" data-ride="carousel" style="background: url(catalog/view/theme/default/image/jhe/no-slogan-40h.png) 0 0 repeat-x;">
      <div>
        <ul class="submenu" >
            <?php foreach ($articles as $key => $article) { ?>
              <li data-target="#myCarousel" data-slide-to="<?php echo $key; ?>" <?php echo ($key == $_GET['item']) ? 'class="active"' : ''; ?> ><a href="#"><?php echo $article['name']; ?></a></li>
            <?php } ?>
            <?php foreach($childCategories as $keyC => $category ){ ?>
              <li data-target="#myCarousel" data-slide-to="<?php echo $keyC + count($articles); ?>" <?php echo (($keyC + count($articles)) == $_GET['item']) ? 'class="active"' : ''; ?> ><a href="#"><?php echo $category['categoryName']; ?></a></li>
            <?php } ?>
        </ul>
      </div>
 
      <!-- Wrapper for slides -->
      <div class="carousel-inner" style="">
            <?php foreach ($articles as $key => $article) { ?>
              <div class="item <?php echo ($key == $_GET['item']) ? 'active' : ''; ?>">
                  <h2 class="news-title" ><?php echo $article['name']; ?></h2>
                  <?php echo $article['description']; ?>
              </div>
            <?php } ?>
            <?php foreach($childCategories as $keyC => $category ){ ?>
              <?php if($category['category_id'] == 19){ ?>
                <div class="item <?php echo (2 == $_GET['item']) ? 'active' : ''; ?>">
                  <ul class="nav nav-tabs col-lg-12">
                    <?php foreach ($category['articles'] as $keyA => $value) { ?>
                        <li class="<?php echo ($keyA == 0) ? 'active' : ''; ?> col-lg-3 col-sm-4 col-md-3 col-xs-4" style="padding: 0px;">
                          <a class="transaction" style="text-align: center;text-transform: uppercase;" href="#<?php echo $keyC . $keyA ?>" data-toggle="tab" aria-expanded="true"><?php echo $value['name'] ?></a></li> 
                    <?php } ?>                          
                  </ul>
                  <div class="tab-content col-lg-12" style="border: 1px solid #dddddd3d;border-top: none;">
                    <?php foreach ($category['articles'] as $keyA => $value) { ?>
                        <div class="tab-pane <?php echo ($keyA == 0) ? 'active' : ''; ?>" id="<?php echo $keyC . $keyA; ?>">
                              <div style="padding: 15px 20px 15px 10px;"><?php echo $value['description']; ?></div>
                      </div>                            
                    <?php } ?>
                  </div>  
                </div>
              <?php } ?>              
              <?php if($category['category_id'] == 3){ ?>
                <div class="item <?php echo ($_GET['item'] == 3) ? 'active' : '3'; ?>">
                  <h2 class="news-title" style="text-transform: uppercase;padding-left: 0px;background: none;"><?php echo $category['categoryName']; ?></h2>
                  <div class="bussiness hidden-sm hidden-xs" style="padding: 10px 0px 10px 5px;width: 100%;">
                      <ul class="nav nav-stacked col-lg-4 col-md-4 hidden-sm hidden-xs">
                        <?php foreach ($category['articles'] as $keyA => $value) { ?>
                            <li class="<?php echo ($keyA == 0) ? 'active' : ''; ?>">
                              <a class="buss-number transaction" href="#<?php echo $keyC . $keyA ?>" data-toggle="tab" aria-expanded="true"><?php echo $keyA + 1; ?></a>
                              <a class="buss-text  transaction" style="padding: 10px 20px;" href="#<?php echo $keyC . $keyA ?>" data-toggle="tab" aria-expanded="true"><?php echo $value['name'] ?></a></li> 
                        <?php } ?>                          
                      </ul>
                      <div class="tab-content clearfix  col-lg-8 col-md-8 hidden-xs hidden-sm">
                        <?php foreach ($category['articles'] as $keyA => $value) { ?>
                            <div class="tab-pane <?php echo ($keyA == 0) ? 'active' : ''; ?>" id="<?php echo $keyC . $keyA; ?>">
                              <div class="col-xs-12" style="background: url(<?php echo $value['original']; ?>) 0% 0% no-repeat; background-size: 100%;">
                                  <h2 style="margin-bottom: 20px;color:#223a80; text-align: right;font: 300 28px/48px 'open sans',verdana;border-bottom: 1px solid #c8c8c8;text-transform: uppercase;"><?php echo $value['name'] ?></h2>
                                  <div class="buss-content" style="position: absolute;top: 60px; left: 0px; background:#0080b1a1;padding: 15px 20px 15px 10px;"><?php echo $value['description']; ?></div>
                              </div>            
                            </div>                            
                        <?php } ?>
                      </div>

                  </div>
                </div>
              <?php } ?>
              <?php if($category['category_id'] == 22){ ?>
                <div class="item <?php echo ($_GET['item'] == 4) ? 'active' : '4'; ?>">
                  <ul class="timeline">
                  <?php foreach ($category['articles'] as $keyA => $value) { ?>
                    <li class="timeline-<?php echo ($keyA % 2) ? 'inverted' : 'badge'; ?>">
                      <div class="timeline-badge"><i class="glyphicon glyphicon-check"></i></div>
                      <div class="timeline-panel">
                        <div class="timeline-heading" style="border-bottom: 1px solid #e0e0e0;margin-bottom: 10px;">
                          <h4 class="timeline-title"><?php echo $value['name']; ?></h4>
                          <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i><?php echo $value['date']; ?></small></p>
                        </div>
                        <div class="timeline-body">
                          <p><?php echo $value['preview']; ?></p>  
                          <?php echo $value['description']; ?>
                        </div>
                      </div>
                                            
                    </li>
                  <?php } ?>    
                  </ul>
                </div>
              <?php } ?>


              
            <?php } ?>
      </div>

      <?php echo $content_bottom; ?>
    </div>
</div>
<script type="text/javascript">

$(document).ready( function() {



  $('#myCarousel').carousel({
    interval:  false
  });


  var clickEvent = false;
  $('#myCarousel').on('click', '.submenu a', function() {
      clickEvent = true;
      $('.submenu li').removeClass('active');
      $(this).parent().addClass('active');

  }).on('slid.bs.carousel', function(e) {
    if(!clickEvent) {
      var count = $('.submenu').children().length -1;
      var current = $('.submenu li.active');
      current.removeClass('active').next().addClass('active');
      var id = parseInt(current.data('slide-to'));
      if(count == id) {
        $('.submenu li').first().addClass('active');  
      }
    }
    clickEvent = false;
  });
});
</script>
<?php echo $footer; ?>