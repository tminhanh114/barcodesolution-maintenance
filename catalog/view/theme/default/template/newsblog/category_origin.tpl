<?php echo $header; ?>
<div class="row cat-image-title" >
    <?php if ($thumb) { ?>
      <img style=""   src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>"  />  
    <?php } ?>
    <div style="background: url(catalog/view/theme/default/image/jhe/title-bg-6.png) 0 0 no-repeat;">
        <h2><?php echo $heading_title; ?></h2>
    </div>
</div>

<div class="container">
  <div class="breadcrumb" style="background-color: #eaeaea !important;padding: 15px; border-left: 3px solid #09004b; border-radius: 0px; ">
    <ul class="hidden">
      <?php foreach ($breadcrumbs as $key => $breadcrumb) { ?>
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ul>
  </div>  
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>" style="padding-top: 30px;"><?php echo $content_top; ?>
      <?php if ($thumb || $description) { ?>

      <hr>
      <?php } ?>
      <?php if ($categories) { ?>
      <h3><?php echo $text_refine; ?></h3>
      <?php if (count($categories) <= 5) { ?>
      <div class="row">
        <div class="col-sm-3">
          <ul>
            <?php foreach ($categories as $category) { ?>
            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
            <?php } ?>
          </ul>
        </div>
      </div>
      <?php } else { ?>
      <div class="row">
        <?php foreach (array_chunk($categories, ceil(count($categories) / 4)) as $categories) { ?>
        <div class="col-sm-3">
          <ul>
            <?php foreach ($categories as $category) { ?>
            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
            <?php } ?>
          </ul>
        </div>
        <?php } ?>
      </div>
      <?php } ?>
      <?php } ?>
      <?php if ($articles) { ?>
      <div class="row">
        <?php foreach ($articles as $article) { ?>
        <div class="product-layout product-list col-xs-12" style="padding: 0px;">
          <div class="product-thumb" style="border:none;border-bottom: 1px dotted #ddd;">
            <div class="image col-lg-4 col-md-4 col-sm-4 col-xs-4" style="padding: 0px;margin-bottom: 20px;"><a href="<?php echo $article['href']; ?>"><img src="<?php echo $article['thumb']; ?>" alt="<?php echo $article['name']; ?>" title="<?php echo $article['name']; ?>" class="img-responsive" /></a></div>
            <div class="caption col-lg-8 col-md-8 col-sm-8 col-xs-8">
                <h3><a href="<?php echo $article['href']; ?>"><?php echo $article['name']; ?></a></h3>
                <p style="text-align: left;color: #808080;font-size: 12px;"><?php echo $article['date']; ?></p>
                <p style="text-align: justify;"><?php echo $article['preview']; ?></p>
                <?php if ($article['attributes']) { ?>
	                <h5><?php echo $text_attributes;?></h5>
	                <?php foreach ($article['attributes'] as $attribute_group) { ?>
	                	<?php foreach ($attribute_group['attribute'] as $attribute_item) { ?>
                       	<b><?php echo $attribute_item['name'];?>:</b> <?php echo $attribute_item['text'];?><br />
	                	<?php } ?>
	                <?php } ?>
                <?php } ?>
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
      <div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $results; ?></div>
      </div>
      <?php } ?>

      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>