<?php echo $header; ?>
  <div class="row " id="carousel-div">
    <div id="aboutus-container" style="width: 100%; max-width: 1200px;margin: 0 auto">
      <div id="myCarousel" data-ride="carousel"  class="carousel slide col-sm-12 col-xs-12 col-lg-12 col-md-12">
        <!-- Wrapper for slides -->
        <div class="aboutus-inner">
          <?php foreach ($articles as $key => $article) { ?>
            <div class="item <?php echo ($key == 0) ? 'active' : ''; ?>" style="margin-bottom: 20px; ">
                <h2 class="news-title" style="font: bold 16px/30px helvetica,sans-serif;     text-decoration: underline #f5a91d solid;"><i id="aboutus-hub-button" class="material-icons"><?php echo $icon_arr[$key]; ?></i><?php echo $article['name']; ?></h2>
                <?php echo $article['description']; ?>
            </div>
          <?php } ?>
        </div>
        <!-- Wrapper for slides :: end-->
      </div>

      <?php echo $content_bottom; ?>
  
    </div>
  </div>
<script type="text/javascript">
$(document).ready( function() {
  $('.newsblog-category').css({'background' : '#fff'});
  
});
</script>
<?php echo $footer; ?>