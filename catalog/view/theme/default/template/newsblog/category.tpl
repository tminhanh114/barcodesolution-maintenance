<!-- category of newsgroup with pagging -->
<?php echo $header; ?>
<div class="row cat-image-title" >
    <?php if ($thumb) { ?>
      <img style="width: 100%;position: relative;float: left;"   src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>"  />  
    <?php } ?>
    <div class="newsblog-title">
        <h2 class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="display: inline-block;"><?php echo $heading_title; ?></h2>
    </div>
</div>

<div class="container">
    <div class="row"><?php echo $column_left; ?>
    
    <?php $class = 'col-sm-12'; ?>
    <div id="content" class="<?php echo $class; ?>" ><?php echo $content_top; ?>
      <?php if ($thumb || $description) { ?>
      <?php } ?>
      <?php if ($categories) { ?>
        <h3 class="hidden"><?php echo $text_refine; ?></h3>
        <?php if (count($categories) <= 5) { ?>
        <div class="row hidden">
          <div class="col-sm-3">
            <ul>
              <?php foreach ($categories as $category) { ?>
              <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
              <?php } ?>
            </ul>
          </div>
        </div>
        <?php } else { ?>
        <div class="row">
          <?php foreach (array_chunk($categories, ceil(count($categories) / 4)) as $categories) { ?>
          <div class="col-sm-3">
            <ul>
              <?php foreach ($categories as $category) { ?>
              <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
              <?php } ?>
            </ul>
          </div>
          <?php } ?>
        </div>
        <?php } ?>
      <?php } ?>
      <?php if ($articles) { ?>
        <!---->
        <div class="row article-3" style="width: 100%;max-width: 1200px;margin: 0 auto;">
          <?php foreach ($articles as $article) { ?>
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-xs-12 item">
            <?php if ($article['thumb']) { ?>
                <a class="img-link"  href="<?php echo $article['href']; ?>"><img style="width: 100%;" src="<?php echo $article['thumb']; ?>" alt="<?php echo $article['name']; ?>" title="<?php echo $article['name']; ?>" class="img-responsive" /></a>
                <a class="title-link" href="<?php echo $article['href']; ?>"><h4 style="font: 400 15px/20px 'open sans',verdana;color: #077cce;text-align: justify;"> <?php echo $article['name']; ?></h4></a>
                <?php if(trim($article['preview']) != ""){ ?><p><?php echo $article['preview']; ?></p><?php } ?>
            <?php } ?>        
            </div>    
          <?php } ?>

          <div class="row" style="width: 100%;" >
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left"><?php echo $pagination; ?></div>
            <div class="col-sm-6 text-right hidden"><?php echo $results; ?></div>
          </div>

        </div>
        <!---->

      <?php }else{ ?>
        <p style="width: 100%; padding: 20px 0;"><img src="image/catalog/system/underconstruction.jpg" style="width: 100%" /></p>
      <?php } ?>

      <?php echo $content_bottom; ?></div>
  </div>
</div>
<?php echo $footer; ?>