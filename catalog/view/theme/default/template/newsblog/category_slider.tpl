<?php echo $header; ?>
  <div class="row " id="carousel-div">
    <div id="aboutus-container" style="width: 100%; max-width: 1200px;margin: 0 auto">
      <div class="col-sm-12 col-xs-12 hidden-lg hidden-md aboutus-menu-div">
        <ul class="aboutus-submenu horizon" >
          <?php foreach ($articles as $key => $article) { ?>
            <li data-target="#myCarousel" data-slide-to="<?php echo $key; ?>" <?php echo ($key == 0) ? 'class="active"' : ''; ?> ><a href="#"><?php echo $article['name']; ?></a></li>
          <?php } ?>
        </ul>
      </div>        
      <div class="col-lg-3 col-md-4 hidden-xs hidden-sm  aboutus-menu-div">
        <ul class="aboutus-submenu vertical" >
          <?php foreach ($articles as $key => $article) { ?>
            <li data-target="#myCarousel" data-slide-to="<?php echo $key; ?>" <?php echo ($key == 0) ? 'class="active"' : ''; ?> ><a href="#"><?php echo $article['name']; ?></a></li>
          <?php } ?>
        </ul>
      </div>
      <div id="myCarousel" data-ride="carousel"  class="carousel slide col-sm-12 col-xs-12 col-lg-9 col-md-8">
        <!-- Wrapper for slides -->
        <div class="carousel-inner">
          <?php foreach ($articles as $key => $article) { ?>
            <div class="item <?php echo ($key == 0) ? 'active' : ''; ?>">
                <h2 class="news-title"><?php echo $article['name']; ?></h2>
                <?php echo $article['description']; ?>
            </div>
          <?php } ?>
        </div>
        <!-- Wrapper for slides :: end-->
      </div>

      <?php echo $content_bottom; ?>
    </div>
  </div>
<script type="text/javascript">
  $(document).ready( function() {
    $('#myCarousel').carousel({
      interval:  false
  });
  
  var clickEvent = false;

  $('#aboutus-container').on('click', '.aboutus-submenu a', function() {
      clickEvent = true;
      $('.submenu li').removeClass('active');
      $(this).parent().addClass('active');

  }).on('slid.bs.carousel', function(e) {
    if(!clickEvent) {
      var count = $('.submenu').children().length -1;
      var current = $('.submenu li.active');
      current.removeClass('active').next().addClass('active');
      var id = parseInt(current.data('slide-to'));
      if(count == id) {
        $('.submenu li').first().addClass('active');  
      }
    }
    clickEvent = false;
  });
  $('.newsblog-category').css({'background' : '#fff'});
});
</script>
<?php echo $footer; ?>