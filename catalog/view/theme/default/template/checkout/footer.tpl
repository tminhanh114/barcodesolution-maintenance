<footer>
  <div class="container">
    <div class="row footer-main-top">
       <div class="col-md-10 footer-left">
          <div class="row">
             <div class="col-md-2 hidden-xs hidden-sm">
                <div class="logo-footer">
                  <a href="#"><img src="image/catalog/system/hth-150.png" title="Bất động sản Hoàng Thịnh" alt="Bất động sản Hoàng Thịnh" class="img-responsive"></a>
                </div>
             </div>
             <div class="col-md-10">
                <p class="color-white" style="font: 300 14px/18px 'open sans',arial; ">Công ty Hoàng Thịnh – là một đơn vị chuyên hoạt động trong lĩnh vực bất động sản. Với sứ mệnh mang lại cho khách hàng giải pháp và sự lựa chọn tối ưu nhất, Công ty Hoàng Thịnh đã và đang trở thành một trong những sàn giao dịch bất động sản uy tín thuộc hàng đầu tại Việt Nam.</p>
<div class="contact-phonefax"><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;&nbsp; Phạm Hữu Lầu, P Phú Mỹ, Q 7, Tp HCM (Căn hộ Belleza, Lô E25)</div>
        <div class="contact-phonefax"><i class="fa fa-phone" aria-hidden="true"></i>&nbsp;&nbsp; (84.28) 66834599 - 0968570909 </div>             </div>
          </div>
       </div>
       <div class="col-md-2 footer-right">
          <ul class="list-inline lst-social">
             <li><a href="https://www.facebook.com/hoangthinhhouse" target="_blank"><i aria-hidden="true" class="fa fa-facebook"></i></a></li>
             <li><a href="http://hth/#"><i aria-hidden="true" class="fa fa-youtube"></i></a></li>
             <li><a href="http://hth/#"><i aria-hidden="true" class="fa fa-envelope-o"></i></a></li>
          </ul>
       </div>
    </div>

    <!-- -->
    <div class="row hidden" style="width:100%">
      <h2 style="width: 50%;padding: 0 5px;margin: 0px;color: #E8E8E8;font: 300 24px/48px 'open sans', sans-serif;display:inline-block;float:left;">PHÙ ĐIÊU ĐẤT SÉT
        <a class="facebook-link" href="https://facebook.com/phudieudatset/"><i class="fa fa-facebook-official" aria-hidden="true" style="font-size: 20px;line-height: 40px;color: #4aa3ef;padding-left: 10px;border-left: 1px solid gray;margin-left: 10px;"></i></a>
        <a class="facebook-link" href="https://www.instagram.com/phudieudatset/"><i class="fa fa-instagram" aria-hidden="true" style="font-size: 20px;line-height: 40px;color: #4aa3ef;padding-left: 5px;"></i></a>
      </h2>
      <div style="width: 10%;display:inline-block;float:left;"></div>
      <div class="hidden-xs hidden-sm" style="width: 30%;display: inline-block;float: right;" >
        <a style="padding: 5px 10px;font-size: 15px; line-height: 30px; border: 1px solid gray;" href="index.php?route=information/information&information_id=7">Hướng dẫn mua hàng</a>        
      </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 hidden">
<!--iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d3920.358918350142!2d106.72492133053971!3d10.706774833562417!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1svi!2s!4v1521788270818" width="100%" height="280" frameborder="0" style="border:0" allowfullscreen></iframe-->    
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1959.7374915638225!2d106.68395021926602!3d10.774886485418785!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752f246097023f%3A0x2f3019dbf7d2f2c1!2zMzYyLzU2IE5ndXnhu4VuIMSQw6xuaCBDaGnhu4N1LCBwaMaw4budbmcgNCwgUXXhuq1uIDMsIEjhu5MgQ2jDrSBNaW5oLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1501077904847" width="100%" height="280" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 hidden">
        <div class="contact-phonefax"><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;&nbsp; Phạm Hữu Lầu, P Phú Mỹ, Q 7, Tp HCM (Căn hộ Belleza, Lô E25)</div>
        <div class="contact-phonefax"><i class="fa fa-phone" aria-hidden="true"></i>&nbsp;&nbsp; (84.28) 66834599 - 0968570909 </div>
        <!--a target="_blank" rel="nofollow" title="Đếm Web" href="http://gostats.vn"><img alt="Đếm Web" src="http://c5.gostats.vn/bin/count/a_1069914/t_5/i_1/counter.png" style="border-width:0"></a-->
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 banklink hidden">
        <div class="contact-phonefax"><img src="catalog/view/theme/default/image/kaga/vcb.png" width="26px"><span style="font: 300 14px/30px 'open sans',sans-serif;color: #E8E8E8;">&nbsp;&nbsp;0071 0007 45245 - Nguyễn Minh Hiền ( Chi nhánh Bến Thành)</span></div>
        <div class="contact-phonefax"><img src="catalog/view/theme/default/image/kaga/techcombank.png" width="26px"><span style="font: 300 14px/30px 'open sans',sans-serif;color: #E8E8E8;">&nbsp;&nbsp;190 2957 3579010 - Ngô Đức Hải (Chi nhánh Bình Thạnh)</span></div>
        <div class="contact-phonefax"><img src="catalog/view/theme/default/image/kaga/donga.png" width="26px"><span style="font: 300 14px/30px 'open sans',sans-serif;color: #E8E8E8;">&nbsp;&nbsp;0102 699 171 - Nguyễn Minh Hiền (Chi nhánh Bến Thành)</span></div>
        <div class="contact-phonefax"><img src="catalog/view/theme/default/image/kaga/acb.png" width="26px"><span style="font: 300 14px/30px 'open sans',sans-serif;color: #E8E8E8;">&nbsp;&nbsp;128 347 739 - Nguyễn Minh Hiền (Chi nhánh Bến Thành)</span></div>
    </div>

    <div class="row col-lg-6 col-md-6 col-sm-12 col-xs-12 hidden">
      <?php if ($informations) { ?>
      <div class="col-sm-12">
        <h5><?php echo $text_information; ?></h5>
        <ul class="list-unstyled">
          <?php foreach ($informations as $information) { ?>
          <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
          <?php } ?>
        </ul>
      </div>
      <?php } ?>
      <div class="col-sm-3 hidden">
        <h5><?php echo $text_service; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
          <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
          <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
        </ul>
      </div>
      <div class="col-sm-3 hidden">
        <h5><?php echo $text_extra; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
          <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
          <li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
          <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
        </ul>
      </div>
      <div class="col-sm-3 hidden">
        <h5><?php echo $text_account; ?></h5>
        <ul class="list-unstyled">
          <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
          <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
          <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
          <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
        </ul>
      </div>
    </div>
    <!--hr>
    <p ><?php echo '';//$powered; ?></p-->
  </div>
<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
(function(){ var widget_id = 'TBVyaNoAL9';var d=document;var w=window;function l(){
var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
<!-- {/literal} END JIVOSITE CODE -->
</footer>
</body></html>