<?php echo $header; ?>
<div class="row " id="carousel-div">
  <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="false">
    <div class="row" style="width: 100%; max-width: 1200px;">
      <h2 style="color: white; margin: 0 auto; line-height: 40px;"><?php echo $heading_title; ?></h2>
    </div>
    <div class="carousel-inner" style="margin-top: 20px;">
      <div class="row">
        <?php echo $column_left; //var_dump($categories); ?>

        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>

        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
          
          <?php if ($categories) { ?>
            <?php foreach ($categories as $category) { ?>
              <h3 class="hidden"> id="<?php echo $category['name']; ?>"><?php echo $category['name']; ?></h3>
              
              <?php if ($category['manufacturer']) { ?>
                <?php foreach (array_chunk($category['manufacturer'], 4) as $manufacturers) { ?>
                <div class="row">
                  <?php foreach ($manufacturers as $manufacturer) { ?>
                    <div class="col-sm-3 col-lg-3 col-md-3 col-sm-3" style="padding: 0px 20px 20px 0px;">
                      <img style="width: 100%;" src="<?php echo $manufacturer['image']; ?>" />
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                      <h3><?php echo $manufacturer['name']; ?></h3>                      
                    </div>
                  <?php } ?>
                </div>
                <?php } ?>
              <?php } ?>
            <?php } ?>
          <?php } else { ?>
          <p><?php echo $text_empty; ?></p>
          <div class="buttons clearfix">
            <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
          </div>
          <?php } ?>
          <?php echo $content_bottom; ?>
        </div>

        <?php echo $column_right; ?>
      </div>
    </div>
    <?php echo $footer; ?>
  </div>
</div>