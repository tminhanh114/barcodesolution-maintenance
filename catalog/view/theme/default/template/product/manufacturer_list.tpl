<?php echo $header; ?>
<div class="row " id="carousel-div">
  <div id="myCarousel" class="carousel slide">
    <div class="row" style="width: 100%; ">
      <h2 style="color: white; margin: 0 auto; line-height: 40px;max-width: 1200px;"><?php echo $heading_title; ?></h2>
    </div>
    <div class="carousel-inner" style="margin-top: 20px;">
      <div class="row">
        <?php echo $column_left;  ?>

        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>

        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
          <?php if ($categories) { ?>
            <?php foreach ($categories as $category) { ?>
              <h3 class="hidden"> id="<?php echo $category['name']; ?>"><?php echo $category['name']; ?></h3>
              
              <?php if ($category['manufacturer']) { ?>
                <?php foreach (array_chunk($category['manufacturer'], 4) as $manufacturers) { ?>
                <div class="row manufacturer-div">
                  <?php foreach ($manufacturers as $manufacturer) { ?>
                    <div class="col-sm-3 col-lg-3 col-md-3 col-sm-3 manufacturer-logo" >
                      <img src="<?php echo $manufacturer['image']; ?>" />
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                      <h3 style="text-transform: uppercase;"><?php echo $manufacturer['name']; ?></h3> 
                      <?php foreach($manufacturer['products'] as $product){ ?>
                        <a href="<?php echo $product['href']; ?>" class="row col-lg-3 col-md-3 col-sm-4 col-xs-6" style="padding: 10px;">
                          <img class="hidden-xs" style="width: 100%" src="<?php echo $product['image']; ?>" />
                          <h4 ><?php echo $product['name']; ?></h4>
                        </a>
                      <?php } ?>                     
                    </div>
                  <?php } ?>
                </div>
                <?php } ?>
              <?php } ?>
            <?php } ?>
          <?php } else { ?>
            <p style="width: 100%; padding: 20px 0;"><img src="image/catalog/system/underconstruction.jpg" style="width: 100%" /></p>
          <?php } ?>
          <?php echo $content_bottom; ?>
        </div>

        <?php echo $column_right; ?>
      </div>
    </div>
    <?php echo $footer; ?>
  </div>
</div>