<?php echo $header; ?>
<div class="row cat-image-title" style="display: none;">
    <?php if ($thumb) { ?>
      <img style="width: 100%;position: relative;float: left;" src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>"  />  
    <?php } ?>
    <div class="newsblog-title">
        <img class="col-lg-4 col-md-4 hidden-sm hidden-xs" style="display: inline-block;max-height: 100px;max-width: 233px;" src="catalog/view/theme/default/image/jhe/title-bg-6.png">
        <h2 class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="display: inline-block;"><?php echo $heading_title; ?></h2>
    </div>
</div>
<div class="container">

  <!-- slide banner-->

  <div class="row" style="width: 100%;">
    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" style="padding: 0px;">
      <div class="owl-carousel owl-theme"  id="carousel-slide">
        <?php for ($key = 0; $key < count($banners);$key++) { ?>
          <div class="item text-center" style="padding: 1px;">
            <?php if ($banners[$key]['link']) { ?>
            <a href="<?php echo $banners[$key]['link']; ?>" style="margin-bottom: 2px;position: relative;float: left;width: 100%;">
                <img style="width: 100%;" src="<?php echo $banners[$key]['image']; ?>" alt="<?php echo $banners[$key]['title']; ?>" class="img-responsive" />
            </a>
            <?php } else { ?>
                <img  style="margin-bottom: 2px;width: 100%;" src="<?php echo $banners[$key]['image']; ?>" alt="<?php echo $banners[$key]['title']; ?>" class="img-responsive" />
            <?php } ?>
          </div>
        <?php } ?>
      </div> 
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4  hidden-sm hidden-xs" style="padding: 0px 0px 0px 10px;">
      <a href="#" class="row" style="width: 100%; padding: 0px 0px 3px 0px;position: relative;float: left;"><img style="width: 100%;" src="<?php echo $banners[count($banners) - 2]['image']; ?>"></a>
      <a href="#" class="row" style="width: 100%; padding: 3px 0px 0px 0px;position: relative;float: left;"><img style="width: 100%;" src="<?php echo $banners[count($banners) - 1]['image']; ?>"></a>
    </div>
    <div class="row col-lg-12 col-md-12 col-xs-12 col-sm-12" style="padding: 5px 0px;"><?php echo $content_top; ?></div>
  </div>
  <!-- slide banner :: end -->

  <div class="row breadcrumb" style="background-color: #eaeaea !important;padding: 15px; border-left: 3px solid #09004b; border-radius: 0px; ">
    <h2 class="" style="width: 100%;"><?php echo $heading_title; ?></h2>
    <ul>
      <?php foreach ($breadcrumbs as $key => $breadcrumb) { if($key == count($breadcrumb)){ break;} ?>
      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
      <?php } ?>
    </ul>
  </div> 
  <?php if ($description) { ?>
  <div style="width: 100%;background: white;padding: 10px;"><?php echo $description; ?></div>
  <?php } ?>
 
   <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-lg-6 col-md-6 col-sm-12 col-xs-12'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-lg-9 col-md-9 col-sm-9 col-xs-12'; ?>
    <?php } else { ?>
    <?php $class = 'col-lg-12 col-md-12 col-xs-12 col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>" style="overflow: hidden;">
      <?php if ($thumb || $description) { ?>
      <div class="row hidden">
        <?php if ($thumb) { ?>
        <div class="col-sm-2"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" class="img-thumbnail" /></div>
        <?php } ?>
        <?php if ($description) { ?>
        <div class="col-sm-10"><?php echo $description; ?></div>
        <?php } ?>
      </div>
      <?php } ?>
      <?php if ($categories) { ?>
      <h3 class="hidden"><?php echo $text_refine; ?></h3>
      <?php if (count($categories) <= 5) { ?>
      <div class="row child-category">
          <ul>
            <?php foreach ($categories as $category) { ?>
            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
            <?php } ?>
          </ul>
      </div>
      <?php } else { ?>
      <div class="row">
        <?php foreach (array_chunk($categories, ceil(count($categories) / 4)) as $categories) { ?>
        <div class="col-sm-3">
          <ul>
            <?php foreach ($categories as $category) { ?>
            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
            <?php } ?>
          </ul>
        </div>
        <?php } ?>
      </div>
      <?php } ?>
      <?php } ?>
      <?php if ($products) { ?>
      <div class="row hidden">
        <div class="col-md-2 col-sm-6 hidden-xs">
          <div class="btn-group btn-group-sm hidden">
            <button type="button" id="list-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_list; ?>"><i class="fa fa-th-list"></i></button>
            <button type="button" id="grid-view" class="btn btn-default" data-toggle="tooltip" title="<?php echo $button_grid; ?>"><i class="fa fa-th"></i></button>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 hidden">
          <div class="form-group">
            <a href="<?php echo $compare; ?>" id="compare-total" class="btn btn-link"><?php echo $text_compare; ?></a>
          </div>
        </div>
        <div class="col-md-3 col-xs-4 col-lg-3 col-sm-3" style="float: right;">
          <div class="form-group input-group input-group-sm">
            <label class="input-group-addon" for="input-sort"><?php echo $text_sort; ?></label>
            <select id="input-sort" class="form-control" onchange="location = this.value;">
              <?php foreach ($sorts as $sorts) { ?>
              <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
              <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
              <?php } else { ?>
              <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
              <?php } ?>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="col-md-3 col-xs-6 hidden">
          <div class="form-group input-group input-group-sm">
            <label class="input-group-addon" for="input-limit"><?php echo $text_limit; ?></label>
            <select id="input-limit" class="form-control" onchange="location = this.value;">
              <?php foreach ($limits as $limits) { ?>
              <?php if ($limits['value'] == $limit) { ?>
              <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
              <?php } else { ?>
              <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
              <?php } ?>
              <?php } ?>
            </select>
          </div>
        </div>
      </div>


      <!-- -->

      <style type="text/css">
        @media (max-width: 767px) {
          .carousel-products{
            width: 120%;
          }
        }
        @media (min-width: 768px) and (max-width: 991px) {
          .carousel-products{
            width: 120%;
          }  
        }

      </style>    
      <div class="owl-carousel owl-theme carousel-products"  id="products">
        <?php foreach ($products as $key => $product) { ?>
          <div class="item text-center <?php echo ($product['khuyenmai']) ? 'khuyenmai-div' : ''; ?>">
            <?php if($product['khuyenmai']){ ?> 
              <?php $num_div = (count($product['thumb']) == 3) ? 4 : 6; ?>
              <h3 style="font-size: 14px;text-align: left;">KHUYẾN MÃI</h3>
              <a href="<?php echo $product['href']; ?>"><div class="row khuyenmai-image" style="width: 100%">
                <?php foreach($product['thumb'] as $khuyenmai_image){ ?>
                  <img class="row col-lg-<?php echo $num_div; ?>"  src="<?php echo $khuyenmai_image['image']; ?>" alt="<?php echo $khuyenmai_image['name']; ?>" />
                <?php } ?>
              </div></a>
              <div class="discount-text"><?php echo $product['meta_description']; ?></div>
            <?php }else{ ?>
              <a class="row" href="<?php echo $product['href']; ?>">
                  <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" class="img-responsive" />
                  <?php if ($product['special']) { ?>
                    <label class="home-discount">Giảm <?php echo $product['downpricerate']; ?></label>  
                  <?php } ?> 
              </a>
                  
              <div class="row info">
                <a href="<?php echo $product['href']; ?>"><h3 style="text-align: left;" ><?php echo $product['name']; ?></h3></a>
                <?php if ($product['price']) { ?>
                <p class="price">
                  <?php if (!$product['special']) { ?>
                    <strong><?php echo $product['price']; ?></strong>
                  <?php } else { ?>
                    <strong><?php echo $product['special']; ?></strong><span class="price-old"><?php echo $product['price']; ?></span>
                  <?php } ?>
                </p>
                <div class="discount-text"><?php echo ($key == 0) ? '' : $product['meta_description']; ?></div>
                <?php } ?>
              </div>
                   
            <?php } ?>
          </div> 
        <?php } ?>
      </div>
      <!-- -->

      <div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right hidden"><?php echo $results; ?></div>
      </div>
      <?php } ?>
      <?php if (!$categories && !$products) { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<script type="text/javascript">
$(document).ready(function(){

    var owl = $("#carousel-slide");
    owl.owlCarousel({
        navigation : false, 
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem: true,
        pagination: false,
        rewindSpeed: 500,
        items : 1,
        dots: false,
        autoplay: true 
    });  
});
</script>
<script type="text/javascript">
$(document).ready(function(){
    
    var fp = $("#products");

    if($(window).width() > 991){
        $('#products').removeClass('owl-carousel owl-theme  carousel-products').addClass('row col-lg-12 col-md-12');
        $('#products > div').removeClass('item').addClass('item-product-in-leaf');
    }else{
      fp.owlCarousel({
          navigation : false, 
          slideSpeed : 300,
          paginationSpeed : 400,
          singleItem: true,
          pagination: false,
          rewindSpeed: 500, 
          itemElement: 'div',
          stageElement: 'div',       
          dots: false,
          autoHeight: false,
          //lazyLoad: true,
          responsive:{
            0:{
                items:3,
                nav:true
            },
            767:{
                items:4,
                nav:false
            },
            1000:{
                items:5,
                nav:true,
                loop:false
            }
          }
      });       
    }

    $( window ).resize(function() {
      if($(window).width() > 991){
        $('#products').removeClass('owl-carousel owl-theme  carousel-products').addClass('row col-lg-12 col-md-12');
        $('#products > div').removeClass('item').addClass('item-product-in-leaf');
      }else{
        $('#products').hasClass('row').addClass('owl-carousel owl-theme  carousel-products');
        $('#products > div').hasClass('item-product-in-leaf').addClass('item');
        fp.owlCarousel({
            navigation : false, 
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem: true,
            pagination: false,
            rewindSpeed: 500, 
            itemElement: 'div',
            stageElement: 'div',       
            dots: false,
            autoHeight: false,
            //lazyLoad: true,
            responsive:{
              0:{
                  items:3,
                  nav:true
              },
              767:{
                  items:4,
                  nav:false
              },
              1000:{
                  items:5,
                  nav:true,
                  loop:false
              }
            }
        });               
      }      
    });


    //fp.trigger('initialized.owl.carousel');
    //fp.trigger('refreshed.owl.carousel');
    //fp.trigger('refreshed.owl.carousel');
    
    
    //fp.data('owl.carousel').settings.paginationSpeed = 400;  
    //fp.trigger('refresh.owl.carousel');    
    //fp.data('owl.carousel').settings.paginationSpeed = 500;  
    //fp.trigger('refresh.owl.carousel');
    
    //alert(maxHeight); 
 
    //fp.trigger('refresh.owl.carousel');
    //fp.trigger('refresh.owl.carousel');
    //fp.trigger('refreshed.owl.carousel');
});
</script>
<script type="text/javascript">
/*
$(document).ready(function(){
    var maxHeight = 0;
    var thisH = 0;
    var fp = $("#products");
    $('#products .owl-item').each(function(){
      thisH = $(this).height();
      if (thisH > maxHeight) { maxHeight = thisH; }
    });

    fp.on('refresh.owl.carousel', function(event){ 
        $('#products .owl-item').each(function(){
          $(this).css('height',maxHeight);
        });
    });

    fp.trigger('refresh.owl.carousel');
});
*/


</script>
<?php echo $footer; ?>
