function Search() {
    $("a.search").bind("click", function() {
        Xwidth > 1100 && (1 == show ? ($(".search-form").removeClass("show"), $("a.search").removeClass("active"), show = 0) : ($(".search-form").addClass("show"), $("a.search").addClass("active"), document.getElementById("search").reset(), show = 1))
    }), $("#qsearch").keydown(function(e) {
        13 == e.keyCode
    })
}

function SlidePicture() {
    function e() {
        $(".videos-slider").length && $(".videos-slider").each(function() {
            var e = $(this).find(".slide-item").length;
            $(window).width() > 880 ? e > 2 ? ($(this).find(".slide-item").css({
                display: "block",
                "float": "left",
                margin: 0
            }), $(this).find(".slide-controls").css({
                height: "auto"
            })) : 2 == e ? $(this).find(".slide-controls").css({
                height: 0
            }) : ($(this).find(".slide-stage").css({
                width: "100%"
            }), $(this).find(".slide-item").css({
                display: "inline-block",
                "float": "none",
                margin: "0 -5px"
            }), $(this).find(".slide-controls").css({
                height: 0
            })) : $(this).find(".slide-item").css({
                display: "block",
                "float": "left",
                margin: 0
            })
        }), $(".news-link").length && $(".news-link").each(function() {
            var e = $(this).find(".slide-item").length;
            $(window).width() > 700 ? e > 3 ? ($(this).find(".slide-item").css({
                display: "block",
                "float": "left",
                margin: 0
            }), $(this).find(".slide-controls").css({
                height: "auto"
            })) : 3 == e ? $(this).find(".slide-controls").css({
                height: 0
            }) : ($(this).find(".slide-stage").css({
                width: "100%"
            }), $(this).find(".slide-item").css({
                display: "inline-block",
                "float": "none",
                margin: "0 -5px"
            }), $(this).find(".slide-controls").css({
                height: 0
            })) : $(this).find(".slide-item").css({
                display: "block",
                "float": "left",
                margin: 0
            })
        })
    }
    if ($(".slider-home").length) {
        $(".left-content h2").lettering("words").children("span").lettering().children("span").lettering(), $(".left-content p").lettering("words").children("span").lettering();
        var t = $(".slider-home").attr("data-time"),
            i = $(".slider-bg .bg").length,
            o = !1,
            a = !1,
            n = !1,
            l = !1,
            s = !1;
        i > 1 && (o = !0, a = !0, n = !0, l = !0, s = !0), $(".slider-bg").BTQSlider({
            items: 1,
            animateOut: "fade-out",
            animateIn: "fade-in",
            loop: o,
            margin: 0,
            autoplay: a,
            autoplayTimeout: t,
            touchDrag: s,
            mouseDrag: l,
            smartSpeed: 600,
            nav: !1,
            dots: n,
            thumbsPrerendered: !1
        }), $(".slider-bg").on("translate.btq.slidebox", function() {}), $(".slider-bg").on("translated.btq.slidebox", function() {
            StopTime(), addMove()
        }), StopTime(), addMove()
    }
    if ($(".slider-bg-inner").length) {
        var t = $(".slider-inner").attr("data-time"),
            t = $(".slider-inner").attr("data-time"),
            i = $(".slider-inner .bg-inner").length,
            o = !1,
            a = !1,
            n = !1,
            l = !1,
            s = !1;
        i > 1 && (o = !0, a = !0, n = !0, l = !0, s = !0), $(".slider-bg-inner").BTQSlider({
            items: 1,
            animateOut: "fade-out",
            animateIn: "fade-in",
            loop: o,
            margin: 0,
            autoplay: a,
            autoplayTimeout: t,
            touchDrag: s,
            mouseDrag: l,
            smartSpeed: 600,
            nav: !1,
            dots: n,
            thumbsPrerendered: !1
        }), $(".slider-bg-inner").on("translate.btq.slidebox", function() {}), $(".slider-bg-inner").on("translated.btq.slidebox", function() {})
    }
    $(".project-new-slider").length && $(".project-new-slider").BTQSlider({
        loop: !0,
        margin: 0,
        autoplay: !0,
        autoplayTimeout: 5e3,
        smartSpeed: 600,
        nav: !1,
        dots: !0,
        dotNum: !1,
        responsive: {
            0: {
                items: 1
            },
            400: {
                items: 1
            },
            500: {
                items: 2
            },
            900: {
                items: 3
            }
        }
    }), $(".pics-slider").length && $(".pics-slider").BTQSlider({
        loop: !0,
        margin: 0,
        autoplay: !0,
        autoplayTimeout: 4e3,
        smartSpeed: 600,
        nav: !1,
        dotNum: !1,
        dots: !0,
        responsive: {
            0: {
                items: 1
            },
            300: {
                items: 2
            },
            350: {
                items: 2
            },
            700: {
                items: 3
            }
        }
    }), $(".videos-slider").length && ($(".videos-slider").BTQSlider({
        margin: 0,
        loop: !0,
        autoplay: !0,
        smartSpeed: 800,
        nav: !1,
        dotNum: !1,
        dots: !0,
        responsive: {
            0: {
                items: 1
            },
            400: {
                items: 2
            },
            880: {
                items: 3
            }
        }
    }), setTimeout(function() {
        e()
    }, 100)), $(".partners-slider").length && $(".partners-slider").BTQSlider({
        loop: !0,
        margin: 0,
        autoplay: !0,
        autoplayTimeout: 4e3,
        smartSpeed: 600,
        nav: !1,
        dots: !0,
        dotNum: !1,
        responsive: {
            0: {
                items: 1
            },
            350: {
                items: 2
            },
            700: {
                items: 3
            },
            1300: {
                items: 7
            }
        }
    }), $(".news-link").length && ($(".news-link").BTQSlider({
        loop: !1,
        margin: 0,
        URLhashListener: !0,
        startPosition: "URLHash",
        dotNum: !1,
        nav: !1,
        dots: !0,
        responsive: {
            0: {
                items: 1
            },
            350: {
                items: 2
            },
            700: {
                items: 3
            }
        }
    }), setTimeout(function() {
        e()
    }, 100)), $(".production-slider").length && $(".production-slider").BTQSlider({
        loop: !0,
        margin: 0,
        autoplay: !0,
        autoplayTimeout: 4e3,
        smartSpeed: 600,
        nav: !1,
        dots: !0,
        dotNum: !1,
        responsive: {
            0: {
                items: 1
            },
            400: {
                items: 2
            },
            700: {
                items: 3
            }
        }
    })
}

function VideoLoad(e) {
    $.ajax({
        url: e,
        cache: !0,
        success: function(e) {
            function t() {
                i.pause()
            }
            $(".allvideo").append(e), $(".allvideo").css({
                width: "100%",
                display: "block"
            }), $(".loadicon").fadeOut(300, "linear", function() {
                $(".loadicon").remove()
            });
            var i = document.getElementById("all-video"),
                o = $("#all-video").length;
            $(".close-video").click(function() {
                0 != o && t(), $(".video-list, .video-skin").fadeOut(500, "linear", function() {
                    $(".close-video").fadeOut(300, "linear"), $(".overlay-video").fadeOut(500, "linear", function() {
                        $(".allvideo").css({
                            width: 0,
                            display: "none"
                        }), $(".allvideo .video-list").remove(), $("body").removeClass("no-scroll"), $(".go-top").css({
                            display: "block"
                        })
                    })
                })
            })
        }
    })
}

function StopTime() {
    timex > 0 && (clearTimeout(timex), timex = 0)
}

function addMove() {
    $(".left-content").removeClass("move"), $(".left-content h2").children().children().removeClass("move"), $(".left-content p").children().removeClass("move"), $(".active").find(".left-content").addClass("move"), $(".move h2").children().children().each(function(e) {
        var t = $(this);
        timex = setTimeout(function() {
            $(t).addClass("move")
        }, 100 * (e + 1))
    }), $(".move p").children().each(function(e) {
        var t = $(this);
        timex = setTimeout(function() {
            $(t).addClass("move")
        }, 50 * (e + 1))
    })
}

function ScrollNiceHide() {
    null == isMobile.all || 0 == isMobile.all || isIE11 || isIE10 || isIE9 ? ($("body").getNiceScroll().show(), $("body").getNiceScroll().resize()) : ($("body").getNiceScroll().remove(), $("body").css({
        "overflow-y": "scroll",
        "overflow-x": "hidden"
    }))
}

function turnWheelTouch() {
    doWheel = !0, doTouch = !0
}

function ScrollHoz() {
    var e = $(".tab-outer");
    $(window).width() <= 1100 && ($(e).css({
        "overflow-x": "auto",
        "overflow-y": "hidden",
        "-ms-touch-action": "auto",
        "-ms-overflow-style": "none",
        overflow: " -moz-scrollbars-none",
        "-webkit-overflow-scrolling": "touch"
    }), $(e).animate({
        scrollLeft: "0px"
    }), 0 != TouchLenght && isTouchDevice || $(window).width() <= 1100 && ($(e).mousewheel(function(e, t) {
        e.preventDefault(), $(window).width() <= 1100 && (this.scrollLeft -= 40 * t)
    }), $(e).addClass("dragscroll"), $(".dragscroll").draptouch()))
}

function LinkPage() {
    $(".nav li a, .colum a, .project-view, .link-direct, .tab-menu a").click(function(e) {
        e.preventDefault();
        var t = $(e.target).attr("data-open");
        if (t && $(this).parent().parent().parent().hasClass("current")) return jumTo(t), !1;
        Xwidth <= 1100 && $(".overlay-menu.active").trigger("click");
        var i = $(this).attr("href");
        return $(".container").css({
            "-webkit-transition": "none",
            transition: "none"
        }), $(".container").stop().animate({
            opacity: 0
        }, 100, "linear", function() {
            $(".header, .logo, .slider-home, .slider-inner, .nav-click, .go-top").removeClass("show"), window.location = i
        }), !1
    })
}

function FocusText() {
    $("input:not(.input-quantity), textarea").focus(function() {
        $(this).attr("data-holder") == $(this).val() && $(this).val("")
    }).focusout(function() {
        "" == $(this).val() && $(this).val($(this).attr("data-holder"))
    })
}

function jumTo(e) {
    var t = $('.section[data-show= "' + e + '"]').offset().top,
        i = document.querySelector(".container");
    isCrios ? $(i).stop().animate({
        scrollTop: i.scrollTop + t - 70
    }, 800, "easeOutExpo") : Xwidth > 1100 ? $("html, body").stop().animate({
        scrollTop: t - 40
    }, 800, "easeOutExpo") : $("html, body").stop().animate({
        scrollTop: t - 70
    }, 800, "easeOutExpo")
}

function ContentLoad() {
    if (ResizeWindows(), SlidePicture(), detectHeight(), Option(), LinkPage(), FocusText(), ScrollHoz(), Finished(), setTimeout(function() {
            $(".tab-menu, .contact-info h2, .recruitment h2").first().addClass("fadeinup")
        }, 1e3), $("#home-page").length && ($(".link-home").addClass("current"), $(".project-new-item").click(function() {
            $(this).find("a").trigger("click")
        }), initialize()), $("#about-page").length && $(".colum li.sub-current").length) {
        var e = $(".colum li.sub-current a").attr("data-open");
        setTimeout(function() {
            var t = $('.section[data-show= "' + e + '"]').offset().top;
            if (isCrios) $(".container").stop().animate({
                scrollTop: t - 70
            }, 2500, "easeOutExpo");
            else {
                var t = $('.section[data-show= "' + e + '"]').offset().top;
                $(window).width() > 1100 ? $("html, body").stop().animate({
                    scrollTop: t - 140
                }, 2500, "easeOutExpo") : $("html, body").stop().animate({
                    scrollTop: t - 70
                }, 2500, "easeOutExpo")
            }
        }, 200)
    }
    if ($("#services-page").length) {
        if ($(".colum li.sub-current").length) {
            var e = $(".colum li.sub-current a").attr("data-open");
            setTimeout(function() {
                var t = $('.section[data-show= "' + e + '"]').offset().top;
                if (isCrios) $(".container").stop().animate({
                    scrollTop: t - 70
                }, 2500, "easeOutExpo");
                else {
                    var t = $('.section[data-show= "' + e + '"]').offset().top;
                    $(window).width() > 1100 ? $("html, body").stop().animate({
                        scrollTop: t - 140
                    }, 2500, "easeOutExpo") : $("html, body").stop().animate({
                        scrollTop: t - 70
                    }, 2500, "easeOutExpo")
                }
            }, 200)
        }
        $(".item img").addClass("zoom-pic"), ZoomPic()
    }
    if ($("#projects-page").length) {
        var t = document.querySelector(".filter-box");
        [].slice.call(document.querySelectorAll(".project-box"));
        isoBoxs = new Isotope(t, {
            isResizeBound: !1,
            itemSelector: ".project-box",
            percentPosition: !0,
            transitionDuration: "0.8s"
        });
        var i = $(".data-updating").text();
        $(".project-box").length || $(".filter-box").append('<p class="no-result">' + i + "</p>"), $(".select-header").bind("click", function() {
            $(".select-header").hasClass("onclick") ? ($(".select-header").removeClass("onclick"), $(this).next(".select-box").fadeOut(200, "linear")) : ($(this).addClass("onclick"), $(this).next(".select-box").fadeIn(200, "linear"), $(this).closest(".select-list").on("mouseleave", function() {
                $(this).find(".select-box").fadeOut(200, "linear"), $(".select-header").removeClass("onclick")
            }))
        }), $(".select-box li a").on("click", function(e) {
            e.preventDefault(), $(".colum-content").css({
                height: "auto"
            }), $(".select-box li").removeClass("selected"), $(this).parent().parent().parent().parent().find(".select-header h3").text($(this).text()), $(this).parent().addClass("selected"), $(this).closest(".select-box").fadeOut(200, "linear"), $(".select-header").removeClass("onclick");
            var t = $(this).attr("data-filter");
            return isoBoxs.arrange({
                filter: t
            }), $(".project-box" + t).length ? ($(".no-result").remove(), $(".project-box" + t).each(function(e, t) {
                var i = $(t).find("img").attr("data-src");
                $(t).find("img").attr("src", i), imagesLoaded(t, "done", function() {
                    isoBoxs.layout()
                })
            })) : ($(".no-result").remove(), $(".filter-box").append('<p class="no-result">' + i + "</p>")), !1
        }), $(".project-box").click(function(e) {
            e.preventDefault();
            var t = $(this).find("a").attr("href");
            $("body").append(pageLoad);
            var i = ($(this).find("a").attr("data-details"), $(this).find("a").attr("href")),
                o = $(this).find("a").attr("data-title"),
                a = $(this).find("a").attr("data-keyword"),
                n = $(this).find("a").attr("data-description"),
                l = $(this).find("a").attr("data-details");
            return changeUrl(i, o, n, a, l, o, n), ProjectLoad(t), !1
        }), $(".select-box li:first-child").addClass("selected"), $(".project-box a.current").length && $(".project-box a.current").trigger("click"), setTimeout(detectBut, 300)
    }
    $("#market-page").length && ($(".market-map img").addClass("zoom-pic"), ZoomPic()), $("#news-page").length && ($(".link-page").click(function(e) {
        e.preventDefault(), $(".link-page").removeClass("current"), $(this).addClass("current");
        var t = ($(this).find("a").attr("data-details"), $(this).find("a").attr("href")),
            i = $(this).find("a").attr("data-title"),
            o = $(this).find("a").attr("data-keyword"),
            a = $(this).find("a").attr("data-description"),
            n = $(this).find("a").attr("data-details");
        changeUrl(t, i, a, o, n, i, a);
        var l = $(this).find("a").attr("href");
        return $(".loadicon").length || $("body").append(pageLoad), $(".news-load").stop().animate({
            opacity: 0
        }, 600, "linear", function() {
            NewsLoad(l)
        }), !1
    }), $(".link-page.current").length ? $(".link-page.current a").trigger("click") : $(".slide-item:first-child .link-page:first a").trigger("click")), $("#recruitment-page").length && ($(".tab-slider li a").click(function(e) {
        e.preventDefault(), $(".tab-slider li").removeClass("current"), $(this).parent().addClass("current");
        var t = ($(this).attr("data-details"), $(this).attr("href")),
            i = $(this).attr("data-title"),
            o = $(this).attr("data-keyword"),
            a = $(this).attr("data-description"),
            n = $(this).attr("data-details");
        changeUrl(t, i, a, o, n, i, a);
        var l = $(this).attr("href");
        return $(".loadicon").length || $("body").append(pageLoad), $(".recruitment-load").stop().animate({
            opacity: 0
        }, 600, "linear", function() {
            RecruitmentLoad(l)
        }), !1
    }), $(".tab-slider li.current").length ? $(".tab-slider li.current a").trigger("click") : $(".tab-slider li:first a").trigger("click")), $("#contact-page").length && initialize(), $("#search-page").length && $(".section").addClass("on-show")
}

function Finished() {
    var e = .2;
    $("#projects-page").length && (e = 0), $("#win-scroll").addClass("activated");
    var t = !0;
    isIE9 ? ($(".loadicon").fadeOut(300, "linear", function() {
        $(".loadicon").remove()
    }), $(".header, .logo, .slider-home, .slider-inner, .nav-click, .content-page").addClass("show"), new Scroller(document.getElementById("win-scroll"), {
        minDuration: .4,
        maxDuration: .9,
        viewportFactor: e
    })) : $("#win-scroll").one(AnimEnd, function() {
        t && ($(".loadicon").fadeOut(300, "linear", function() {
            $(".loadicon").remove()
        }), $(".header, .logo, .slider-home, .slider-inner, .nav-click, .content-page").addClass("show"), new Scroller(document.getElementById("win-scroll"), {
            minDuration: .4,
            maxDuration: .9,
            viewportFactor: e
        })), t = !1
    })
}

function NewsLoad(e) {
    $.ajax({
        url: e,
        cache: !0,
        success: function(e) {
            $(".news-load").children().remove(), $(".news-load").append(e), $(".news-text p a").attr("target", "_blank"), $(".news-text img").addClass("zoom-pic"), ZoomPic(), $(".news-load").animate({
                opacity: 1
            }, 600, "linear", function() {
                $(".news-load").children().addClass(ajaxClass), $(".loadicon").fadeOut(300, "linear", function() {
                    $(".loadicon").remove()
                }), detectBut()
            })
        }
    })
}

function RecruitmentLoad(e) {
    $.ajax({
        url: e,
        cache: !0,
        success: function(e) {
            $(".recruitment-load").children().remove(), $(".recruitment-load").append(e), $(".recruitment-text p a").attr("target", "_blank"), $(".recruitment-text img").addClass("zoom-pic"), ZoomPic(), $(".recruitment-load").animate({
                opacity: 1
            }, 600, "linear", function() {
                $(".recruitment-load").children().addClass(ajaxClass), $(".loadicon").fadeOut(300, "linear", function() {
                    $(".loadicon").remove()
                }), detectBut()
            })
        }
    })
}

function ProjectLoad(e) {
    $.ajax({
        url: e,
        cache: !0,
        success: function(e) {
            $(".filter-result").prepend(e), $(".bottom, .filter-top, .filter-box").addClass("hide"), $(".center-detail a").attr("target", "_blank"), $(".loadicon").fadeOut(300, "linear", function() {
                $(".popup-page").addClass(popClass);
                var e = $(".filter-result").innerHeight(),
                    t = $(".popup-page").innerHeight();
                t > e && $(".filter-result").css({
                    height: t
                });
                var i = $(".tab-menu").offset().top;
                $(".tab-menu").innerHeight();
                $(window).width() > 1100 ? $("html, body").stop().animate({
                    scrollTop: i - 40
                }, 800, "linear", function() {
                    $(".header, .logo").hasClass("hide") || setTimeout(function() {
                        $(".header, .logo").addClass("hide")
                    }, 300)
                }) : isCrios ? $(".container").stop().animate({
                    scrollTop: 150
                }, 800, "linear") : $("html, body").stop().animate({
                    scrollTop: i - 70
                }, 800, "linear"), $(".loadicon").remove()
            }), $(".center-pic img").addClass("zoom-pic"), ZoomPic(), setTimeout(function() {
                $(".popup-page").hasClass(popClass) || $(".popup-page").addClass(popClass), $(".popup-page").css({
                    "min-height": $(".content-page").innerHeight() - $(".tab-menu").innerHeight() + 5
                })
            }, 310), $(".center-slider").length && $(".center-slider").BTQSlider({
                loop: !0,
                autoplay: !0,
                autoplayTimeout: 4e3,
                margin: 0,
                items: 1,
                smartSpeed: 600,
                nav: !1,
                dots: !0
            }), $(".close-popup, .close-popup-mobile, .black-popup, .bottom").click(function(e) {
                e.preventDefault();
                var t = $(this).attr("href"),
                    i = $(this).attr("data-title"),
                    o = $(this).attr("data-keyword"),
                    a = $(this).attr("data-description"),
                    n = $(this).attr("data-name");
                return changeUrl(t, i, a, o, n, i, a), $(".popup-page").removeClass("fadein").addClass("fadeout"), setTimeout(function() {
                    $(".bottom, .filter-top, .filter-box").removeClass("hide"), $(".filter-result").css({
                        height: "auto"
                    }), $(".popup-page").remove(), $(".close-popup-mobile").remove(), $(".header, .logo").removeClass("index-level"), $(".go-top, .nav-click, .language-sp").removeClass("index-level_0"), $("body").removeClass("no-scroll")
                }, 500), !1
            })
        }
    })
}

function Option() {
    var e = document.querySelector(".container");
    isCrios ? $(e).bind("scroll", function() {
        var t = e.scrollTop;
        t > 50 ? $(".scroll-down").fadeOut(500, "linear").css({
            opacity: 0
        }) : $(".scroll-down").fadeIn(500, "linear").css({
            opacity: 1
        }), t >= 50 ? $(".header, .logo").addClass("hide") : $(".header, .logo").removeClass("hide"), t > $(window).height() ? $(".go-top").addClass("show") : $(".go-top").removeClass("show")
    }) : $(document).bind("scroll", function() {
        var e = document.documentElement && document.documentElement.scrollTop || document.body.scrollTop;
        Xwidth <= 1100 && (e > 50 ? $(".scroll-down").fadeOut(500, "linear") : $(".scroll-down").fadeIn(500, "linear")), Xwidth > 1100 && (e >= 50 ? $(".scroll-down-desktop").removeClass("fadeon").addClass("fadeoff") : $(".scroll-down-desktop").removeClass("fadeoff").addClass("fadeon")), e >= windscroll && windscroll >= 100 ? $(".header, .logo").addClass("hide") : $(".header, .logo").removeClass("hide"), e > $(window).height() ? $(".go-top").addClass("show") : $(".go-top").removeClass("show"), windscroll = e
    }), $(".go-top").click(function() {
        isCrios ? $(".container").stop().animate({
            scrollTop: 0
        }, "slow") : $("html, body").stop().animate({
            scrollTop: 0
        }, "slow")
    }), $(".scroll-down-desktop").click(function(e) {
        return e.preventDefault(), $("html, body").animate({
            scrollTop: $(window).height() - 200
        }, "slow"), !1
    }), $(".zoom").click(function(e) {
        e.preventDefault(), $("body").addClass("no-scroll"), $(this).parent().addClass("to-scroll"), $(".loadicon").length || $("body").append('<div class="loadicon" style="display:block"></div>'), $(".all-pics").css({
            display: "block"
        }), $(".all-pics").append('<div class="full"  style="display:block"></div>'), $(".overlay-dark").fadeIn(300, "linear");
        var t = $(this).parent().find("img").attr("src") || $(this).parent().find("img").attr("data-src"),
            i = t.replace("_s", "_l");
        return $(".all-pics").find(".full").append('<img src ="' + i + '" alt="pic" />'), $("body").append('<div class="close-pics"></div>'), $(".all-pics").append('<div class="close-pics-small"></div>'), $(".all-pics img").load(function() {
            $(".all-pics").addClass("show"), 0 != TouchLenght && isTouchDevice ? ($(".full").addClass("pinch-zoom"), $(".pinch-zoom").each(function() {
                new Pic.PinchZoom($(this), {})
            })) : ($(".full").addClass("dragscroll"), $(".dragscroll").draptouch()), $(".full img").length > 1 && $(".full img").last().remove(), $(".loadicon").fadeOut(400, "linear", function() {
                0 != TouchLenght && isTouchDevice || detectMargin(), $(".full img").addClass("fadein"), $(".loadicon").remove()
            })
        }), $(".close-pics, .close-pics-small").click(function() {
            $(".loadicon").remove(), $(".full, .close-pics, .close-pics-small").fadeOut(300, "linear"), $(".overlay-dark").fadeOut(300, "linear", function() {
                if ($(".all-pics .full, .all-pics .text-length, .all-pics .pinch-zoom-container").remove(), $(".close-pics, .close-pics-small").remove(), $(".all-pics").css({
                        display: "none"
                    }).removeClass("show"), $("body").removeClass("no-scroll"), $(".to-scroll").length) {
                    var e = $(".to-scroll").offset().top;
                    $(".to-scroll").removeClass("to-scroll"), $(window).width() < 1100 && $("html, body").scrollTop(e - 60)
                }
            })
        }), !1
    })
}

function ZoomPic() {
    $("img").click(function() {
        if ($(window).width() <= 740 && $(this).hasClass("zoom-pic")) {
            $("body").addClass("no-scroll"), $(".loadicon").length || $("body").append('<div class="loadicon" style="display:block"></div>'), $(".all-pics").css({
                display: "block"
            }), $(".all-pics").append('<div class="full"  style="display:block"></div>'), $(".overlay-dark").fadeIn(300, "linear");
            var e = $(this).attr("src");
            $(".all-pics").find(".full").append('<img src ="' + e + '" alt="pic" />'), $(".all-pics").append('<div class="close-pics-small"></div>'), $("body").append('<div class="close-pics"></div>'), $(".all-pics img").load(function() {
                $(".all-pics").addClass("show"), 0 != TouchLenght && isTouchDevice ? ($(".full").addClass("pinch-zoom"), $(".pinch-zoom").each(function() {
                    new Pic.PinchZoom($(this), {})
                })) : ($(".full").addClass("dragscroll"), $(".dragscroll").draptouch()), $(".full img").length > 1 && $(".full img").last().remove(), $(".loadicon").fadeOut(400, "linear", function() {
                    0 != TouchLenght && isTouchDevice || detectMargin(), $(".full img").addClass("fadein"), $(".loadicon").remove()
                })
            }), $(".close-pics-small, .close-pics, .overlay-dark").click(function() {
                $(".loadicon").remove(), $(".full, .close-pics-small, .close-pics, .overlay-dark").fadeOut(300, "linear", function() {
                    $(".all-pics .full,  .all-pics .pinch-zoom-container").remove(), $(".all-pics").css({
                        display: "none"
                    }).removeClass("show"), $(".album-pic-center").length || $("body").removeClass("no-scroll"), $(".close-pics-small, .close-pics").remove()
                })
            })
        }
        return !1
    })
}

function detectMargin() {
    var e = $(".full").children().width(),
        t = $(".full").children().height();
    Xwidth > e ? $(".full").children().css({
        "margin-left": Xwidth / 2 - e / 2
    }) : $(".full").children().css({
        "margin-left": 0
    }), $(window).height() > t ? $(".full").children().css({
        "margin-top": $(window).height() / 2 - t / 2
    }) : $(".full").children().css({
        "margin-top": 0
    })
}

function detectZoom() {
    var e = $(".full img").width();
    $(".full img").height();
    e > Xwidth ? ($(".show-zoom").addClass("show"), $(".full img").addClass("fullsize")) : $(".full img").removeClass("fullsize")
}

function detectHeight() {
    if (Xwidth <= 1100) {
        var e = $(document).innerHeight();
        e > $(window).height() + 100 ? $(".scroll-down").css({
            display: "block",
            opacity: 1
        }) : $(".scroll-down").css({
            display: "none",
            opacity: 0
        })
    }
}

function detectBut() {
    if ($(".tab-outer").length && $(".tab-outer ul").length) {
        var e = $(".tab-outer ul").offset().left,
            t = $(".tab-outer li.current").offset().left,
            i = Xwidth / 100 * 10;
        (Xwidth - i) / 2 - $(".product-nav li.current").width() / 2;
        $(".tab-outer").stop().animate({
            scrollLeft: t - e
        }, "slow", function() {})
    }
    if ($(".tab-slider").length && $(".tab-slider ul").length) {
        var e = $(".tab-slider ul").offset().left,
            t = $(".tab-slider li.current").offset().left,
            i = Xwidth / 100 * 10;
        (Xwidth - i) / 2 - $(".product-nav li.current").width() / 2;
        $(".tab-slider").stop().animate({
            scrollLeft: t - e
        }, "slow", function() {})
    }
}

function execSearch() {
    var e = $("#qsearch").val(),
        t = $("#href_search").val(),
        i = $("#defaultvalue").val(),
        o = $("#errorsearch").val();
    if (hidemsg(), e == i || "" == e) return !1;
    if (e.length <= 1) return $("html, body").animate({
        scrollTop: 0
    }, "slow"), $(".overlay-dark").after("<div  class='contact-success color-red'>" + o + "</div>"), setTimeout(hidemsg, 5e3), !1;
    if ("" != e) {
        var a = t + "?qsearch=" + encodeURIComponent(e);
        return window.location = a, !1
    }
}

function LocationHash() {
    var e = window.location.hash;
    e = e.slice(1), Arrhash = e.split("/"), $(".link-page a[data-details='" + e + "']").trigger("click"), $(".project-bg a[data-details='" + e + "']").trigger("click"), $(".tab-slider li a[data-details='" + e + "']").trigger("click")
}! function(e) {
    var t = {
        on: e.fn.on,
        bind: e.fn.bind
    };
    e.each(t, function(i) {
        e.fn[i] = function() {
            var e, o = [].slice.call(arguments),
                a = o.pop(),
                n = o.pop();
            return o.push(function() {
                var t = this,
                    i = arguments;
                clearTimeout(e), e = setTimeout(function() {
                    n.apply(t, [].slice.call(i))
                }, a)
            }), t[i].apply(this, isNaN(a) ? arguments : o)
        }
    })
}(jQuery);
var isoBoxs = null,
    doWheel = !0,
    doTouch = !0,
    show = 0,
    timex, windscroll = $(document).scrollTop();
! function(e) {
    "use strict";

    function t() {
        var t = s.clientHeight,
            i = e.innerHeight;
        return i > t ? i : t
    }

    function i() {
        var t = e.pageYOffset || s.scrollTop;
        return isCrios && (t = r.scrollTop), t
    }

    function o(e) {
        var t = 0,
            i = 0;
        do isNaN(e.offsetTop) || (t += e.offsetTop), isNaN(e.offsetLeft) || (i += e.offsetLeft); while (e = e.offsetParent);
        return {
            top: t,
            left: i
        }
    }

    function a(e, a) {
        var n = e.offsetHeight,
            l = i(),
            s = l + t(),
            r = o(e).top,
            c = r + n,
            a = a || 0;
        return s + 50 >= r + n * a && c - n * a >= l
    }

    function n(e, t) {
        for (var i in t) t.hasOwnProperty(i) && (e[i] = t[i]);
        return e
    }

    function l(e, t) {
        this.el = e, this.options = n(this.defaults, t), this._init()
    }
    var s = e.document.documentElement,
        r = document.querySelector(".container");
    l.prototype = {
        defaults: {
            minDuration: 0,
            maxDuration: 0,
            viewportFactor: 0
        },
        _init: function() {
            this.items = [].slice.call(document.querySelectorAll(".section, .project-box")), this.itemsCount = this.items.length, this.itemsRenderedCount = 0, this.didScroll = !1;
            var t = this;
            t.items.forEach(function(e) {
                if (a(e, t.options.viewportFactor)) {
                    if (t.checkRender(), classie.has(e, "project-box")) {
                        var i = $(e).find("img").attr("data-src");
                        $(e).find("img").attr("src", i), imagesLoaded(e, "done", function() {
                            isoBoxs.layout(), $(".popup-page").css({
                                "min-height": $(".content-page").innerHeight() - $(".tab-menu").innerHeight() + 5
                            })
                        })
                    }
                    classie.add(e, aniClass)
                }
            }), isCrios ? (r.addEventListener("scroll", function() {
                t.scrollHandler()
            }, !1), r.addEventListener("resize", function() {
                t.resizeHandler()
            }, !1)) : (e.addEventListener("scroll", function() {
                t.scrollHandler()
            }, !1), e.addEventListener("resize", function() {
                t.resizeHandler()
            }, !1))
        },
        scrollHandler: function() {
            var e = this;
            e.didScroll || (this.didScroll = !0, setTimeout(function() {
                e.onScroll()
            }, 60))
        },
        onScroll: function() {
            var e = this;
            e.items.forEach(function(t) {
                !classie.has(t, "on-show") && a(t, e.options.viewportFactor) && setTimeout(function() {
                    if (e.checkRender(), classie.has(t, "project-box")) {
                        var i = $(t).find("img").attr("data-src");
                        $(t).find("img").attr("src", i), imagesLoaded(t, "done", function() {
                            isoBoxs.layout(), $(".popup-page").css({
                                "min-height": $(".content-page").innerHeight() - $(".tab-menu").innerHeight() + 5
                            })
                        })
                    }
                    classie.add(t, aniClass)
                }, 25)
            }), e.didScroll = !1
        },
        resizeHandler: function() {
            function e() {
                t.onScroll(), t.resizeTimeout = null
            }
            var t = this;
            t.resizeTimeout && clearTimeout(t.resizeTimeout), t.resizeTimeout = setTimeout(e, 1e3)
        },
        checkRender: function() {
            var t = this;
            ++t.itemsRenderedCount, t.itemsRenderedCount === t.itemsCount && e.removeEventListener("scroll", t.scrollHandler)
        }
    }, e.Scroller = l
}(window);
var timerScroll = null;
$(document).ready(function() {
    var e = document.getElementById("view-video");
    $("a.player, a.play-video").click(function(t) {
        t.preventDefault(), $("body").append('<div class="loadicon" style="display:block"><span class="circle"></span></div>');
        var i = $(this).attr("data-youtube"),
            o = $(this).attr("data-href");
        return i && i.length ? (e.style.display = "none", $(".video-youtube").html(i), $(".overlay-video").fadeIn(500, "linear", function() {
            $(".allvideo").css({
                width: "100%",
                display: "block"
            }), $(".loadicon").fadeOut(300, "linear", function() {
                $(".loadicon").remove()
            })
        }), $(".video-wrap iframe").css({
            width: $(".video-wrap iframe").attr("width"),
            height: $(".video-wrap iframe").attr("height"),
            top: $(window).height() / 2 - $(".video-wrap iframe").attr("height") / 2,
            left: $(window).width() / 2 - $(".video-wrap iframe").attr("width") / 2
        })) : o && o.length && (e.src = o, e.addEventListener("loadedmetadata", function() {
            $(".overlay-video").fadeIn(500, "linear", function() {
                $(".allvideo").css({
                    width: "100%",
                    display: "block"
                }), $(".loadicon").fadeOut(300, "linear", function() {
                    $(".loadicon").remove()
                })
            })
        }), e.play()), !1
    }), $(".close-video").click(function() {
        $(".allvideo").fadeOut(500, "linear", function() {
            $(".overlay-video").fadeOut(300, "linear", function() {
                $(".allvideo").css({
                    width: 0,
                    display: "none"
                }), $(".video-youtube").html(""), e.pause(), e.src = ""
            })
        })
    }), $("#home-page").length || ($(".logo").css({
        cursor: "pointer"
    }), $(".logo").click(function() {
        $(".link-home").trigger("click")
    })), $(".language a").click(function(e) {
        return e.preventDefault(), $(".language li").removeClass("active"), $(this).parent().addClass("active"), !1
    }), $(".nav-click").click(function() {
        $(".nav-click.active").length ? ($("body").removeClass("no-scroll"), ScrollBody(), $(".overlay-menu, .nav-click, .header, .language-sp").removeClass("active")) : ($(".top").css({
            "min-height": $(window).height() + 100
        }), $("body").getNiceScroll().remove(), $("body").addClass("no-scroll"), $(".overlay-menu, .nav-click, .header, .language-sp").addClass("active"))
    }), $(".overlay-menu").click(function() {
        $(".nav-click").trigger("click")
    }), $("a.search").click(function(e) {
        return e.preventDefault(), Xwidth > 1100 && (1 == show ? ($(".search-form").removeClass("show"), $("a.search").removeClass("active"), show = 0, execSearch()) : ($(".search-form").addClass("show"), $("a.search").addClass("active"), document.getElementById("search").reset(), show = 1)), !1
    }), $("#qsearch").keydown(function(e) {
        13 == e.keyCode && execSearch()
    }), $("#search input").click(function(e) {
        e.stopPropagation()
    }), $(".container, .nav, .right").click(function() {
        1 == show && $("a.search").trigger("click")
    });
    var t = document.querySelector(".overlay-menu");
    t.addEventListener("touchmove", function(e) {
        e.preventDefault()
    })
}), $(window).resize(function() {
    ResizeWindows(), $("#projects-page").length && isoBoxs.layout()
}), $(window).on("resize", function() {
    ResizeWindows(), $(window).width() > 740 && $(".all-pics").hasClass("show") && $(".close-pics-small, .close-pics").trigger("click"), $(window).width() > 1100 ? $(".nav-click.active").trigger("click") : detectHeight(), $("#projects-page").length && setTimeout(function() {
        isoBoxs.layout()
    }, 100), detectBut(), $(".album-pic-center").length && $(".all-pics").hasClass("show") || ScrollBody(), $(".dragscroll").length && (detectMargin(), $(".dragscroll").draptouch()), $(".popup-page").length && $(".popup-page").css({
        "min-height": $(".content-page").innerHeight() - $(".tab-menu").innerHeight() + 5
    })
}, 150), $(window).bind("popstate", function(e) {
    $(window).width() > 1100 && e.preventDefault();
    var t = $(".httpserver").text();
    if (null !== e.originalEvent.state) {
        var i = e.originalEvent.state.path,
            o = e.originalEvent.state.dataName,
            a = e.originalEvent.state.title,
            n = document.URL;
        changeUrl(i, a, "", "", o, "", "");
        var l = i.replace(t, "");
        l.split("/");
        $("#projects-page").length && ($(".close-popup").length && $(".close-popup").trigger("click"), $(".project-box a").each(function(e, t) {
            i == $(t).attr("href") && $(t).trigger("click")
        })), $("#news-page").length && ($(".nav li a").each(function(e, t) {
            i == $(t).attr("href") && window.history.back()
        }), $(".tab-outer li a").each(function(e, t) {
            i == $(t).attr("href") && window.history.back()
        }), $(".link-page a").each(function(e, t) {
            i == $(t).attr("href") && $(t).trigger("click")
        })), $("#recruitment-page").length && ($(".nav li a").each(function(e, t) {
            i == $(t).attr("href") && window.history.back()
        }), $(".tab-slider li a").each(function(e, t) {
            i == $(t).attr("href") && $(t).trigger("click")
        }))
    } else {
        var n = document.URL,
            s = n.replace(t, "");
        s.split("/");
        $("#projects-page").length && ($(".close-popup").length && $(".close-popup").trigger("click"), $(".project-box a").each(function(e, t) {
            n == $(t).attr("href") && $(t).trigger("click")
        })), $("#news-page").length && ($(".nav li a").each(function(e, t) {
            n == $(t).attr("href") && window.history.back()
        }), $(".tab-outer li a").each(function(e, t) {
            n == $(t).attr("href") && window.history.back()
        }), $(".link-page a").each(function(e, t) {
            n == $(t).attr("href") && $(t).trigger("click")
        })), $("#recruitment-page").length && ($(".nav li a").each(function(e, t) {
            n == $(t).attr("href") && window.history.back()
        }), $(".tab-slider li a").each(function(e, t) {
            n == $(t).attr("href") && $(t).trigger("click")
        }))
    }
});