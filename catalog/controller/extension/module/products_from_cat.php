<?php 
class ControllerExtensionModuleProductsFromCat extends Controller {

	public function index($setting) {

		static $module = 0;

		$this->language->load('extension/module/products_from_cat');
		
		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		
		$this->load->model('extension/module/products_from_cat');

		//$data['heading_title'] = $this->language->get('heading_title_personal');

		if (isset($setting['module_description'][$this->config->get('config_language_id')])) {
			$data['heading_title'] = html_entity_decode($setting['module_description'][$this->config->get('config_language_id')]['title'], ENT_QUOTES, 'UTF-8');
			//$data['html'] = html_entity_decode($setting['module_description'][$this->config->get('config_language_id')]['description'], ENT_QUOTES, 'UTF-8');
		} else {
			$data['heading_title'] = false;
			//$data['html'] = false;
		}		
		
		$limit = html_entity_decode($setting['limit']);

		$link = html_entity_decode($setting['link']);

		$data['min'] = explode(',', $setting['min']);

		$data['language'] = $this->config->get('config_language_id');


		 if((int)$link && count($setting['categories']) < 2){
		 	$data['link'] = $this->url->link('product/category', 'path=' . $setting['categories'][0]);
		 } else {
		 	$data['link'] = false;
		 }
		 
		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');
		//$data['heading_title'] = $setting['title'];

		$data['products'] = array();

		$data['module'] = $module++;

		$results = $this->model_extension_module_products_from_cat->getProducts($setting);

		foreach ($results as $key => $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
				}

				if($result['price'] == 0){
					$price = $this->language->get('text_contact');
				}elseif ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price = false;
				}

				if ((float)$result['special'] && (float)$result['price']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					$downpricerate = $this->currency->format($this->tax->calculate($result['price'] - $result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					$downrate = round(($result['price'] - $result['special'])*100/$result['price']) . ' %';
				} else {
					$special = false;
					$downpricerate = false;
					$downrate = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $result['rating'];
				} else {
					$rating = false;
				}

				/* calculator min-height */
				$min = 0;
				if(strlen($result['name']) < 30){
					$min = 0;
				}else{
					$min =+ 1;
				}
				
				/* calculator min-height :: end */

				$data['products'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $result['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
					'meta_description'	  => html_entity_decode($result['meta_description'], ENT_QUOTES, 'UTF-8'),
					// 'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'downpricerate' => $downpricerate,
					'downrate' => $downrate,
					//'tax'         => $tax,
					'rating'      => $rating,
					'href'        => $this->url->link('product/product', '&path=' . $this->model_catalog_product->getLongistPathString($result['product_id']) . '&product_id=' . $result['product_id']),
					//'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id']),
					'khuyenmai'   => false	
				);

				if(isset($result['khuyenmaisps']) && ($key < 1)){
					$khuyenmai_image_array = array();
					foreach ($result['khuyenmaisps'] as $key => $khuyenmaisp) {
						//coi cac san pham khuyen mai la thanh phan nhom
						if ($khuyenmaisp['image']) {
							$image = $this->model_tool_image->resize($khuyenmaisp['image'], $setting['width']/3, $setting['height']/3);
						} else {
							$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
						}
						$khuyenmai_image_array[] = array(
							'image' => $image,
							'name'	=> $khuyenmaisp['name']
						);
					}

					$data['products'][] = array(
						'khuyenmai'	=> true,	
						'product_id'  => $result['product_id'],
						'thumb'       => $khuyenmai_image_array,//sua
						'name'        => 'Khuyến mãi sản phẩm',
						'meta_description'	  => html_entity_decode($result['meta_description'], ENT_QUOTES, 'UTF-8'),
						'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
					);
				}
		}

		//var_dump($data['products']);
		if($setting['categories'][0] == 67){
			return $this->load->view('extension/module/products_from_cat_km', $data);
		}elseif($setting['categories'][0] == 69){
			return $this->load->view('extension/module/products_from_cat_pk', $data);	
		}else{
			return $this->load->view('extension/module/products_from_cat', $data);	
		}
		

	}
}
?>