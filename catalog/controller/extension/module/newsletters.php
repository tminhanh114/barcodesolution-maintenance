<?php
class ControllerExtensionModuleNewsletters extends Controller {

	public function index() {

		$this->load->language('extension/module/newsletter');

		$this->load->model('extension/module/newsletters');
		
		$this->model_extension_module_newsletters->createNewsletter();

		$data['text_when'] = $this->language->get('text_when');
		$data['text_when_comment'] = $this->language->get('text_when_comment');
		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_email'] = $this->language->get('text_email');
		$data['text_subscribe'] = $this->language->get('text_subscribe');
		$data['text_subscribe_title'] = $this->language->get('text_subscribe_title');
		$data['text_subscribe_comment'] = $this->language->get('text_subscribe_comment');
		$data['text_subscribe_btn'] = $this->language->get('text_subscribe_btn');
		$data['text_subscribe_placeholder'] = $this->language->get('text_subscribe_placeholder');

		$data['text_brochure_title'] = $this->language->get('text_brochure_title');
		$data['text_brochure_comment'] = $this->language->get('text_brochure_comment');

		$data['footer_hub'] = array(
			'text_news'	=> $this->language->get('text_news'),
			'news_hbcnews' => array('text' => $this->language->get('text_hbcnews'),'link' => $this->url->link('newsblog/category','&newsblog_path=1_5')),
			'news_jescoasianews' => array('text' => $this->language->get('text_jescoasianews'),'link' => $this->url->link('newsblog/category','&newsblog_path=1_6')),
			'news_jescohoabinhnews' => array('text' => $this->language->get('text_jescohoabinhnews'),'link' => $this->url->link('newsblog/category','&newsblog_path=1_7')),
			'news_careers' => array('text' => $this->language->get('text_careers'),'link' => $this->url->link('newsblog/category','newsblog_path=2')),
			'activity_sustainability' => $this->language->get('text_sustainability'),
			'activity_company' => array('text' => $this->language->get('text_activity_company'),'link' => $this->url->link('newsblog/category','newsblog_path=9')),
			'activity_social' => array('text' => $this->language->get('text_activity_social'),'link' => $this->url->link('newsblog/category','newsblog_path=10')),
			'investor_relation' => array('text' => $this->language->get('text_investor_relation'),'link' => $this->url->link('newsblog/category','newsblog_path=4')),
			'corporate_governance' => array('text' => $this->language->get('text_corporate_governance'),'link' => $this->url->link('newsblog/category','newsblog_path=4')),
			'announcements' => array('text' => $this->language->get('text_announcements'),'link' => $this->url->link('newsblog/category','newsblog_path=4')),
			'investor_activity' => array('text' => $this->language->get('text_investor_activity'),'link' => $this->url->link('newsblog/category','newsblog_path=4'))
		);


		
		$data['error_news_email_invalid'] = $this->language->get('error_news_email_invalid');
		$data['error_news_email_required'] = $this->language->get('error_news_email_required');
		$data['error_newsletter_sent'] = $this->language->get('error_newsletter_sent');
		$data['error_newsletter_fail'] = $this->language->get('error_newsletter_fail');

		return $this->load->view($this->config->get('config_template') . '/extension/module/newsletters', $data);
	}

	public function add() {

		$this->load->model('extension/module/newsletters');

		$json = array();
		$json['message'] = $this->model_extension_module_newsletters->addNewsletter($this->request->post);
		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}