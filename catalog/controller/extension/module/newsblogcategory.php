<?php
class ControllerExtensionModuleNewsblogCategory extends Controller {
	public function index($setting) {

		static $module = 0;

		if (isset($this->request->get['newsblog_path'])) {
			$parts = explode('_', $this->request->get['newsblog_path']);
		}else{
			$parts = array(
				0
			);
		}
		$newsblogcategory_arr =  explode(',',(string)$setting['newsblogcategory_arr']);
		//var_dump($newsblogcategory_arr);

		if(count($parts)){
			if(in_array($parts[count($parts) - 1], $newsblogcategory_arr)){

				//var_dump($setting['newsblogcategory_id']);
				
				$this->load->model('newsblog/category');

				$childCategories = $this->model_newsblog_category->getCategories((int)$setting['newsblogcategory_id']);

				//var_dump($childCategories);

				$data['categories'] = array();

				foreach ($childCategories as $category) {

					$children_data = array();

					$children = $this->model_newsblog_category->getCategories($category['category_id']);
					if($children){
						foreach($children as $child) {

							$children_data[] = array(
								'category_id' => $child['category_id'],
								'name' => $child['name'] ,
								'href' => $this->url->link('newsblog/category', 'newsblog_path=' . $category['category_id'] . '_' . $child['category_id'])
							);
						}	
					}

					$data['categories'][] = array(
						'category_id' => $category['category_id'],
						'name'        => $category['name'],
						'children'    => $children_data,
						'href'        => $this->url->link('newsblog/category', 'newsblog_path=' . $category['category_id'])
					);
					
				}
				

			}
		}
		
		//var_dump($data['categories']);


		$data['module'] = $module++;

		return $this->load->view('extension/module/newsblogcategory', $data);
	}
}