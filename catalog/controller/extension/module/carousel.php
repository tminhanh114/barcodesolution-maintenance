<?php
class ControllerExtensionModuleCarousel extends Controller {
	public function index($setting) {
		
		static $module = 0;

		$this->load->model('design/banner');
		$this->load->model('tool/image');
		//$this->load->model('newsblog/article');
		//$latest_article = $this->model_newsblog_category->getLatestarticles("1");

		$data['banners'] = array();
		$data['main_menu'] = array(
			'solution' => array('text' => 'Solution','link' => 'http://rfidsolution.com.vn/giai-phap-accesscontrol'),
			'rent' => array('text' => 'Rent','link' => '#'),
			'projects' => array('text' => 'Projects','link' => $this->url->link('product/filter','&filter=1')),
			'promotion' => array('text' => 'Promotion','link' => '#'),
			'scanner' => array('text' => 'Scanner','link' => $this->url->link('product/category','&path=65')),
			'printer' => array('text' => 'Printer','link' => $this->url->link('product/category','&path=68')),
			'accessories' => array('text' => 'Accessories','link' => $this->url->link('product/category','&path=69')),
			'repair' => array('text' => 'Repair','link' => $this->url->link('#')),
			'contact' => array('text' => 'Contact', 'link' => $this->url->link('information/contact'))
		);
		
		
		$results = $this->model_design_banner->getBanner($setting['banner_id']);
		$num = count($results);
		$data['heading_title'] = $results[0]['name'];

		foreach ($results as $key => $result) {
			if (is_file(DIR_IMAGE . $result['image']) && $setting['type'] == 1) {
				if($key < ($num - 3)){
					$image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
				}else{
					$image = $this->model_tool_image->resize($result['image'], 400, 110);
				}
				
				$data['banners'][] = array(
					'title' => $result['title'],
					'link'  => $result['link'],
					'image' => $image
				);
			}else{
				$data['banners'][] = array(
					'title' => $result['title'],
					'link'  => $result['link'],
					'image' => $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height'])
				);
			}
		}

		if($setting['type'] == 1){
			//TGDD - first row slider
			
			//$latest_article = $this->model_design_banner->getLatestarticles();	
			$data['latest_article'][] = array(
						'name' => $latest_article['name'],
						'href' => $this->url->link('newsblog/article', 'newsblog_path=1&newsblog_article_id=' . $latest_article['article_id'])
					);
			
		}		



		$data['module'] = $module++;

		if($setting['type'] == 1){
			//TGDD - first row slider
			return $this->load->view('extension/module/carousel_1', $data);
		}else{
			return $this->load->view('extension/module/carousel', $data);	
		}

		
	}
}