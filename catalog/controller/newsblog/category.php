<?php
//status = 1: group news with pagging
//status = 2: group news with slider
//status = 4: group news with slider and some category of news
//status = 5: group news and box of category
//status = 6: group news of career
//status = 7: group news and box of category (category has 3 news item)
class ControllerNewsBlogCategory extends Controller {
	public function index() {
		$this->load->language('newsblog/category');

		$this->load->model('newsblog/category');

		$this->load->model('newsblog/article');

		$this->load->model('tool/image');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		if (isset($this->request->get['newsblog_path'])) {
			$path = '';

			$parts = explode('_', (string)$this->request->get['newsblog_path']);

			$category_id = (int)array_pop($parts);

			foreach ($parts as $path_id) {
				if (!$path) {
					$path = (int)$path_id;
				} else {
					$path .= '_' . (int)$path_id;
				}

				$category_info = $this->model_newsblog_category->getCategory($path_id);

				if ($category_info) {
					$data['breadcrumbs'][] = array(
						'text' => $category_info['name'],
						'href' => $this->url->link('newsblog/category', 'newsblog_path=' . $path)
					);
				}
			}
		} else {
			$category_id = 0;
		}

		$category_info = $this->model_newsblog_category->getCategory($category_id);

		$data['category_id'] = $category_id;

		$status = $category_info['status'];

		if ($category_info) {

			$articles_image_size=array($this->config->get($this->config->get('config_theme') . '_image_product_width'),$this->config->get($this->config->get('config_theme') . '_image_product_height'));
			
	        $category_image_size=array($this->config->get($this->config->get('config_theme') . '_image_category_width'),$this->config->get($this->config->get('config_theme') . '_image_category_height'));


			$date_format=$this->language->get('date_format_short');
			if ($category_info['settings']) {
				$settings=unserialize($category_info['settings']);
	            $category_info=array_merge($category_info,$settings);

	            $articles_image_size=array($settings['images_size_width'],$settings['images_size_height']);
	            $category_image_size=array($settings['image_size_width'],$settings['image_size_height']);
	            $date_format=$settings['date_format'];
            }

			if ($category_info['meta_title']) {
				$this->document->setTitle($category_info['meta_title']);
			} else {
				$this->document->setTitle($category_info['name']);
			}

			$this->document->setDescription($category_info['meta_description']);
			$this->document->setKeywords($category_info['meta_keyword']);

			if ($category_info['meta_h1']) {
				$data['heading_title'] = $category_info['meta_h1'];
			} else {
				$data['heading_title'] = $category_info['name'];
			}



			$data['text_empty'] = $this->language->get('text_empty');
			$data['button_continue'] = $this->language->get('button_continue');
			$data['text_refine'] = $this->language->get('text_refine');
			$data['text_attributes'] = $this->language->get('text_attributes');

			$data['continue'] = $this->url->link('common/home');

			// Set the last category breadcrumb
			$data['breadcrumbs'][] = array(
				'text' => $category_info['name'],
				'href' => $this->url->link('newsblog/category', 'newsblog_path=' . $this->request->get['newsblog_path'])
			);

			if ($category_info['image']) {
				$data['original']	= HTTP_SERVER.'image/'.$category_info['image'];
				$data['thumb'] 		= $this->model_tool_image->resize($category_info['image'], $category_image_size[0], $category_image_size[1]);
			} else {
				$data['original'] 	= '';
				$data['thumb'] 		= '';
			}

			$data['description'] = html_entity_decode($category_info['description'], ENT_QUOTES, 'UTF-8');

			$data['categories'] = array();

			$categories = $this->model_newsblog_category->getCategories($category_id);

			foreach ($categories as $category) {
				if ($category['image']) {
					$original 	= HTTP_SERVER.'image/'.$category['image'];
					$thumb 		= $this->model_tool_image->resize($category['image'], $articles_image_size[0], $articles_image_size[1]);
				} else {
					$original 	= false;
					$thumb 		= false;
				}

				$filter_data = array(
					'filter_category_id' 	=> $category['category_id'],
					'filter_sub_category' 	=> true
				);

				$articles_total = $this->model_newsblog_article->getTotalArticles($filter_data);

				$data['categories'][] = array(
					'name' 			=> $category['name'],
					'total'       	=> $articles_total,
					'original'		=> $original,
					'thumb'			=> $thumb,
					'href' 			=> $this->url->link('newsblog/category', 'newsblog_path=' . $this->request->get['newsblog_path'] . '_' . $category['category_id'])
				);
			}

			//get articles of child category
			$childCategories = $this->model_newsblog_category->getCategories($category_id);

			$data['childCategories'] = array();

			$data['childCategories_new'] = array();


			if($childCategories){

				foreach ($childCategories as $key => $category) {

					$filter_data = array(
						'filter_category_id' => $category['category_id'],
						'sort'               => 'sort_order',
						'order'              => 'DESC'
					);
					
					$childArticles = $this->model_newsblog_article->getArticles($filter_data);

					if($childArticles){
						foreach ($childArticles as $value) {
							$arrayArticles[] = array(
								'article_id'  		=> $value['article_id'],
								'original'			=> HTTP_SERVER. 'image/' . $value['image'],
								'name'        		=> $value['name'],
								'preview'     		=> html_entity_decode($value['preview'], ENT_QUOTES, 'UTF-8'),
								'href'        		=> $this->url->link('newsblog/article', 'newsblog_path=' . $this->request->get['newsblog_path'] . '_' . $category['category_id'] . '&newsblog_article_id=' . $value['article_id']),
								'description' 		=> html_entity_decode($value['description'], ENT_QUOTES, 'UTF-8'),
								'attributes'  		=> $value['attributes'],
								'date'		  		=> ($date_format ? date($date_format, strtotime($value['date_available'])) : false)
							);
						}
						$data['childCategories'][] = array(
							'categoryName'	=> $category['name'],
							'category_id'	=> $category['category_id'],
							'articles'		=> $arrayArticles	
						);	

						$data['childCategories_new'][] =array(
							'categoryName'	=> $category['name'],
							'category_id'	=> $category['category_id'],						
							'href'			=> $this->url->link('newsblog/category', 'newsblog_path=' . $this->request->get['newsblog_path'] . '_' . $category['category_id']),
						); 
												
						$arrayArticles = [];
					}
					
				}	
			}

			//$this->log->write($data['childCategories']);
			//get articles of child category :: end
			//get articles of sibing category with parent id = 8 - hoat dong cong ty :: start
			if($status == 7){
				$siblingCategories = $this->model_newsblog_category->getCategories(8);
				//var_dump($siblingCategories);

				$data['siblingCategories'] = array();

				if($siblingCategories){

					foreach ($siblingCategories as $key => $category) {
					
						if($category['category_id'] == $category_id){
							continue;
						}

						$filter_data = array(
							'filter_category_id' => $category['category_id'],
							'sort'               => 'sort_order',
							'order'              => 'DESC',
							'limit'				=> '10'	
						);
						
						$childArticles = $this->model_newsblog_article->getArticles($filter_data);

						$arrayArticles = array();

						if($childArticles){
							foreach ($childArticles as $value) {
								$arrayArticles[] = array(
									'article_id'  		=> $value['article_id'],
									'name'        		=> $value['name'],
									'href'        		=> $this->url->link('newsblog/article', 'newsblog_path=' . $this->request->get['newsblog_path'] . '_' . $category['category_id'] . '&newsblog_article_id=' . $value['article_id']),
									'date'		  		=> ($date_format ? date($date_format, strtotime($value['date_available'])) : false)
								);
							}
							$data['siblingCategories'][] = array(
								'name'	=> $category['name'],
								'href'			=> $this->url->link('newsblog/category', 'newsblog_path=8_' . $category['category_id'], 'SSL'),
								'articles'		=> $arrayArticles	
							);	
						}
					}	
				}
			}
			//get articles of sibing category with parent id = 8 - hoat dong cong ty :: end



			$data['articles'] = array();

			$limit = $category_info['limit'];

			if ($limit>0) {

				$sort = $category_info['sort_by'];
				$order = $category_info['sort_direction'];

				$filter_data = array(
					'filter_category_id' => $category_id,
					'sort'               => $sort,
					'order'              => $order,
					'start'              => ($page - 1) * $limit,
					'limit'              => $limit
				);

				$article_total = $this->model_newsblog_article->getTotalArticles($filter_data);

				$results = $this->model_newsblog_article->getArticles($filter_data);


				foreach ($results as $result) {

					if ($result['image']) {
						$original 	= HTTP_SERVER.'image/'.$result['image'];
						$thumb 		= $this->model_tool_image->resize($result['image'], $articles_image_size[0], $articles_image_size[1]);
					} else {
						$original 	= '';
						$thumb 		= '';	//or use 'placeholder.png' if you need
					}

					$data['articles'][] = array(
						'article_id'  		=> $result['article_id'],
						'original'			=> $original,
						'thumb'       		=> $thumb,
						'name'        		=> $result['name'],
						'preview'     		=> html_entity_decode($result['preview'], ENT_QUOTES, 'UTF-8'),
						//'attributes'  		=> $result['attributes'],
						'href'        		=> $this->url->link('newsblog/article', 'newsblog_path=' . $this->request->get['newsblog_path'] . '&newsblog_article_id=' . $result['article_id']),
						//'date'		  		=> ($date_format ? date($date_format, strtotime($result['date_available'])) : false),
						//'date_modified'		=> ($date_format ? date($date_format, strtotime($result['date_modified'])) : false),
						//'viewed' 			=> $result['viewed']
						'description' => html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')
					);
				}


				$pagination = new Pagination();
				$pagination->total = $article_total;
				$pagination->page = $page;
				$pagination->limit = $limit;
				$pagination->url = $this->url->link('newsblog/category', 'newsblog_path=' . $this->request->get['newsblog_path'] . '&page={page}');

				$data['pagination'] = $pagination->render();

				$data['results'] = sprintf($this->language->get('text_pagination'), ($article_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($article_total - $limit)) ? $article_total : ((($page - 1) * $limit) + $limit), $article_total, ceil($article_total / $limit));

				// http://googlewebmastercentral.blogspot.com/2011/09/pagination-with-relnext-and-relprev.html
				if ($page == 1) {
				    $this->document->addLink($this->url->link('newsblog/category', 'newsblog_path=' . $category_info['category_id'], 'SSL'), 'canonical');
				} elseif ($page == 2) {
				    $this->document->addLink($this->url->link('newsblog/category', 'newsblog_path=' . $category_info['category_id'], 'SSL'), 'prev');
				} else {
				    $this->document->addLink($this->url->link('newsblog/category', 'newsblog_path=' . $category_info['category_id'] . '&page='. ($page - 1), 'SSL'), 'prev');
				}

				if ($limit && ceil($article_total / $limit) > $page) {
				    $this->document->addLink($this->url->link('newsblog/category', 'newsblog_path=' . $category_info['category_id'] . '&page='. ($page + 1), 'SSL'), 'next');
				}
			}

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if($status == 1){
				$template_default='category.tpl'; //default of group news	
				if ($category_info['template_category']) $template_default=$category_info['template_category'];
			}elseif ($status == 2){
				$template_default='category_slider.tpl';
			}elseif ($status == 3){
				$template_default='category_3.tpl';
			}elseif ($status == 4){
				$template_default='category_4.tpl';
			}elseif ($status == 5){
				$template_default='category_5.tpl';
			}elseif ($status == 6){
				$template_default='category_6.tpl';
			}elseif ($status == 7){
				$template_default='category_7.tpl';
			}elseif ($status == 9){
				$template_default='category_9.tpl';
			}elseif ($status == 10){
				$template_default='category_10.tpl';
			}


			//

			$this->response->setOutput($this->load->view('newsblog/'.$template_default, $data));

		} else {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('newsblog/category')
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('error/not_found.tpl', $data));
		}
	}
}
