<?php
class ControllerCommonHeader extends Controller {
	public function index() {
		// Analytics
		$this->load->model('extension/extension');

		$data['analytics'] = array();

		$analytics = $this->model_extension_extension->getExtensions('analytics');

		foreach ($analytics as $analytic) {
			if ($this->config->get($analytic['code'] . '_status')) {
				$data['analytics'][] = $this->load->controller('extension/analytics/' . $analytic['code'], $this->config->get($analytic['code'] . '_status'));
			}
		}

		if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}

		if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
			$this->document->addLink($server . 'image/' . $this->config->get('config_icon'), 'icon');
		}

		$data['title'] = $this->document->getTitle();

		$data['base'] = $server;
		$data['description'] = $this->document->getDescription();
		$data['image'] = $this->document->getImage();
		$data['imageHeight'] = $this->document->getImageHeight();
		$data['imageWidth'] = $this->document->getImageWidth();
		$data['keywords'] = $this->document->getKeywords();
		$data['links'] = $this->document->getLinks();
		$data['styles'] = $this->document->getStyles();
		$data['scripts'] = $this->document->getScripts();
		$data['lang'] = $this->language->get('code');
		$data['direction'] = $this->language->get('direction');

		$data['name'] = $this->config->get('config_name');

		if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
			$data['logo'] = $server . 'image/' . $this->config->get('config_logo');
		} else {
			$data['logo'] = '';
		}

		$this->load->language('common/header');

		$data['text_home'] = $this->language->get('text_home');

		// Wishlist
		if ($this->customer->isLogged()) {
			$this->load->model('account/wishlist');

			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
		} else {
			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
		}

		$data['text_shopping_cart'] = $this->language->get('text_shopping_cart');
		$data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', true), $this->customer->getFirstName(), $this->url->link('account/logout', '', true));

		$data['text_account'] = $this->language->get('text_account');
		$data['text_register'] = $this->language->get('text_register');
		$data['text_login'] = $this->language->get('text_login');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_transaction'] = $this->language->get('text_transaction');
		$data['text_download'] = $this->language->get('text_download');
		$data['text_logout'] = $this->language->get('text_logout');
		$data['text_checkout'] = $this->language->get('text_checkout');
		$data['text_category'] = $this->language->get('text_category');
		$data['text_all'] = $this->language->get('text_all');

		$data['home'] = $this->url->link('common/home');
		$data['wishlist'] = $this->url->link('account/wishlist', '', true);
		$data['logged'] = $this->customer->isLogged();
		$data['account'] = $this->url->link('account/account', '', true);
		$data['register'] = $this->url->link('account/register', '', true);
		$data['login'] = $this->url->link('account/login', '', true);
		$data['order'] = $this->url->link('account/order', '', true);
		$data['transaction'] = $this->url->link('account/transaction', '', true);
		$data['download'] = $this->url->link('account/download', '', true);
		$data['logout'] = $this->url->link('account/logout', '', true);
		$data['shopping_cart'] = $this->url->link('checkout/cart');
		$data['checkout'] = $this->url->link('checkout/checkout', '', true);
		$data['contact'] = $this->url->link('information/contact');
		//$data['telephone'] = $this->url-link('product/category','path=59');

		$data['main_menu'] = array(
			'aboutus' => array('text' => $this->language->get('text_aboutus'),'link' => $this->url->link('newsblog/category','newsblog_path=4')),
			'aboutus_overview' => array('text' => $this->language->get('text_overview'),'link' => $this->url->link('newsblog/category','newsblog_path=4')),
			'aboutus_vision' => array('text' => $this->language->get('text_vision'),'link' => $this->url->link('newsblog/category','newsblog_path=4')),
			'aboutus_leadership' => array('text' => $this->language->get('text_leadership'),'link' => $this->url->link('newsblog/article','newsblog_path=3&newsblog_article_id=3')),
			'aboutus_oganization' => array('text' => $this->language->get('text_oganization'),'link' => $this->url->link('newsblog/article','newsblog_path=3&newsblog_article_id=3')),
			'aboutus_engagement' => array('text' => $this->language->get('text_engagement'),'link' => $this->url->link('newsblog/article','newsblog_path=3&newsblog_article_id=3')),
			'aboutus_establish' => array('text' => $this->language->get('text_establish'),'link' => $this->url->link('newsblog/article','newsblog_path=3&newsblog_article_id=3')),
			'news' => array('text' => $this->language->get('text_news'),'link' => $this->url->link('product/filter','&filter=1')),
			'tech_news' => array('text' => $this->language->get('text_tech_news'),'link' => $this->url->link('newsblog/category','&newsblog_path=1_5')),
			'news_jescoasianews' => array('text' => $this->language->get('text_jescoasianews'),'link' => $this->url->link('newsblog/category','&newsblog_path=1_6')),
			'news_jescohoabinhnews' => array('text' => $this->language->get('text_jescohoabinhnews'),'link' => $this->url->link('newsblog/category','&newsblog_path=1_7')),
			'news_careers' => array('text' => $this->language->get('text_careers'),'link' => $this->url->link('newsblog/category','newsblog_path=2')),
			'buss' => array('text' => $this->language->get('text_buss'),'link' => $this->url->link('newsblog/category','newsblog_path=4')),
			'solution' => array('text' => $this->language->get('text_solution'),'link' => $this->url->link('newsblog/category','newsblog_path=9')),
			'rent' => array('text' => $this->language->get('text_rent'),'link' => $this->url->link('newsblog/category','newsblog_path=3')),
			'projects' => array('text' => $this->language->get('text_projects'),'link' => $this->url->link('product/filter','&filter=1')),
			'promotion' => array('text' => $this->language->get('text_promotion'),'link' => $this->url->link('newsblog/category','newsblog_path=7')),
			'scanner' => array('text' => $this->language->get('text_scanner'),'link' => $this->url->link('product/category','&path=65')),
			'printer' => array('text' => $this->language->get('text_printer'),'link' => $this->url->link('product/category','&path=68')),
			'accessories' => array('text' => $this->language->get('text_accessories'),'link' => $this->url->link('product/category','&path=69')),
			'projects_completed' => array('text' => $this->language->get('text_completed'),'link' => $this->url->link('newsblog/category','newsblog_path=4')),
			'activity' => array('text' => $this->language->get('text_activity'),'link' => $this->url->link('product/category','&path=64')),
			'activity_company' => array('text' => $this->language->get('text_activity_company'),'link' => $this->url->link('newsblog/category','newsblog_path=8_9')),
			'activity_social' => array('text' => $this->language->get('text_activity_social'),'link' => $this->url->link('newsblog/category','newsblog_path=8_10')),
			'activity_gallery' => array('text' => $this->language->get('text_activity_gallery'),'link' => $this->url->link('newsblog/category','newsblog_path=8_11')),
			'investor_relation' => array('text' => $this->language->get('text_investor_relation'),'link' => $this->url->link('newsblog/category','newsblog_path=4')),
			'corporate_governance' => array('text' => $this->language->get('text_corporate_governance'),'link' => $this->url->link('newsblog/category','newsblog_path=4')),
			'announcements' => array('text' => $this->language->get('text_announcements'),'link' => $this->url->link('newsblog/category','&newsblog_path=20_23')),
			'gms' => array('text' => $this->language->get('text_gms'),'link' => $this->url->link('newsblog/category','&newsblog_path=20_24')),
			'investor_activity' => array('text' => $this->language->get('text_investor_activity'),'link' => $this->url->link('product/manufacturer')),
			'contact' => array('text' => $this->language->get('text_contact'), 'link' => $this->url->link('information/contact'))
		);

		// Menu
		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$data['categories'] = array();

		$categories = $this->model_catalog_category->getCategories(0);

		foreach ($categories as $category) {
			if ($category['top']) {
				// Level 2
				$children_data = array();

				$children = $this->model_catalog_category->getCategories($category['category_id']);

				foreach ($children as $child) {
					$filter_data = array(
						'filter_category_id'  => $child['category_id'],
						'filter_sub_category' => true
					);

					$children_data[] = array(
						'name'  => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
						'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
					);
				}

				// Level 1
				$data['categories'][] = array(
					'name'     => $category['name'],
					'children' => $children_data,
					'column'   => $category['column'] ? $category['column'] : 1,
					'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
				);
			}
		}

		$data['language'] = $this->load->controller('common/language');
		$data['currency'] = $this->load->controller('common/currency');
		$data['search'] = $this->load->controller('common/search');
		$data['cart'] = $this->load->controller('common/cart');

		// For page specific css
		if (isset($this->request->get['route'])) {
			if (isset($this->request->get['product_id'])) {
				$class = '-' . $this->request->get['product_id'];
			} elseif (isset($this->request->get['path'])) {
				$class = '-' . $this->request->get['path'];
			} elseif (isset($this->request->get['manufacturer_id'])) {
				$class = '-' . $this->request->get['manufacturer_id'];
			} elseif (isset($this->request->get['information_id'])) {
				$class = '-' . $this->request->get['information_id'];
			} else {
				$class = '';
			}

			$data['class'] = str_replace('/', '-', $this->request->get['route']) . $class;
		} else {
			$data['class'] = 'common-home';
		}

		return $this->load->view('common/header', $data);
	}
}
