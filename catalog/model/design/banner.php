<?php
class ModelDesignBanner extends Model {
	public function getBanner($banner_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "banner b LEFT JOIN " . DB_PREFIX . "banner_image bi ON (b.banner_id = bi.banner_id) WHERE b.banner_id = '" . (int)$banner_id . "' AND b.status = '1' AND bi.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY bi.sort_order ASC");
		return $query->rows;
	}

	public function getLatestarticles() {

		$article_id_array = $this->db->query("SELECT article_id FROM latest_article");
		//var_dump($article_id_array);

		$query = $this->db->query("SELECT DISTINCT pd.name AS name, p.article_id as article_id
		FROM " . DB_PREFIX . "newsblog_article p
		LEFT JOIN " . DB_PREFIX . "newsblog_article_description pd ON (p.article_id = pd.article_id)
		LEFT JOIN " . DB_PREFIX . "newsblog_article_to_store p2s ON (p.article_id = p2s.article_id)

		WHERE p.article_id = '" . (int)$article_id_array->row['article_id'] . "' AND
		pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND
		p.status = '1' AND
		p.date_available <= NOW() AND
		p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'");

		var_dump("SELECT DISTINCT pd.name AS name, p.article_id as article_id
		FROM " . DB_PREFIX . "newsblog_article p
		LEFT JOIN " . DB_PREFIX . "newsblog_article_description pd ON (p.article_id = pd.article_id)
		LEFT JOIN " . DB_PREFIX . "newsblog_article_to_store p2s ON (p.article_id = p2s.article_id)

		WHERE p.article_id = '" . (int)$article_id_array->row[0] . "' AND
		pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND
		p.status = '1' AND
		p.date_available <= NOW() AND
		p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'");

		//var_dump($query->row);

		if ($query->num_rows) {
			return array(
				'article_id'       => $query->row['article_id'],
				'name'             => $query->row['name']
			);
		} else {
			return false;
		}

	}
}
