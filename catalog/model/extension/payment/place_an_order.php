<?php
class ModelExtensionPaymentPlaceAnOrder extends Model {
	public function getMethod($address, $total) {
		$this->load->language('extension/payment/place_an_order');

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('place_an_order_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

		if ($this->config->get('place_an_order_total') > 0 && $this->config->get('place_an_order_total') > $total) {
			$status = false;
		} elseif (!$this->config->get('place_an_order_geo_zone_id')) {
			$status = true;
		} elseif ($query->num_rows) {
			$status = true;
		} else {
			$status = false;
		}

		$method_data = array();

		//$languages = $this->model_localisation_language->getLanguages();

		if ($status) {
			$method_data = array(
				'code'       => 'place_an_order',
				'title'      => $this->language->get('text_title') . ' ' . $this->config->get('place_an_order_total') * 100 .  '% tương ứng số tiền là: ' . $this->currency->format($this->tax->calculate($this->config->get('place_an_order_total') * $total, $this->config->get('item_tax_class_id'), $this->config->get('config_tax')), $this->session->data['currency']),
				'bank'		=> 	html_entity_decode($this->config->get('place_an_order_bank2')),
				'terms'      => '',
				'sort_order' => $this->config->get('place_an_order_sort_order')
			);
		}

		return $method_data;
	}
}