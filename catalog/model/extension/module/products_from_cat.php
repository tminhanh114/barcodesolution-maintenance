<?php
class ModelExtensionModuleProductsFromCat extends Model {

	public function getProducts($setting) {

		$product_data = array();

		$products_ids = $this->getProductsInCats($setting['categories'], $setting['limit'], $setting['children']);

		if (count($products_ids) < 1) {
			return array();
		} else {
		
			if ($this->customer->isLogged()) {
				$customer_group_id = $this->customer->getId();
			} else {
				$customer_group_id = $this->config->get('config_customer_group_id');
			}	

			//var_dump($products_ids);
			
			foreach($products_ids as $key => $id) {
				$product_query = $this->db->query("SELECT *, 
				(SELECT ps.price FROM " . DB_PREFIX . "product_special ps WHERE p.product_id = ps.product_id AND ps.customer_group_id = '" . (int)$customer_group_id . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW()) ) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special, 
				(SELECT AVG(rating) AS total FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating,
				(SELECT COUNT(*) AS total FROM " . DB_PREFIX . "review r2 WHERE r2.product_id = p.product_id AND r2.status = '1' GROUP BY r2.product_id) AS reviews
				FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE p.product_id = '" . (int)$id . "' AND p.status = '1' AND p.date_available <= NOW() AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "' ORDER BY p.sort_order DESC");
				
				//$result = $product_query->row;

				if($key < 2){
					$km = $this->getProductKhuyenMaiSP($id);
				}else{
					$km = null;	
				}
				
				$product = $product_query->row;

				if ((count($km) > 0) && ($key < 2)){					
					$product_data[] = array(
						'product_id'  => $product['product_id'],
						'image'       => $product['image'],
						'name'        => $product['name'],
						'description' => $product['description'], 
						'meta_description'	=> $product['meta_description'],
						'price'       => $product['price'],
						'special'     => $product['special'],
						'rating'      => $product['rating'],
						'tax_class_id'     => $product['tax_class_id'],
						'khuyenmaisps' => $km
					);
				}else{
					$product_data[] = $product;
				}
				
			}

		
			return $product_data;
		}
	}

	private function getProductsInCats( $categories, $limit, $children = false ){

		if(!$children){
			$implode = implode(",", $categories);
		} else {
			$parents = implode(",", $categories);
			$rows = $this->db->query("SELECT category_id FROM " . DB_PREFIX ."category WHERE parent_id IN(" . $parents . ")");
			if(!empty($rows->rows)){
				foreach ($rows->rows as $row) {
					$categories[] = $row['category_id'];
				}
			}
			$implode = implode(",", $categories);
		}

		$query = $this->db->query("SELECT pc.product_id FROM " . DB_PREFIX . "product_to_category pc INNER JOIN " . DB_PREFIX . "product p ON pc.product_id = p.product_id  WHERE category_id IN( ". $implode ." ) ORDER BY p.sort_order DESC, p.product_id DESC LIMIT ".$limit );

		$product_ids = array();

		foreach ($query->rows as $row) {
			$product_ids[] = $row['product_id'];
		}

		return $product_ids;
	}

	private function getProductKhuyenMaiSP($product_id) {
		$product_data = array();
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_khuyenmaisp pr LEFT JOIN " . DB_PREFIX . "product p ON (pr.khuyenmaisp_id = p.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE pr.product_id = '" . (int)$product_id . "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'");
		foreach ($query->rows as $result) {
			$product_data[$result['khuyenmaisp_id']] = $this->getSmallInfoProduct($result['khuyenmaisp_id']);
		}

		return $product_data;
	}

	private function getSmallInfoProduct($product_id){

		$query = $this->db->query("SELECT p.product_id as product_id, pd.name as name, p.image FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'");
		//var_dump("SELECT p.product_id, pd.name, p.image FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'");
		return $query->row;	
	}
	
	
}
?>