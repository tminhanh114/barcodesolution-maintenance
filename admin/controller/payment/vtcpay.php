<?php
/*****************************************************************************
 *                                                                           *
 *            Module t�ch h?p thanh to�n VTCPAY                              *
 * Phi�n b?n : 2.0                                                           *
 * Module du?c ph�t tri?n b?i IT TRUNG T�M PH?N M?M INTECOM                                     *
 * Ch?c nang :                                                               *
 * - T�ch h?p thanh to�n qua pay.vtc.vn cho c�c merchant site c� dang k� API.*
 * - G?i th�ng tin thanh to�n t?i pay.vtc.vn d? x? l� vi?c thanh to�n.       *
 * - X�c th?c t�nh ch�nh x�c c?a th�ng tin du?c g?i v? t? pay.vtc.vn         *
 * @author toanld                                                            *
 *****************************************************************************
 * Xin h�y d?c ki t�i li?u t�ch h?p tr�n trang                               *
 * http://pay.vtc.vn/                                                        *
 *                                                                           *
 *****************************************************************************/
class ControllerPaymentVTCPay extends Controller {
	private $error = array(); 

	public function index() {
		$this->load->language('payment/vtcpay');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
			
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validate())) {
			$this->load->model('setting/setting');
			
			$this->model_setting_setting->editSetting('vtcpay', $this->request->post);				
			
			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'));
		}

		  $data['heading_title']  		= $this->language->get('heading_title');
    	  $data['text_enabled']   		= $this->language->get('text_enabled');
    	  $data['text_disabled']  		= $this->language->get('text_disabled');
    	  $data['entry_receiver'] 		= $this->language->get('entry_receiver');
          $data['entry_urlreturn'] 		= $this->language->get('entry_urlreturn');
          $data['entry_order_status'] 	= $this->language->get('entry_order_status');		
    	  $data['entry_status'] 		= $this->language->get('entry_status');
    	  $data['entry_sort_order'] 	= $this->language->get('entry_sort_order');
    	  $data['button_save'] 			= $this->language->get('button_save');
    	  $data['button_cancel'] 		= $this->language->get('button_cancel');
    	  $data['tab_general'] 			= $this->language->get('tab_general');

  		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
        if (isset($this->error['receiver'])) {
	       $data['error_receiver'] = $this->error['receiver'];
	    } else {
	       $data['error_receiver'] = '';
        }
        
        if (isset($this->error['urlreturn'])) {
	       $data['error_urlreturn'] = $this->error['urlreturn'];
	    } else {
	       $data['error_urlreturn'] = '';
        }

 		

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_payment'),
			'href' => $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('payment/vtcpay', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['action'] = $this->url->link('payment/vtcpay', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL');

        if (isset($this->request->post['vtcpay_total'])) {
			$data['vtcpay_total'] = $this->request->post['vtcpay_total'];
		} else {
			$data['vtcpay_total'] = $this->config->get('vtcpay_total'); 
		}

		if (isset($this->request->post['vtcpay_receiver'])) {
            $data['vtcpay_receiver'] = $this->request->post['vtcpay_receiver'];
        } else {
            $data['vtcpay_receiver'] = $this->config->get('vtcpay_receiver');
        }
        
        if (isset($this->request->post['vtcpay_urlreturn'])) {
            $data['vtcpay_urlreturn'] = $this->request->post['vtcpay_urlreturn'];
        } else {
            $data['vtcpay_urlreturn'] = $this->config->get('vtcpay_urlreturn');
        }
        
        if (isset($this->request->post['vtcpay_order_status_id'])) {
            $data['vtcpay_order_status_id'] = $this->request->post['vtcpay_order_status_id'];
        } else {
            $data['vtcpay_order_status_id'] = $this->config->get('vtcpay_order_status_id'); 
        }  
        
      	$data['callback'] = HTTP_CATALOG . 'index.php?route=payment/vtcpay/callback';
		
		$this->load->model('localisation/order_status');
		
		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
		
        
        
		if (isset($this->request->post['vtcpay_status'])) {
			$data['vtcpay_status'] = $this->request->post['vtcpay_status'];
		} else {
			$data['vtcpay_status'] = $this->config->get('vtcpay_status');
		}
		
		if (isset($this->request->post['vtcpay_sort_order'])) {
			$data['vtcpay_sort_order'] = $this->request->post['vtcpay_sort_order'];
		} else {
			$data['vtcpay_sort_order'] = $this->config->get('vtcpay_sort_order');
		}
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$this->template = 'payment/vtcpay.tpl';
		$this->response->setOutput($this->load->view('payment/vtcpay.tpl', $data));
		//$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
	}

	private function validate() {
		if (!$this->user->hasPermission('modify', 'payment/vtcpay')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		
        if (!$this->request->post['vtcpay_receiver']) {
	       $this->error['receiver'] = $this->language->get('error_receiver');
	    }
        
		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}	
	}
}
?>