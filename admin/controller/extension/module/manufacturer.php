<?php
/*
    Module Name: Manufacturers Module
    Description:  The module is developed to show manufacturers list on any page of opencart. You can set module name, you can show or hide manaufacturers image in module settings.This is very initial version of module.
    Author: Umer Khalid Cheema
	Author Email:umertoday@gmail.com
    Author URI: https://www.facebook.com/umertoday
    Version: 1.0
    Tags: Module, Manufacturers, OpenCart
*/
class ControllerExtensionModuleManufacturer extends Controller

	{
	private $error = array();
	public

	function index()
		{
		$this->load->language('extension/module/manufacturer');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('setting/setting');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate())
			{
			$this->model_setting_setting->editSetting('manufacturer', $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true));
			}

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['entry_heading'] = $this->language->get('entry_heading');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_image_height_width'] = $this->language->get('entry_image_height_width');
		$data['entry_image_height'] = $this->language->get('entry_image_height');
		$data['entry_image_width'] = $this->language->get('entry_image_width');
		$data['author_creadits'] = $this->language->get('author_creadits');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		if (isset($this->error['warning']))
			{
			$data['error_warning'] = $this->error['warning'];
			}
		  else
			{
			$data['error_warning'] = '';
			}

		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home') ,
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension') ,
			'href' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title') ,
			'href' => $this->url->link('extension/module/manufacturer', 'token=' . $this->session->data['token'], true)
		);
		$data['action'] = $this->url->link('extension/module/manufacturer', 'token=' . $this->session->data['token'], true);
		$data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true);
		if (isset($this->request->post['manufacturer_heading']))
			{
			$data['manufacturer_heading'] = $this->request->post['manufacturer_heading'];
			}
		  else
			{
			$data['manufacturer_heading'] = $this->config->get('manufacturer_heading');
			}

		if (isset($this->request->post['manufacturer_image_status']))
			{
			$data['manufacturer_image_status'] = $this->request->post['manufacturer_image_status'];
			}
		  else
			{
			$data['manufacturer_image_status'] = $this->config->get('manufacturer_image_status');
			}

		if (isset($this->request->post['manufacturer_image_height']))
			{
			$data['manufacturer_image_height'] = $this->request->post['manufacturer_image_height'];
			}
		  else
			{
			$data['manufacturer_image_height'] = $this->config->get('manufacturer_image_height');
			}

		if (isset($this->request->post['manufacturer_image_width']))
			{
			$data['manufacturer_image_width'] = $this->request->post['manufacturer_image_width'];
			}
		  else
			{
			$data['manufacturer_image_width'] = $this->config->get('manufacturer_image_width');
			}

		if (isset($this->request->post['manufacturer_status']))
			{
			$data['manufacturer_status'] = $this->request->post['manufacturer_status'];
			}
		  else
			{
			$data['manufacturer_status'] = $this->config->get('manufacturer_status');
			}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$this->response->setOutput($this->load->view('extension/module/manufacturer', $data));
		}

	protected
	function validate()
		{
		if (!$this->user->hasPermission('modify', 'extension/module/manufacturer'))
			{
			$this->error['warning'] = $this->language->get('error_permission');
			}

		return !$this->error;
		}
	}